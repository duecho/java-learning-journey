
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ShannonFanoCoding {

    static class Symbol {
        char symbol;
        int frequency;
        String code;

        Symbol(char symbol, int frequency) {
            this.symbol = symbol;
            this.frequency = frequency;
            this.code = "";
        }
    }

    public static void main(String[] args) {
        String inputString = "FEDCBA"; // 你的输入文本
        List<Symbol> symbols = calculateFrequencies(inputString);
        Collections.sort(symbols, (s1, s2) -> Integer.compare(s2.frequency, s1.frequency));

        createShannonFanoCodes(symbols, 0, symbols.size() - 1);

        double averageCodeLength = calculateAverageCodeLength(symbols);
        double codingEfficiency = calculateCodingEfficiency(averageCodeLength);
        double informationTransmissionEfficiency = calculateInformationTransmissionEfficiency(averageCodeLength);

        System.out.println("字符\t频率\t香农编码");
        for (Symbol symbol : symbols) {
            System.out.printf("%c\t%d\t%s\n", symbol.symbol, symbol.frequency, symbol.code);
        }
        System.out.printf("\n平均码长: %.2f bits/字符\n", averageCodeLength);
        System.out.printf("编码效率: %.2f%%\n", codingEfficiency);
        System.out.printf("信息传输效率: %.2f%%\n", informationTransmissionEfficiency);
    }

    private static List<Symbol> calculateFrequencies(String input) {
        Map<Character, Integer> frequencyMap = new HashMap<>();
        for (char symbol : input.toCharArray()) {
            frequencyMap.put(symbol, frequencyMap.getOrDefault(symbol, 0) + 1);
        }

        List<Symbol> symbols = new ArrayList<>();
        for (Map.Entry<Character, Integer> entry : frequencyMap.entrySet()) {
            char symbol = entry.getKey();
            int frequency = entry.getValue();
            symbols.add(new Symbol(symbol, frequency));
        }

        return symbols;
    }

    private static void createShannonFanoCodes(List<Symbol> symbols, int start, int end) {
        if (start == end) {
            return;
        }

        int cumulativeFrequency = 0;
        int halfFrequency = 0;

        for (int i = start; i <= end; i++) {
            cumulativeFrequency += symbols.get(i).frequency;
        }

        for (int i = start; i <= end; i++) {
            halfFrequency += symbols.get(i).frequency;
            if (halfFrequency >= cumulativeFrequency / 2) {
                for (int j = start; j <= i; j++) {
                    symbols.get(j).code += "0";
                }
                for (int j = i + 1; j <= end; j++) {
                    symbols.get(j).code += "1";
                }

                createShannonFanoCodes(symbols, start, i);
                createShannonFanoCodes(symbols, i + 1, end);
                break;
            }
        }
    }

    private static double calculateAverageCodeLength(List<Symbol> symbols) {
        double averageCodeLength = 0;

        for (Symbol symbol : symbols) {
            averageCodeLength += symbol.frequency * symbol.code.length();
        }

        return averageCodeLength / symbols.stream().mapToInt(s -> s.frequency).sum();
    }

    private static double calculateCodingEfficiency(double averageCodeLength) {
        return 1 / averageCodeLength * 100;
    }

    private static double calculateInformationTransmissionEfficiency(double averageCodeLength) {
        return Math.log(2) / averageCodeLength * 100;
    }
}
