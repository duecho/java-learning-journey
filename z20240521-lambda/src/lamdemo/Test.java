package lamdemo;

/**
 * @Author: zzj
 * @Description:
 */

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.PriorityQueue;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

//无返回值无参数================================
@FunctionalInterface
interface NoParameterNoReturn {
    void test();
}
//无返回值一个参数
@FunctionalInterface
interface OneParameterNoReturn {
    void test(int a);
}
//无返回值多个参数
@FunctionalInterface
interface MoreParameterNoReturn {
    void test(double a,int b);
}

//有返回值无参数
@FunctionalInterface
interface NoParameterReturn {
    int test();
}
//有返回值一个参数
@FunctionalInterface
interface OneParameterReturn {
    int test(int a);
}

//有返回值多参数
@FunctionalInterface
interface MoreParameterReturn {
    int test(int a,int b);
}
public class Test {

    public static void main(String[] args) {
        HashMap< String,Integer> map = new HashMap<>();
        map.put("宋江",1);
        map.put("诸葛亮",2);
        map.put("张飞",3);
        map.put("关羽",3);
        System.out.println(map);
    }

    public static void main5(String[] args) {
        HashMap<Integer, String> map = new HashMap<>();
        map.put(1, "hello");
        map.put(2, "bit");
        map.put(3, "aello");
        map.put(4, "lambda");

        /*map.forEach(new BiConsumer<Integer, String>() {
            @Override
            public void accept(Integer integer, String s) {
                System.out.println("key: "+integer+" val: "+s);
            }
        });*/

        map.forEach((integer, s) -> System.out.println("key: "+integer+" val: "+s));

    }

    public static void main4(String[] args) {
        ArrayList<String> list = new ArrayList<>();
        list.add("hello");
        list.add("bit");
        list.add("lambda");
        /*list.forEach(new Consumer<String>() {
            @Override
            public void accept(String s) {
                System.out.println(s);
            }
        });*/
       /* list.sort(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.compareTo(o2);
            }
        });*/
        list.forEach(s-> System.out.println(s));
        list.sort(((o1, o2) -> o1.compareTo(o2)));
        System.out.println("====");
        list.forEach(s-> System.out.println(s));
    }

    public static void main3(String[] args) {
        //NoParameterReturn noParameterReturn = ()->{return 10;};
        NoParameterReturn noParameterReturn = ()->10;
        int ret = noParameterReturn.test();
        System.out.println(ret);

        OneParameterReturn oneParameterReturn = b -> b+9;
        ret = oneParameterReturn.test(10);
        System.out.println(ret);

        MoreParameterReturn moreParameterReturn = (a,b)->a+b;

        ret = moreParameterReturn.test(10,20);
        System.out.println(ret);
    }

    public static void main2(String[] args) {
        NoParameterNoReturn noParameterNoReturn = ()->System.out.println("test()....");
        noParameterNoReturn.test();
        //OneParameterNoReturn oneParameterNoReturn = (int a)->{System.out.println(a+1);};
        OneParameterNoReturn oneParameterNoReturn =  a -> System.out.println(a+1);
        oneParameterNoReturn.test(10);

        MoreParameterNoReturn moreParameterNoReturn = (a,b)-> System.out.println(a+b);
        moreParameterNoReturn.test(1.0,2);
    }

    public static void main1(String[] args) {
        int common = 10;
        NoParameterNoReturn noParameterNoReturn = new NoParameterNoReturn(){
            @Override
            public void test() {
                //common = 99;
                System.out.println("test()....");
                System.out.println(common);
            }
        };

        noParameterNoReturn.test();

        PriorityQueue<Integer> priorityQueue = new PriorityQueue<>(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o1-o2;
            }
        });

        PriorityQueue<Integer> priorityQueue2 = new PriorityQueue<>((o1,o2)->o1-o2);
    }
}