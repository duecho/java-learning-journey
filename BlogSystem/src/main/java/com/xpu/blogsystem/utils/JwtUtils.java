package com.xpu.blogsystem.utils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.JwtParser;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.Assert;

import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Slf4j
public class JwtUtils {
    private static long expiration =  30 * 60 * 1000;  //30分钟过期
    private static String secretString = "XX7LWoRwue2WbMUlgopSdpofgAm3K0zIUenGsGD8D0l";
    private static Key key = Keys.hmacShaKeyFor(Decoders.BASE64.decode(secretString));

    /**
     * 生成令牌
     * @param claims
     * @return
     */
    public static String genJwt(Map<String, Object> claims){
        return Jwts.builder()
                .setClaims(claims)
                .setExpiration(new Date(System.currentTimeMillis() + expiration))
                .signWith(key)
                .compact();
    }

    /**
     * 校验令牌
     */
    public static Claims verify(String token){
        JwtParser build = Jwts.parserBuilder().setSigningKey(key).build();

        Claims claims = null;
        try {
            claims = build.parseClaimsJws(token).getBody();
        }catch (ExpiredJwtException e){
            log.error("token过期, 校验失败, token:"+ token);
        }catch (Exception e){
            log.error("token校验失败, token:"+ token);
        }

        return claims;
    }

    public static Integer getUserIdFromToken(String token){
        Claims claims = verify(token);
        Assert.notNull(claims, "token不合法");
        Integer userId = (Integer) claims.get("id");
        return userId;
    }

}
