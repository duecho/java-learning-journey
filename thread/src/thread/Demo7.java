package thread;

/**
 * @Author: zzj
 * @Description:
 */
public class Demo7 {
    public static void main(String[] args) {
        Thread t = new Thread(() -> {
            for (int i = 0; i < 5; i++) {
                System.out.println("hello thread");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        });
        //设置后台线程
        t.setDaemon(true);
        t.start();
    }
}
