/**
 * @Author: zzj
 * @Description:
 */
public class HashBuck2 <k,v>{
    static class Node<k,v> {
        public k key;
        public v val;
        public Node<k,v> next;

        public Node(k key, v val) {
            this.key = key;
            this.val = val;
        }
    }
    public Node<k,v>[] array =(Node<k,v>[]) new Node[10];
    public int usedSize;
    public static final double LOAF_FACTOR = 0.75;
    public void push(k key,v val) {
        Node<k,v> node = new Node<k,v>(key,val);
        int hash = key.hashCode();
        int index = hash % array.length;
        Node<k,v> cur = array[index];
        while (cur != null) {
            if (cur.key.equals(key)) {
                cur.val = val;
                return;
            }
            cur = cur.next;
        }
        node.next = array[index];
        array[index] = node;
        usedSize++;
        //重新哈希

     }
     public v get(k key) {
        int hash = key.hashCode();
        int index = hash % array.length;
        Node<k,v> cur = array[index];
        while (cur != null) {
            if (cur.key.equals(key)) {
                return cur.val;
            }
            cur = cur.next;
        }
        return null;
     }
}
