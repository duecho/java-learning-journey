package com.xpu.demo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author: zzj
 * @Description:
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserInfo {

    private String userName;
    private String password;
    private int age;

//    public UserInfo() {
//
//    }
//
//
//    public String getUserName() {
//        return userName;
//    }


//    @Override
//    public String toString() {
//        return "UserInfo{" +
//                "userName='" + userName + '\'' +
//                ", password='" + password + '\'' +
//                ", age=" + age +
//                '}';
//    }

//    public void setUserName(String userName) {
//        this.userName = userName;
//    }
//
//    public String getPassword() {
//        return password;
//    }
//
//    public void setPassword(String password) {
//        this.password = password;
//    }
//
//    public int getAge() {
//        return age;
//    }
//
//    public void setAge(int age) {
//        this.age = age;
//    }
}
