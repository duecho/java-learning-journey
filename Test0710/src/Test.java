import java.util.PriorityQueue;
import java.util.Scanner;

/**
 * @Author: zzj
 * @Description:
 */
public class Test {
    //给一个数组，一共有 n 个数。
    //你能进行最多k次操作。每次操作可以进行以下步骤
    //选择数组中的一个偶数 ai，将其变成 ai/2。
    //现在你进行不超过k次操作后，让数组中所有数之和尽可
    //能小。请输出这个最小的和。
    public static void main3(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int k = in.nextInt();
        //创建大根堆，堆顶元素减半，是偶数重新入堆
        PriorityQueue<Integer> heap = new PriorityQueue<>((a, b)->{
            return b-a;
        });
        long sum = 0;
        int  x = -1;
        for (int i = 0; i < n; i++) {
            x = in.nextInt();
            sum += x;
            if (x % 2 == 0) {
                heap.add(x);
            }
        }
        while (!heap.isEmpty() && k-- != 0) {
            int t = heap.poll() / 2;
            sum -= t;
            if (t % 2 == 0) {
                heap.add(t);
            }
        }
        System.out.println(sum);
    }

    //dd爱框框
    //给一个数组，指定一个和，求数组中累加大于和的区间段
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int k = in.nextInt();
        int[] arr = new int[n +1];
        for (int i = 1; i < n; i++) {
            arr[i] = in.nextInt();
        }
        int right = 1;
        int left = 1;
        int sum = 0;
        int retright = -1;
        int retleft = -1;
        int retLen = n;
        while (right <= n) {
            sum += arr[right];
            while (sum >= k) {
                if (right-left+1 > retLen) {
                    retleft = left;
                    retright = right;
                    retLen = right-left+1;
                }
                sum -= arr[left];
            }
            right++;
        }
        System.out.println(sum);
    }
    //简写单词
    public static void main1(String[] args) {
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()) {
            char ch = sc.next().charAt(0);
            if (ch >= 'a' && ch <= 'z'){
                System.out.println((char) (ch-32));
            }else {
                System.out.print(ch);
            }
        }
    }
}
