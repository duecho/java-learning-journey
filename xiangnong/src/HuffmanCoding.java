import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

class HuffmanNode implements Comparable<HuffmanNode> {
    char data;
    int frequency;
    HuffmanNode left, right;

    public HuffmanNode(char data, int frequency) {
        this.data = data;
        this.frequency = frequency;
    }

    @Override
    public int compareTo(HuffmanNode node) {
        return this.frequency - node.frequency;
    }
}

public class HuffmanCoding {

    private static Map<Character, String> huffmanCodes = new HashMap<>();

    public static void main(String[] args) {
        String inputText = "Hello, World!";
        Map<Character, Integer> frequencyMap = calculateFrequency(inputText);
        HuffmanNode root = buildHuffmanTree(frequencyMap);
        generateHuffmanCodes(root, "");

        // 输出哈夫曼编码
        System.out.println("哈夫曼编码表:");
        for (Map.Entry<Character, String> entry : huffmanCodes.entrySet()) {
            System.out.printf("%s : %s%n", entry.getKey(), entry.getValue());
        }

        // 计算平均码长
        double averageCodeLength = calculateAverageCodeLength(frequencyMap);
        System.out.printf("平均码长: %.2f bit/字符%n", averageCodeLength);

        // 计算编码效率
        double encodingEfficiency = calculateEncodingEfficiency(frequencyMap, averageCodeLength);
        System.out.printf("编码效率: %.2f%%%n", encodingEfficiency);

        // 计算信息传输效率
        double transmissionEfficiency = calculateTransmissionEfficiency(averageCodeLength);
        System.out.printf("信息传输效率: %.2f%%%n", transmissionEfficiency);
    }

    private static Map<Character, Integer> calculateFrequency(String input) {
        Map<Character, Integer> frequencyMap = new HashMap<>();
        for (char c : input.toCharArray()) {
            frequencyMap.put(c, frequencyMap.getOrDefault(c, 0) + 1);
        }
        return frequencyMap;
    }

    private static HuffmanNode buildHuffmanTree(Map<Character, Integer> frequencyMap) {
        PriorityQueue<HuffmanNode> priorityQueue = new PriorityQueue<>();

        // 初始化优先队列
        for (Map.Entry<Character, Integer> entry : frequencyMap.entrySet()) {
            priorityQueue.offer(new HuffmanNode(entry.getKey(), entry.getValue()));
        }

        // 构建哈夫曼树
        while (priorityQueue.size() > 1) {
            HuffmanNode left = priorityQueue.poll();
            HuffmanNode right = priorityQueue.poll();

            HuffmanNode newNode = new HuffmanNode('\0', left.frequency + right.frequency);
            newNode.left = left;
            newNode.right = right;

            priorityQueue.offer(newNode);
        }

        return priorityQueue.poll(); // 返回根节点
    }

    private static void generateHuffmanCodes(HuffmanNode node, String code) {
        if (node != null) {
            if (node.data != '\0') {
                huffmanCodes.put(node.data, code);
            }
            generateHuffmanCodes(node.left, code + "0");
            generateHuffmanCodes(node.right, code + "1");
        }
    }

    private static double calculateAverageCodeLength(Map<Character, Integer> frequencyMap) {
        double totalLength = 0;
        double totalFrequency = 0;

        for (Map.Entry<Character, Integer> entry : frequencyMap.entrySet()) {
            char symbol = entry.getKey();
            int frequency = entry.getValue();
            totalLength += frequency * huffmanCodes.get(symbol).length();
            totalFrequency += frequency;
        }

        return totalLength / totalFrequency;
    }
    private static double calculateEncodingEfficiency(Map<Character, Integer> frequencyMap, double averageCodeLength) {
        double entropy = 0;

        for (Map.Entry<Character, Integer> entry : frequencyMap.entrySet()) {
            double probability = (double) entry.getValue() / frequencyMap.size();
            entropy += probability * log2(1 / probability, 2); // 使用香农熵公式
        }

        double minimumAverageCodeLength = calculateMinimumAverageCodeLength(frequencyMap);
        return ((minimumAverageCodeLength - averageCodeLength) / minimumAverageCodeLength) * 100;
    }

    private static double calculateMinimumAverageCodeLength(Map<Character, Integer> frequencyMap) {
        double entropy = 0;

        for (Map.Entry<Character, Integer> entry : frequencyMap.entrySet()) {
            double probability = (double) entry.getValue() / frequencyMap.size();
            entropy += probability * log2(1 / probability, 2);
        }

        return entropy / Math.log(2); // 最小可能平均码长就是香农熵
    }

    private static double calculateTransmissionEfficiency(double averageCodeLength) {
        return (1 / averageCodeLength) * 100;
    }

    private static double log2(double x, int base) {
        return Math.log(x) / Math.log(base);
    }
}
