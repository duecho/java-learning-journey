import javax.naming.PartialResultException;
import java.sql.BatchUpdateException;
import java.sql.SQLOutput;
import java.util.Arrays;
import java.util.Date;
import static java.lang.Math.*;
import static java.lang.System.*;
class  Student {
    private String name;
    private int age;
    public static String classRoom;
    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }
    //只能在该类中使用eat()
    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return this.name;
    }
    public void eat() {
        System.out.println(this.name +" 正在吃饭！ ");
        sleep();
        System.out.println(classRoom);
    }
    public static void sleep() {
        System.out.println(" 正在睡觉！");
    }
   /* public void eat1() {
        System.out.println(this.name +" 正在吃饭！ ");
    }*/

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public static String getClassRoom() {
        return classRoom;
    }

    public  void setClassRoom(String classRoom) {
        Student.classRoom = classRoom;
    }

    //封装对内体现隐藏
}

class TestStatic {
    public static int staticNum = 100;
    public int data1 = 10;
    {
        System.out.println("实例代码块被执行了。。。。");
        data1 = 1000;
    }
    static {
        System.out.println("静态代码块被执行了。。、。。");
    }

    public static void main(String[] args) {
        TestStatic testStatic = new TestStatic();
        System.out.println(testStatic.data1);
        System.out.println(TestStatic.staticNum);
    }
}

public class Test {
    public static void main5(String[] args) {
        Student student1 = new Student("hangman",18);
        student1.setClassRoom("2-101");
        Student.classRoom = "2-101";
        /*student1.sleep();
        Student.sleep();*/





        Student student2 = new Student("lisi",13);
        /*student1.setClassRoom("2-101");*/
        Student.classRoom = "2-101";
        Student student3 = new Student("zzj",21);
        /*student1.setClassRoom("2-101");*/
        Student.classRoom = "2-101";
    }

    public static void main3(String[] args) {
        java.util.Date date1 = new java.util.Date();
        Date date = new Date();
    }
    public static void main2(String[] args) {
        Student student =new Student("张三",18);
        //实现封装后，类外 就无法直接拿到这个name字段
        student.setName("小明");
        System.out.println(student.getName());
        student.setAge(18);
        System.out.println(student.getAge());
        student.eat();
    }
    public static void main1(String[] args) {
        int[] array = {1,2,3,4};
        transform(array);
        System.out.println(Arrays.toString(array));
    }

    public static void transform(int[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = 2*array[i];
        }
    }
}
