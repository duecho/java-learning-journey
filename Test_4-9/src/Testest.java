/**
 * @Author: zzj
 * @Description:
 */

import javax.crypto.Cipher;
import javax.crypto.spec.OAEPParameterSpec;
import javax.crypto.spec.PSource;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.MGF1ParameterSpec;


public class Testest {


        public static void main(String[] args) throws Exception {
            // 生成RSA密钥对
            KeyPair keyPair = generateRSAKeyPair();

            // 获取公钥和私钥
            PublicKey publicKey = keyPair.getPublic();
            PrivateKey privateKey = keyPair.getPrivate();

            // 明文
            byte[] plaintext = "Hello, RSA-OAEP!".getBytes();

            // 使用公钥进行加密
            byte[] ciphertext = encrypt(publicKey, plaintext);

            System.out.println("Ciphertext: " + new String(ciphertext));

            // 使用私钥进行解密
            byte[] decryptedText = decrypt(privateKey, ciphertext);

            System.out.println("Decrypted: " + new String(decryptedText));
        }

        public static KeyPair generateRSAKeyPair() throws Exception {
            KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
            keyPairGenerator.initialize(2048); // Key size 2048 bits
            return keyPairGenerator.generateKeyPair();
        }

        public static byte[] encrypt(PublicKey publicKey, byte[] plaintext) throws Exception {
            Cipher cipher = Cipher.getInstance("RSA/ECB/OAEPWithSHA-256AndMGF1Padding");
            OAEPParameterSpec oaepParams = new OAEPParameterSpec("SHA-256", "MGF1", MGF1ParameterSpec.SHA256, PSource.PSpecified.DEFAULT);
            cipher.init(Cipher.ENCRYPT_MODE, publicKey, oaepParams);
            return cipher.doFinal(plaintext);
        }

        public static byte[] decrypt(PrivateKey privateKey, byte[] ciphertext) throws Exception {
            Cipher cipher = Cipher.getInstance("RSA/ECB/OAEPWithSHA-256AndMGF1Padding");
            OAEPParameterSpec oaepParams = new OAEPParameterSpec("SHA-256", "MGF1", MGF1ParameterSpec.SHA256, PSource.PSpecified.DEFAULT);
            cipher.init(Cipher.DECRYPT_MODE, privateKey, oaepParams);
            return cipher.doFinal(ciphertext);
        }

}
