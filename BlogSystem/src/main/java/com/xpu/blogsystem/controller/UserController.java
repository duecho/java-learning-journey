package com.xpu.blogsystem.controller;

import com.xpu.blogsystem.enums.ResultCodeEnum;
import com.xpu.blogsystem.model.Result;
import com.xpu.blogsystem.model.UserInfo;
import com.xpu.blogsystem.service.UserService;
import com.xpu.blogsystem.utils.JwtUtils;
import com.xpu.blogsystem.utils.SecurityUtils;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@RequestMapping("/user")
@RestController
public class UserController {
    @Autowired
    private UserService userService;

    @RequestMapping("/login")
    public Result login(String userName, String password) {
        log.info("用户登录, userName: {}", userName);
        Assert.hasLength(userName, "用户名不能为空");
        Assert.hasLength(password, "密码不能为空");
        //验证账号密码是否正确
        UserInfo userInfo = userService.selectUserInfoByName(userName);
        if (userInfo==null || userInfo.getId()==null){
            return Result.fail(ResultCodeEnum.PARAM_INVALID.getCode(), "用户不存在");
        }
        if (!SecurityUtils.verify(password, userInfo.getPassword())){
            log.error("密码验证失败");
            return Result.fail(ResultCodeEnum.PARAM_INVALID.getCode(), "密码错误");
        }
//        if (!password.equals(userInfo.getPassword())){
//            return Result.fail(ResultCodeEnum.PARAM_INVALID.getCode(), "密码错误");
//        }
//        //账号密码正确的逻辑
        Map<String,Object> cliams = new HashMap<>();
        cliams.put("id", userInfo.getId());
        cliams.put("name", userInfo.getUserName());

        String jwt = JwtUtils.genJwt(cliams);
        return Result.success(jwt);

    }
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public Result register(@RequestParam String userName, @RequestParam String password, @RequestParam String confirmPassword) throws UnsupportedEncodingException {
        log.info("用户注册, userName: {}", userName);
        Assert.hasLength(userName, "用户名不能为空");
        Assert.hasLength(password, "密码不能为空");
        Assert.hasLength(confirmPassword, "确认密码不能为空");

        if (!password.equals(confirmPassword)) {
            return Result.fail(ResultCodeEnum.PARAM_INVALID.getCode(), "密码和确认密码不匹配");
        }

        // 检查用户名是否已经存在
        UserInfo existingUser = userService.selectUserInfoByName(userName);
        if (existingUser != null) {
            return Result.fail(ResultCodeEnum.PARAM_INVALID.getCode(), "用户名已存在");
        }

        // 加密密码并保存用户信息
        String encryptedPassword = SecurityUtils.encrypt(password);
        UserInfo newUser = new UserInfo();
        newUser.setUserName(userName);
        newUser.setPassword(encryptedPassword);

        boolean registrationSuccess = userService.registerUser(newUser);
        if (registrationSuccess) {
            return Result.success("注册成功");
        } else {
            return Result.fail(ResultCodeEnum.FAIL.getCode(), "注册失败");
        }
    }



    @RequestMapping("/getUserInfo")
    public UserInfo getUserInfo(HttpServletRequest request){
        String userToken = request.getHeader("user_token_header");
        //从token中获取用户ID
        Integer userId = JwtUtils.getUserIdFromToken(userToken);
        Assert.notNull(userId, "用户未登录");
        UserInfo userInfo = userService.selectUserInfoById(userId);
        return userInfo;
    }

    @RequestMapping("/getAuthorInfo")
    public UserInfo getAuthorInfo(Integer blogId){
        Assert.notNull(blogId, "博客ID不能为null");
        return userService.selectAuthorInfoByBlogId(blogId);
    }
}
