import java.util.ArrayList;
import java.util.Arrays;

/**
 * @Author: zzj
 * @Description:
 */
public class MyArrayList implements IList {
    public int[] elem;
    public int usedSize;
    public static final int DEFAULT_CAPACITY = 5;
    public MyArrayList() {
        elem = new int[DEFAULT_CAPACITY];
    }
    //默认添加到数组的最后位置
    @Override
    public void add(int data) {
        //1.判断是否满了
        if(isFull()){
            elem = Arrays.copyOf(elem,2*elem.length);
        }
        this.elem[usedSize] = data;
        usedSize++;
    }

    @Override
    public boolean isFull() {
        return usedSize ==elem.length;
    }

    @Override
    public void add(int pos, int data) {
            //1.检查pos的位置判断，即合法性
            CheckPosOfAdd(pos);
        if(isFull()){
            elem = Arrays.copyOf(elem,2*elem.length);
        }
        for (int i = usedSize - 1; i >= pos ; i--) {
            elem[i+1] = elem[i];
        }
        this.elem[pos] = data;
        usedSize++;
    }
    private void CheckPosOfAdd(int pos) {
        if (pos < 0 || pos > usedSize) {
            throw new PosException("pos的位置： " + pos);
        }
    }
    //找到当前元素是否存在
    @Override
    public boolean contains(int toFind) {
        for (int i = 0; i < usedSize; i++) {
            if (toFind == elem[i]) { //引用类型用equals比较
                return true;
            }
        }
        return false;
    }
    //查找当前元素的下标
    @Override
    public int indexOf(int toFind) {
        for (int i = 0; i < usedSize; i++) {
            if (toFind == elem[i]){
                return i;
            }
        }
        return -1;
    }
    //获取pos位置的值
    @Override
    public int get(int pos) {
        //1.pos位置不合法怎么办
        CheckPosOfGet(pos);
        //2.为空怎么办
        if (isEmpty()){
            throw new EmptyException("顺序表为空");
        }
        return elem[pos];
    }
    public  boolean isEmpty() {
        return  usedSize == 0;
    }
    private void CheckPosOfGet(int pos) {
        if (pos < 0 || pos > this.usedSize) {
            throw new PosException("pos的位置： " + pos);
        }
    }
    //更新pos的值为value
    @Override
    public void set(int pos, int value) {
        CheckPosOfGet(pos);
        if (isEmpty()) {
            throw new EmptyException("顺序表为空");
        }
        this.elem[pos] = value;
    }
    //删除toRemove这个数字
    @Override
    public void remove(int toRemove) {
        if (isEmpty()) {
            throw  new EmptyException("顺序表为空");
        }
        int index = indexOf(toRemove);
        for (int i = index; i < usedSize-1; i++) {
            elem[i] = elem[i+1];
        }
        usedSize--;
    }

    @Override
    public int size() {
        return this.usedSize;
    }
    //清空顺序表，防止资源泄漏----内存泄露
    @Override
    public void clear() {
        this.usedSize = 0;
    }
    // 打印顺序表
    @Override
    public void display() {
        for (int i = 0; i < this.usedSize; i++) {
            System.out.print(elem[i] + " ");
        }
        System.out.println();
    }
}
