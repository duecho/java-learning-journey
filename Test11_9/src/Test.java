abstract class Shape {
    public abstract void draw();
}

class Rect extends Shape {
    @Override
    public void draw() {
        System.out.println("画矩形");
    }
}
class Cycle extends Shape {

    public void draw() {
        System.out.println("画圈圈");
    }
}
class Flower extends Shape {
    @Override
    public void draw() {
        System.out.println("画花");
    }
}
public class Test {
    public static void main(String[] args) {

    }
}
