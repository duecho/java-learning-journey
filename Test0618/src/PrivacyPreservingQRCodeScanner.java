import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

public class PrivacyPreservingQRCodeScanner extends JFrame {

    // 模拟数据库，存储加密后的信息、时间戳、设备标识和 IP 地址
    static Map<String, ScanInfo> database = new HashMap<>();

    private JTextField nameTextField;
    private JTextField idNumberTextField;
    private JButton scanButton;
    private JButton generateQRButton;
    private JButton traceButton;
    private JTextArea resultTextArea;
    private JLabel qrCodeLabel;

    public PrivacyPreservingQRCodeScanner() {
        setTitle("隐私保护扫码系统");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setMinimumSize(new Dimension(800, 600));

        JPanel inputPanel = new JPanel(new FlowLayout());
        JLabel nameLabel = new JLabel("姓名:");
        nameTextField = new JTextField(20);
        JLabel idNumberLabel = new JLabel("身份证号:");
        idNumberTextField = new JTextField(20);
        scanButton = new JButton("扫码");
        generateQRButton = new JButton("生成二维码");

        inputPanel.add(nameLabel);
        inputPanel.add(nameTextField);
        inputPanel.add(idNumberLabel);
        inputPanel.add(idNumberTextField);
        inputPanel.add(scanButton);
        inputPanel.add(generateQRButton);

        JPanel buttonPanel = new JPanel(new FlowLayout());
        traceButton = new JButton("追踪");

        buttonPanel.add(traceButton);

        resultTextArea = new JTextArea(10, 30);
        resultTextArea.setEditable(false);
        JScrollPane scrollPane = new JScrollPane(resultTextArea);

        qrCodeLabel = new JLabel();
        qrCodeLabel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

        add(inputPanel, BorderLayout.NORTH);
        add(buttonPanel, BorderLayout.CENTER);
        add(qrCodeLabel, BorderLayout.WEST);
        add(scrollPane, BorderLayout.SOUTH);

        scanButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String name = nameTextField.getText();
                String idNumber = idNumberTextField.getText();
                if (name.isEmpty() || idNumber.isEmpty()) {
                    JOptionPane.showMessageDialog(PrivacyPreservingQRCodeScanner.this, "姓名和身份证号不能为空！");
                    return;
                }
                String encryptedInfo = scanCode(name, idNumber);
                resultTextArea.append("扫码成功，加密信息: " + encryptedInfo + "\n");
            }
        });

        generateQRButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String name = nameTextField.getText();
                String idNumber = idNumberTextField.getText();
                if (name.isEmpty() || idNumber.isEmpty()) {
                    JOptionPane.showMessageDialog(PrivacyPreservingQRCodeScanner.this, "姓名和身份证号不能为空！");
                    return;
                }
                String content = name + " " + idNumber;
                generateQRCode(content);
            }
        });

        traceButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String encryptedInfo = JOptionPane.showInputDialog(PrivacyPreservingQRCodeScanner.this, "请输入要追踪的加密信息:");
                if (encryptedInfo!= null &&!encryptedInfo.isEmpty()) {
                    traceInfo(encryptedInfo);
                }
            }
        });

        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    // 加密函数
    public static String encryptInfo(String name, String idNumber) {
        String combinedInfo = name + idNumber;
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(combinedInfo.getBytes());
            StringBuilder hexString = new StringBuilder();
            for (byte b : hash) {
                String hex = Integer.toHexString(0xff & b);
                if (hex.length() == 1) {
                    hexString.append('0');
                }
                hexString.append(hex);
            }
            return hexString.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
    }

    // 扫码函数（模拟）
    public static String scanCode(String name, String idNumber) {
        String encryptedInfo = encryptInfo(name, idNumber);
        String deviceId = generateDeviceId();  // 模拟生成设备标识
        String ipAddress = getLocalIPAddress();  // 获取本地 IP 地址

        long timestamp = System.currentTimeMillis();
        ScanInfo scanInfo = new ScanInfo(timestamp, deviceId, ipAddress);
        database.put(encryptedInfo, scanInfo);
        System.out.println("扫码信息已存储，加密信息: " + encryptedInfo);
        return encryptedInfo;
    }

    // 生成随机设备标识
    public static String generateDeviceId() {
        Random random = new Random();
        StringBuilder deviceId = new StringBuilder();
        for (int i = 0; i < 16; i++) {
            deviceId.append(random.nextInt(10));
        }
        return deviceId.toString();
    }

    // 获取本地 IP 地址
    public static String getLocalIPAddress() {
        try {
            InetAddress address = InetAddress.getLocalHost();
            return address.getHostAddress();
        } catch (UnknownHostException e) {
            e.printStackTrace();
            return "Unknown";
        }
    }

    // 生成二维码函数
    public void generateQRCode(String content) {
        int width = 800;  // 进一步增加二维码的宽度
        int height = 800; // 进一步增加二维码的高度
        QRCodeWriter qrCodeWriter = new QRCodeWriter();
        Map<EncodeHintType, Object> hints = new HashMap<>();
        hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");
        hints.put(EncodeHintType.MARGIN, 30);  // 增加二维码的边距

        try {
            BitMatrix bitMatrix = qrCodeWriter.encode(content, BarcodeFormat.QR_CODE, width, height, hints);
            BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            Graphics2D graphics = (Graphics2D) bufferedImage.getGraphics();

            graphics.setColor(Color.WHITE);  // 确保 Color 类被正确导入
            graphics.fillRect(0, 0, width, height);

            graphics.setColor(Color.BLACK);
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    if (bitMatrix.get(x, y)) {
                        graphics.fillRect(x, y, 1, 1);
                    }
                }
            }

            Image scaledImage = bufferedImage.getScaledInstance(600, 600, Image.SCALE_SMOOTH);
            qrCodeLabel.setIcon(new ImageIcon(scaledImage));

            saveQRCodeImage(bufferedImage, "qrcode.png");
            System.out.println("二维码生成成功");
        } catch (WriterException e) {
            e.printStackTrace();
            System.out.println("二维码生成失败: " + e.getMessage());
        }
    }

    // 保存二维码图片到文件
    public void saveQRCodeImage(BufferedImage image, String fileName) {
        try {
            ImageIO.write(image, "png", new File(fileName));
            System.out.println("二维码图片保存成功");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("二维码图片保存失败: " + e.getMessage());
        }
    }

    // 追踪函数
    public static void traceInfo(String encryptedInfo) {
        if (database.containsKey(encryptedInfo)) {
            ScanInfo scanInfo = database.get(encryptedInfo);
            JOptionPane.showMessageDialog(null, "可以追踪到该扫码记录。\n时间戳: " + scanInfo.getTimestamp() +
                    "\n设备标识: " + scanInfo.getDeviceId() +
                    "\nIP 地址: " + scanInfo.getIpAddress());
        } else {
            JOptionPane.showMessageDialog(null, "未找到该扫码记录");
        }
    }

    static class ScanInfo {
        private long timestamp;
        private String deviceId;
        private String ipAddress;

        public ScanInfo(long timestamp, String deviceId, String ipAddress) {
            this.timestamp = timestamp;
            this.deviceId = deviceId;
            this.ipAddress = ipAddress;
        }

        public long getTimestamp() {
            return timestamp;
        }

        public String getDeviceId() {
            return deviceId;
        }

        public String getIpAddress() {
            return ipAddress;
        }
    }

    public static void main(String[] args) {
        new PrivacyPreservingQRCodeScanner();
    }
}