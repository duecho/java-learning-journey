package com.xpu.demo.controller;

import com.xpu.demo.model.UserInfo;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @Author: zzj
 * @Description:
 */
@RequestMapping("/response")
//@RestController
@Controller
public class ResponseController {
    //返回视图
    @RequestMapping("/index")
    public String index() {
        return "/index.html";
    }
    //返回数据
    @ResponseBody
    @RequestMapping("/returnData")
    public String returnData(){
        return "返回数据";
    }
    @ResponseBody
    @RequestMapping("/returnHTML")
    public String returnHTML(){
        return "<h1>我是HTML页面</h1>";
    }
    @ResponseBody
    @RequestMapping("/returnJson")
    public UserInfo returnJson() {
        UserInfo userInfo = new UserInfo("zhangsan","123",18);
        return userInfo;
    }
    @ResponseBody
    @RequestMapping("/setStatus")
    public String setStatus(HttpServletResponse response) {
        response.setStatus(401);
        return "设置状态码成功！";
    }
    @ResponseBody
    @RequestMapping(value = "/r1",produces = "application/json;charset=UTF-8")
    public String r1(){
        return "{\"status\":400}";
    }
    @ResponseBody
    @RequestMapping("/setHeader")
    public String setHeader(HttpServletResponse response) {
        response.setHeader("MyHeader","MyHeaderValue");
        return "设置Header成功";
    }

}
