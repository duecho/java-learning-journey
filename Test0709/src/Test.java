import java.io.BufferedReader;
import java.util.Objects;
import java.util.Scanner;

/**
 * @Author: zzj
 * @Description:
 */
public class Test {
    //两个字符串在一个字符数组之间的最小距离 -- 贪心算法 -----太难了！！！！！！！
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        String str1 = sc.next();
        String str2 = sc.next();
        int prev1 = -1;
        int prev2 = -1;
        int ret = 0x3f3f3f3f;
        String[] ch = new String[n];
        for (int i = 0; i < n; i++) {
            ch[i] = sc.next();
            if (ch[i].equals(str1)) {
                if (prev2 != -1) {
                    ret = Math.min(ret,(i-prev2));

                }
                prev1 = i ;
            }else if (ch[i].equals(str2)){
                if (prev1 != -1) {
                    ret = Math.min(ret,(i-prev1));
                }
                prev2 = i;
            }
        }
        System.out.println(ret == 0x3f3f3f3f ? -1 : ret);
    }




    //最短花费爬楼梯问题---动态规划
    public static void main3(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] cost = new int[n];
        int[] dp = new int[n+1];

        for (int i = 0; i < n; i++) {
            cost[i] = sc.nextInt();
        }
        for (int i = 2; i <= n ; i++) {
            dp[i] = Math.min(dp[i-1]+cost[i-1],dp[i-2]+cost[i-2]);
        }
        System.out.println(dp[n]);
    }




    //牛牛的快递问题
    //1kg以内20，超出每kg1元,不足按1kg算，加急5元

    public static void main2(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double a = scanner.nextDouble();
        char b = scanner.next().charAt(0);
        System.out.println(offer1(a,b));
    }
    public static int offer1(double a, char b) {
        int ret = 0;
        if (a <= 1) {
            ret = 20;
        }else {
            ret = 20 + (int) a;
        }
        if (b == 'y') {
            ret += 5;
        }
        return ret;
    }

    public static void main1(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double a = scanner.nextDouble();
        char b = scanner.next().charAt(0);
        System.out.println(offer(a,b));

    }
    public static int offer(double a, char b) {
        int ret = 20;

        if (a <= 1 && b == 'y') {
            if (a == 0) {
                return 0;
            }else {
                int m = ret + 5;
                return m;
            }
        } else if (a > 1 && b == 'y') {
            int n = ret+((int)a)+5;
            return n;
        }else if(a <= 1 && b == 'n') {
            if (a == 0) {
                return 0;
            }else {
                int c = ret;
                return c;
            }
        }else {
            int p = ret + (int) a;
            return p;
        }
    }

}
