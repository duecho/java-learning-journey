package com.xpu.blogsystem.service;

import com.xpu.blogsystem.mapper.BlogInfoMapper;
import com.xpu.blogsystem.model.BlogInfo;
import com.xpu.blogsystem.model.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Author: zzj
 * @Description:
 */
@Service
public class BlogService {
    @Autowired
    private BlogInfoMapper blogInfoMapper;

    public List<BlogInfo> getList() {
        return blogInfoMapper.selectAll();
    }

    public BlogInfo getBlogDetail(Integer blogId) {
        return blogInfoMapper.selectById(blogId);
    }

    public Integer insertBlog(BlogInfo blogInfo) {
        return blogInfoMapper.insertBlog(blogInfo);
    }

    public Integer update(BlogInfo blogInfo) {
        return blogInfoMapper.updateBlog(blogInfo);
    }

//    // 点赞功能的实现
//    public boolean likeBlog(Integer blogId) {
//        return blogInfoMapper.incrementLikes(blogId) > 0;
//    }
}