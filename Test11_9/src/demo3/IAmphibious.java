package demo3;

/**
 * @projectName: Test11_9
 * @package: demo3
 * @className: IAmphibious
 * @author: Tom of loveing coding
 * @description: TODO
 * @date: 2023/11/9 23:02
 * @version: 1.0
 */

public interface IAmphibious extends IRun,ISwim{

    void test1();
}