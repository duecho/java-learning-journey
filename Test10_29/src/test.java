import java.util.Random;
import java.util.Scanner;


public class test {




    //方法的重载
    public static int add(int x, int y){
        return x+y;
    }
    public static double add(double a, double b){
        return a+b;
    }
    public static void main(String[] args) {
        int x = 10;
        int y = 20;
        int ret1=add(x,y);
        System.out.println(ret1);
        double a = 1;
        double b = 2;
        double ret2 = add(a,b);
        System.out.println(ret2);

    }




    //方法的使用
    public static void swap(int a,int b){
        int tmp = a;
        a = b;
        b = tmp;
        System.out.println("swap: x = " + a + " y = " + b);
    }
    public static void main112(String[] args) {
        int x = 10;
        int y = 20;
        swap(x,y);
        System.out.println("main: x = " + x + " y = " + y);
    }


    public static void main111(String[] args) {
        int x = 1;
        int y = 2;
        int year = 2000;
        add(x,y);
        isLeaper(year);
        int ret = add(x,y);
        System.out.println(ret);
        boolean ret1 = isLeaper(2000);
        System.out.println(ret1);
    }

    public static boolean isLeaper(int year) {
        if ((0 == year % 4 && 0 != year % 100) || 0 == year % 400) {
            return true;
        } else {
            return false;
        }
    }

    //水仙花数
    public static void main15(String[] args) {
        for (int i = 1; i < 999999; i++) {
            int count = 0;
            int tmp = i;
            while (tmp != 0) {
                count++;
                tmp /= 10;
            }
            //i --> 0
            tmp = i; // tmp = 153
            int sum = 0;
            while (tmp != 0) {
                sum += Math.pow(tmp % 10,count);
                tmp /= 10;
            }
            if(i == sum) {
                System.out.println(i);
            }
        }
    }

    //求两个数的最大公约数
    //
    public static void main14(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = a % b;
        while (c != 0) {
            a = b;
            b = c;
            c = a % b;
        }
        System.out.println(b);
    }


    //二进制的奇数和偶数序列输出
    public static void main11(String[] args) {
        int n = 7;
        for (int i = 0; i < 32; i += 2){
            System.out.print(((n>>i) & 1)+" ");
        }//偶数
        System.out.println();
        for (int i = 1; i < 32; i += 2){
            System.out.print(((n >> i) & 1)+" ");

        }//奇数
        System.out.println();

    }



    //二进制中1的个数
    public static void main9(String[] args) {
        int n=7;
        int count =0;
        while(n != 0){
            count++;
            n = n & (n-1);
        }
        /*for(int i=0;i<31;i++){
            if(((n>>i & 1) !=0)){
                count++;
            }
        }*/
        System.out.println(count);
    }




    //乘法口诀表
    public static void main8(String[] args) {
        for(int i =1;i <=9;i++){
            for(int j =1;j<=i;j++) {
                System.out.print(j+"*"+i + "=" + (i * j)+"\t");
            }
            System.out.println();
        }

    }



    public static void main7(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();
        int randNum = random.nextInt(100);
        while(true){
          /*  int count=3;
            for(count=3;;count--){*/
            System.out.println("请输入你要猜的数字：");
            int num = scanner.nextInt();
            if(num<randNum){
                System.out.println("猜小了");
            } else if (num>randNum) {
                System.out.println("猜大了");
            } else
                System.out.println("猜对了");
            break;
        }

    }



    public static void main5(String[] args) {
        int i;
        int count=0;
        for(i=1;i <=100;i++){
            if(i%10==9){
                count++;
            }
            if(i/10==9){
                count++;
            }
        }
        System.out.println(count);
    }





    public static void main4(String[] args) {
        for(int year = 1000;year <= 2000;year++)
            if (year % 4 ==0 && year % 100 !=0 || year %400 ==0){
                System.out.println(year);
            }
    }



    public static void main2(String[] args) {
        for (int n=1;n<=100;n++){
            int i=0;
            for (i=2 ; i <= Math.sqrt(n) ; i++){
                if(n%i == 0) {
                    /*System.out.println("n不是素数： "+ n);*/
                    break;
                }
            }
            if(i > Math.sqrt(n)){
                System.out.println(n+"是素数");
            }
        }

    }



    public static void main1(String[] args) {
        int n=1;
        for(n=1;n<=100;n++){
            if(n%3==0&&n%5==0){
                System.out.println(n);
                continue;
            }
            n++;
        }
    }


    public static void main3(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int i;
        for ( i=2 ; i<n ; i++){
            if(n%i == 0) {
                System.out.println("n不是素数： "+ n);
                break;
            }
        }
        if(i >= n){
            System.out.println(n+"是素数");
        }

    }
}
