package com.xpu.demo.service;

import com.xpu.demo.mapper.MessageInfoMapper;
import com.xpu.demo.model.MessageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MessageInfoService {
    @Autowired
    private MessageInfoMapper messageInfoMapper;

    public Integer insertMessage(MessageInfo messageInfo) {
        return messageInfoMapper.insertMessage(messageInfo);
    }

    public List<MessageInfo> queryMessage() {
        return messageInfoMapper.queryMessage();
    }
}