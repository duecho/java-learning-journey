package com.xpu.blogsystem.controller;

import com.xpu.blogsystem.model.BlogInfo;
import com.xpu.blogsystem.service.BlogService;
import com.xpu.blogsystem.utils.JwtUtils;
import io.jsonwebtoken.lang.Assert;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: zzj
 * @Description:
 */
@Slf4j
@RequestMapping("/blog")
@RestController
public class BlogController {
    @Autowired
    private BlogService blogService;

    @RequestMapping("/getList")
    public List<BlogInfo> getList(){
        return blogService.getList();
    }

    @RequestMapping("/getBlogDetail")
    public BlogInfo getBlogDetail(Integer blogId,HttpServletRequest request){
        log.info("getBlogDetail 接收参数: blogId:"+blogId);
        Assert.notNull(blogId, "blogId不能为null");
        BlogInfo blogInfo = blogService.getBlogDetail(blogId);
        //作者ID
        Integer authorId = blogInfo.getUserId();
        //登录用户
        //获取userID
        String userToken = request.getHeader("user_token_header");
        //从token中获取用户ID
        Integer userId = JwtUtils.getUserIdFromToken(userToken);
        if (authorId!=null && authorId==userId){
            blogInfo.setIsLoginUser(true);
        }

        return blogInfo;
    }
    /**
     * 发布博客
     */
    @RequestMapping("/add")
    public Boolean addBlog(@Validated @RequestBody BlogInfo blogInfo, HttpServletRequest request){
        log.info("addBlog 接收参数: "+ blogInfo);
        //获取userID
        String userToken = request.getHeader("user_token_header");
        //从token中获取用户ID
        Integer userId = JwtUtils.getUserIdFromToken(userToken);
        blogInfo.setUserId(userId);
        Integer result = null;
        try {
            result = blogService.insertBlog(blogInfo);
            if (result==1){
                return true;
            }
        }catch (Exception e){
            log.error("发布博客失败, e:", e);
        }
        return false;
    }
    /**
     * 更新博客
     */
    @RequestMapping("/update")
    public Boolean updateBlog(@Validated @RequestBody BlogInfo blogInfo){
        log.info("updateBlog 接收参数: "+ blogInfo);
        Integer result =null;
        try {
            result = blogService.update(blogInfo);
            if (result>=1){
                return true;
            }

        }catch (Exception e){
            log.error("updateBlog 发生异常, e: ", e);
        }
        return false;
    }

    @RequestMapping("/delete")
    public Boolean deleteBlog(Integer blogId){
        Assert.notNull(blogId, "删除的博客ID不能为null");
        log.info("deleteBlog 接收参数: "+ blogId);
        Integer result = null;
        try {
            BlogInfo blogInfo = new BlogInfo();
            blogInfo.setId(blogId);
            blogInfo.setDeleteFlag(1);
            result = blogService.update(blogInfo);
            if (result>=1){
                return true;
            }
        }catch (Exception e){
            log.error("deleteBlog 发生异常, e: ", e);
        }
        return false;
    }

//    /**
//     * 点赞功能的接口
//     */
//    @PostMapping("/like")
//    public Map<String, Object> likeBlog(@RequestParam Integer blogId) {
//        Assert.notNull(blogId, "blogId 不能为 null");
//        log.info("likeBlog 接收参数: " + blogId);
//        Integer result = null;
//        Map<String, Object> response = new HashMap<>();
//        try {
//            boolean success = blogService.likeBlog(blogId);
//            response.put("success", success);
//            if (success) {
//                BlogInfo blogInfo = blogService.getBlogDetail(blogId);
//                response.put("message", "点赞成功");
//                response.put("likes", blogInfo.getLikes());
//            } else {
//                response.put("message", "点赞失败");
//            }
//        } catch (Exception e) {
//            log.error("likeBlog 发生异常, e: ", e);
//            response.put("success", false);
//            response.put("message", "点赞失败，发生异常");
//        }
//        return response;
//    }
}