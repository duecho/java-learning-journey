public class Date {
    public int year;//年
    public int month;//月
    public int day;//日

    public Date() {
        this.year = 2023;
        this.month = 11;
        this.day = 2;
        System.out.println("执行了不带参数的构造方法");
    }
    public  Date(int year,int month,int day) {
        this.year = 2023;
        this.month = 11;
        this.day = 2;
        System.out.println("执行了带参数的构造方法");
    }

    //局部变量优先使用     谁调用当前这个方法  谁就是this
    public void setDate(int y,int m,int d) {
        year = y;
        month = m;
        day = d;
    }
    public void setDate1(int y,int m,int d) {
        this.year = y;
        this.month = m;
        this.day = d;
    }
    public void printDate() {
        System.out.println("年: "+this.year+" 月: "+this.month+" 日："+this.day);
    }

    public static void main(String[] args) {
        Date date1 = new Date();
        date1.setDate(1945,11,2);
        date1.printDate();
        Date date = new Date();
        Date date2 = new Date(2023,11,2);


        /*Date date2 = new Date();
        date.setDate(1988,11,2);
        date.printDate();*/
    }
}
