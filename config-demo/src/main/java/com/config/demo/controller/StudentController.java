package com.config.demo.controller;

import com.config.demo.model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: zzj
 * @Description:
 */
@RestController
public class StudentController {
    @Autowired
    public Student student;
    @RequestMapping("/readStudent")
    public String readStudent() {
        return student.toString();
    }

}
