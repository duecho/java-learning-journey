import java.util.*;

/**
 * @Author: zzj
 * @Description:
 */

class Student {
    public String id;

    public Student(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id='" + id + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return Objects.equals(id, student.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
public class Test {


    public static void main6(String[] args) {
        Map<String,Integer> map = new HashMap<>();
        Set<String> set = new HashSet<>();
        HashMap<String,Integer> map2 = new HashMap<>();
     }
    public static void main3(String[] args) {
        Student student = new Student("1515");
        int hashcode1 = student.hashCode();
        System.out.println(hashcode1);

        Student student2 = new Student("1515");
        int hashcode2 = student2.hashCode();
        System.out.println(hashcode2);

        HashBuck2<Student,Integer> hashBuck2 = new HashBuck2<>();
        hashBuck2.push(student,123);
        System.out.println(hashBuck2.get(student2));


    }
    public static void main4(String[] args) {
        HashBuck hashBuck = new HashBuck();
        hashBuck.push(4,888);
        hashBuck.push(1,888);
        hashBuck.push(2,888);
        hashBuck.push(3,888);
        hashBuck.push(5,888);
        hashBuck.push(6,888);
        hashBuck.push(14,5555);
        hashBuck.push(7,7777);
        System.out.println("=======");
        System.out.println(hashBuck.get(14));



    }
    public static void main2(String[] args) {
        Set<String> set = new TreeSet<>();
        set.add("hello");
        set.add("hihihi");
        set.add("hahaha");
        System.out.println(set);
        for (String x : set) {
            System.out.println(x);
        }
    }
    public static void main(String[] args) {
        Map<String,Integer> map = new TreeMap<>();
        map.put("abc",4);
        map.put("hahaha",2);
        map.put("zzz",3);
        map.put("zzz",4);
        Integer val = map.getOrDefault("abc",99);
        System.out.println(val);

        Set<String> set = map.keySet();
        System.out.println("set: " +set);
        Set<Map.Entry<String,Integer>> set1 = map.entrySet();
        for (Map.Entry<String,Integer> entry:set1) {
            System.out.println("key: " + entry.getKey()+" "+"value: " + entry.getValue());
        }
    }
}
