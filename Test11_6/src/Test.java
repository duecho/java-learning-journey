import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;

 class animals {
    public String name;
    public int age;
    public String color;
    public void sleep() {
        System.out.println(this.name+" 正在睡觉。。。。。");
    }
}
 class Dog extends animals {
/*    public String name;
    public int age;
    public String color;*/
    public void eat() {
        System.out.println(this.name+" 正在吃。。。。。");
    }
    /*public void sleep() {
        System.out.println(this.name+" 正在睡觉。。。。。");
    }*/

}

 class Cat extends animals {
   /* public String name;
    public int age;
    public String color;*/
    public void bark() {
        System.out.println(this.name+" 正在喵喵叫。。。。。");
    }
    /*public void sleep() {
        System.out.println(this.name+" 正在睡觉。。。。。");
    }*/
}

public class Test {
    public static void main(String[] args) {
        Dog dog = new Dog();
        dog.name = "多多";
        dog.age = 3;
        dog.color = "白色";
        dog.eat();
        Cat cat = new Cat();
        cat.name = "咪咪";
        cat.age = 3;
        cat.color = "蓝色";
        cat.bark();
    }
}

