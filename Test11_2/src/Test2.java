class MyValue {
    public int val;
}
public class Test2 {
    public static void swap(MyValue val1,MyValue val2) {
        int tmp = val1.val;
        val1.val = val2.val;
        val2.val = tmp;
    }
    public static void main(String[] args) {
        MyValue myValue1 = new MyValue();
        myValue1.val = 10;
        MyValue myValue2 = new MyValue();
        myValue2.val = 20;
        System.out.println("交换前："+myValue1.val);
        System.out.println("交换前："+myValue2.val);

        swap(myValue1,myValue2);

        System.out.println("交换后："+myValue1.val);
        System.out.println("交换后："+myValue2.val);
    }
}
