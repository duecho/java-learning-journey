import java.util.Arrays;
import java.util.Scanner;

public class Work {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] array = {2, 3, 5, 4, 5, 3, 4};
        boolean result = threeConsecutiveOdds(array);
        System.out.println("数组的判断结果为：" + result);
    }
    public static boolean threeConsecutiveOdds(int[] arr) {
        // 引入标志位记录连续出现奇数的个数
        int count = 0;
        for (int i = 0; i < arr.length; i++) {
            if (isConsecutiveOdd(arr[i])) {
                // 出现奇数，count ++;
                count ++;
                if (count == 3) {
                    // 出现连着三个奇数，返回true
                    return true;
                }
            }else {
                // 碰到偶数，count重置
                count = 0;
            }
        }
        return false;
    }

    private static boolean isConsecutiveOdd(int num) {
        return num % 2 != 0;
    }
    public static void main333(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] nums = {2, 3, 5, 4, 5, 3, 4};
        int result = majorityElement(nums);
        System.out.println("大于n/2的数是: " + result);
    }
    public static int majorityElement(int[] nums) {
        Arrays.sort(nums);
        return nums[nums.length/2];
    }
    public static void main443(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] nums = {2, 3, 5, 4, 5, 3, 4};
        int result = singleNumber(nums);
        System.out.println("The single number is: " + result);
    }
    public static int singleNumber(int[] nums) {
        // 用异或运算的性质可以巧妙的解决这个问题，因为数组中只有一个数字出现一次
        // 则其他出现两次的数字用异或运算后都是0，最终整个数组异或运算的结果即为所求。
        int ret = 0;
        for (int i : nums) {
            ret ^= i;
        }
        return ret;
    }
    public static void main111(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] nums = {2, 7, 11, 15};
        int target = 9;
        int[] result = twoSum(nums, target);
        System.out.println("Indices: " + result[0] + ", " + result[1]);
    }
    public static int[] twoSum(int[] nums, int target) {
        int[] result = new int[2];
        // 双指针i和j，i从前向后遍历，j从后向i遍历，若arr[i]+arr[j]=target,即为题解
        for (int i = 0; i < nums.length - 1; i++) {
            for (int j = nums.length - 1; j > i; j--) {
                if (nums[i] + nums[j] == target) {
                    result[0] = i;
                    result[1] = j;
                }
            }
        }
        return result;
    }
    public static void main22(String[] args) {
        int[] arr = {1, 2, 3, 4, 5};
        func4(arr);
        for (int num : arr) {
            System.out.print(num + " ");
        }
    }
    public static void func4(int[] array) {
        int i = 0;
        int j = array.length-1;
        while (i < j) {

            while (i < j && array[i] % 2 != 0) {
                i++;
            }
            while (i < j && array[j] % 2 == 0) {
                j--;
            }
            int tmp = array[i];
            array[i] = array[j];
            array[j] = tmp;
        }

    }
    public static void main8(String[] args) {
        int[] arr = {1, 2, 3, 4, 5};
        int sum = add(arr);
        System.out.println("数组元素之和为： " + sum);
    }
    public static int add(int[] array) {
        int sum =  0;
        for (int i = 0; i < array.length; i++) {
            sum = sum+array[i];
        }
        return sum;

    }
    public static void main33(String[] args) {
        int[] array = new int[100];
        for (int i = 0; i < array.length; i++) {
            array[i] = i + 1;
        }
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
    }

   /* public static void main(String[] args) {
        System.out.println("hello world");
    }
*/
        public static void main2(String[]args){
            int[] arr = {1, 2, 3};
            func3(arr);
            for (int i : arr) {
                System.out.print(i + " ");
            }
        }

        public static int[] func3 ( int[] array){

            for (int i = 0; i < array.length; i++) {
                array[i] *= 2;
            }
            return array;
        }
}