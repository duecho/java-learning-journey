package com.xpu.demo.controller;

import com.xpu.demo.model.MessageInfo;
import com.xpu.demo.service.MessageInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: zzj
 * @Description:
 */
@RequestMapping("/message")
@RestController
public class MessageController {
    @Autowired
    private MessageInfoService messageInfoService;
    @RequestMapping(value = "/publish",method = RequestMethod.POST)
    public Boolean publish(@RequestBody MessageInfo messageInfo) {
        System.out.println("发表留言");
        messageInfoService.insertMessage(messageInfo);
        return true;
    }
    @RequestMapping("/getList")
    public List<MessageInfo> getList() {
        return messageInfoService.queryMessage();
    }
}
