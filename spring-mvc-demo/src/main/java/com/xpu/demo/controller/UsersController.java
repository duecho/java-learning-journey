package com.xpu.demo.controller;

import jakarta.servlet.http.HttpSession;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: zzj
 * @Description:
 */
@RequestMapping("/users")
@RestController
public class UsersController {
    @RequestMapping("/login")
    public Boolean login(String userName, String password, HttpSession session) {
//        if (StringUtils.hasLength(userName) && StringUtils.hasLength(password)) {
//            if ("zhangsan".equals(userName) && "123456".equals(password)) {
//                return true;
//            }
//        }
        if (!StringUtils.hasLength(userName) || !StringUtils.hasLength(password)) {
            return false;
        }
        //校验账号和密码
        if ("zhangsan".equals(userName) && "123456".equals(password)) {
            //存储Session
            session.setAttribute("userName",userName);
                return true;
        }
        return false;
    }
    @RequestMapping("/getUserInfo")
    public String getUserInfo(HttpSession session) {
        String userName = (String) session.getAttribute("userName");
        System.out.println("登录用户："+userName);
        return userName == null ? "" : userName;
    }
}
