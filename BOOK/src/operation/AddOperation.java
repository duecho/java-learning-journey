package operation;

import book.Book;
import book.BookList;

import java.util.Scanner;

/**
 * @Author: zzj
 * @Description:
 */
public class AddOperation implements IOOperation {
    public void work(BookList bookList) {
        System.out.println("新增图书！！！");
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入您要新增图书的书名：");
        String name = scanner.nextLine();
        System.out.println("请输入您要新增图书的作者：");
        String author = scanner.nextLine();
        System.out.println("请输入您要新增图书的价格：");
        int price = scanner.nextInt();
        System.out.println("请输入您要新增图书的类型：");
        scanner.nextLine();//此处Line和Int发生冲突，解决办法有三种
        //1.将Int放在最后能顺利解决   2.将nextLine改为next  3. 多设置一次nextLine,将空格跳过
        String type = scanner.nextLine();


        Book book = new Book(name,author,price,type);
        int currentSize = bookList.getUsedSize();

        for(int i = 0; i <currentSize; i++) {
            Book tmp = bookList.getBook(i);
            if (tmp.getName().equals(name)) {
                System.out.println("这本书存在，不能重复添加！！！");
                return;
            }
        }
        //新增图书
        bookList.setBook(book,currentSize);
        bookList.setUsedSize(currentSize+1);


    }
}
