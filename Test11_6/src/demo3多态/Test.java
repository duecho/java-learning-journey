package demo3多态;

/**
 * @Author 12629
 * @Description：
 */
class Animal {
    public String name;
    public int age;
    public Animal(String name, int age) {
        this.name = name;
        this.age = age;
        System.out.println("Animal(String name, int age)");
    }

    public void eat() {
        System.out.println(this.name+"正在吃.....");
    }
}

class Dog extends Animal {
    public String color;

    public Dog(String name,int age,String color) {
        //调用父类的 带有2个参数的构造方法 来初始化父类当中成员 必须在第一行 所以，super()和this() 是不能同时存在的
        super(name,age);
        this.color = color;
        System.out.println("Dog(String name,int age,String color)");
    }
    public void barks() {
        System.out.println(this.name+" 正在叫.....");
    }

    public void eat() {
        System.out.println(this.name+" 正在吃狗粮......");
    }
}


public class Test {
    //什么是向上转型 -》 发生多态的条件之一
    public static void main(String[] args) {
        /*Dog dog = new Dog("旺财",2,"红色");
        Animal animal = dog;*/
        //父类引用 引用了子类对象
        Animal animal = new Dog("旺财",2,"红色");
        animal.eat();
        //animal.barks();//通过Animal这个引用 只能调用Animal自己的属性
    }
    public static void main1(String[] args) {
        Dog dog = new Dog("旺财",2,"红色");
        dog.eat();
        dog.barks();

        Animal animal = new Animal("动物",3);
        animal.eat();
        //animal.barks();//通过Animal这个引用 只能调用Animal自己的属性

    }
}