package demo3;

/**
 * @projectName: Test11_9
 * @package: demo3
 * @className: ISwim
 * @author: Tom of loveing coding
 * @description: TODO
 * @date: 2023/11/9 23:04
 * @version: 1.0
 */
public interface ISwim {
    void swim();
}