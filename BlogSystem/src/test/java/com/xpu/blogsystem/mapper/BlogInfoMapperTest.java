package com.xpu.blogsystem.mapper;

import com.xpu.blogsystem.model.BlogInfo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @Author: zzj
 * @Description:
 */
@SpringBootTest
class BlogInfoMapperTest {
    @Autowired
    private BlogInfoMapper blogInfoMapper;

    @Test
    void selectAll() {
        blogInfoMapper.selectAll().forEach(System.out::println);
    }

    @Test
    void selectById() {
        System.out.println(blogInfoMapper.selectById(2));
    }

    @Test
    void insertBlog() {
        BlogInfo blogInfo = new BlogInfo();
        blogInfo.setTitle("测试");
        blogInfo.setContent("博客内容");
        blogInfo.setUserId(1);
        blogInfoMapper.insertBlog(blogInfo);
    }

    @Test
    void updateBlog() {
        BlogInfo blogInfo = new BlogInfo();
        blogInfo.setId(3);
        blogInfo.setTitle("hahaha");
        blogInfo.setContent("博客内容哈哈哈");
        blogInfoMapper.updateBlog(blogInfo);
    }
    @Test
    void deleteBlog() {
        BlogInfo blogInfo = new BlogInfo();
        blogInfo.setId(3);
        blogInfo.setDeleteFlag(1);
        blogInfoMapper.updateBlog(blogInfo);
    }
}