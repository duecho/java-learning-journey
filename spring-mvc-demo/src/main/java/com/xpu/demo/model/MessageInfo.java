package com.xpu.demo.model;

import lombok.Data;

/**
 * @Author: zzj
 * @Description:
 */
@Data
public class MessageInfo {
    private Integer id;
    private String from;
    private String to;
    private String message;
    private Integer deleteFlag;
    private String createTime;
    private String updateTime;
}
