package demo1;

/**
 * &#064;projectName:  Test11_9
 * &#064;package:  demo1
 * &#064;className:  Test
 * &#064;author:  Tom of loving coding
 * &#064;description:  TODO
 * &#064;date:  2023/11/9 22:51
 * &#064;version:  1.0
 */
abstract class Shape {
    /*public static int count = 0;
    public int size=10;
    public void test() {
    }*/
    public abstract void draw();
}

class Rect extends Shape {

    @Override
    public void draw() {
        System.out.println("画矩形");
    }
}
class Cycle extends Shape {
    @Override
    public void draw() {
        System.out.println("画圆圈");
    }
}

class Flower extends Shape {
    @Override
    public void draw() {
        System.out.println("画❀");
    }
}


abstract class Person {
    public String name;
    public int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }
}
class Student extends Person {
    public Student() {
        super("zhangsan",10);
    }
}


public class Test {
    public static void func(Shape shape) {
        shape.draw();
    }

    public static void main(String[] args) {
        //Shape shape = new Shape();
        func(new Flower());
        func(new Cycle());
        func(new Rect());
    }
}