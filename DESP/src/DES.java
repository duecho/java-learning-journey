import java.util.Arrays;

public class DES {
    private static final int[] IP = {
            58, 50, 42, 34, 26, 18, 10, 2,
            60, 52, 44, 36, 28, 20, 12, 4,
            62, 54, 46, 38, 30, 22, 14, 6,
            64, 56, 48, 40, 32, 24, 16, 8,
            57, 49, 41, 33, 25, 17, 9, 1,
            59, 51, 43, 35, 27, 19, 11, 3,
            61, 53, 45, 37, 29, 21, 13, 5,
            63, 55, 47, 39, 31, 23, 15, 7
    };

    private static final int[] FP = {
            40, 8, 48, 16, 56, 24, 64, 32,
            39, 7, 47, 15, 55, 23, 63, 31,
            38, 6, 46, 14, 54, 22, 62, 30,
            37, 5, 45, 13, 53, 21, 61, 29,
            36, 4, 44, 12, 52, 20, 60, 28,
            35, 3, 43, 11, 51, 19, 59, 27,
            34, 2, 42, 10, 50, 18, 58, 26,
            33, 1, 41, 9, 49, 17, 57, 25
    };

    private static final int[] E = {
            32, 1, 2, 3, 4, 5, 4, 5,
            6, 7, 8, 9, 8, 9, 10, 11,
            12, 13, 12, 13, 14, 15, 16, 17,
            16, 17, 18, 19, 20, 21, 20, 21,
            22, 23, 24, 25, 24, 25, 26, 27,
            28, 29, 28, 29, 30, 31, 32, 1
    };

    private static final int[][] S_BOX = {
            {
                    14, 4, 13, 1, 2, 15, 11, 8, 3, 10, 6, 12, 5, 9, 0, 7,
                    0, 15, 7, 4, 14, 2, 13, 1, 10, 6, 12, 11, 9, 5, 3, 8,
                    4, 1, 14, 8, 13, 6, 2, 11, 15, 12, 9, 7, 3, 10, 5, 0,
                    15, 12, 8, 2, 4, 9, 1, 7, 5, 11, 3, 14, 10, 0, 6, 13
            },
            {
                    15, 1, 8, 14, 6, 11, 3, 4, 9, 7, 2, 13, 12, 0, 5, 10,
                    3, 13, 4, 7, 15, 2, 8, 14, 12, 0, 1, 10, 6, 9, 11, 5,
                    0, 14, 7, 11, 10, 4, 13, 1, 5, 8, 12, 6, 9, 3, 2, 15,
                    13, 8, 10, 1, 3, 15, 4, 2, 11, 6, 7, 12, 0, 5, 14, 9
            },
            {
                    10, 0, 9, 14, 6, 3, 15, 5, 1, 13, 12, 7, 11, 4, 2, 8,
                    13, 7, 0, 9, 3, 4, 6, 10, 2, 8, 5, 14, 12, 11, 15, 1,
                    13, 6, 4, 9, 8, 15, 3, 0, 11, 1, 2, 12, 5, 10, 14, 7,
                    1, 10, 13, 0, 6, 9, 8, 7, 4, 15, 14, 3, 11, 5, 2, 12
            },
            {
                    7, 13, 14, 3, 0, 6, 9, 10, 1, 2, 8, 5, 11, 12, 4, 15,
                    13, 8, 11, 5, 6, 15, 0, 3, 4, 7, 2, 12, 1, 10, 14, 9,
                    10, 6, 9, 0, 12, 11, 7, 13, 15, 1, 3, 14, 5, 2, 8, 4,
                    3, 15, 0, 6, 10, 1, 13, 8, 9, 4, 5, 11, 12, 7, 2, 14
            },
            {
                    2, 12, 4, 1, 7, 10, 11, 6, 8, 5, 3, 15, 13, 0, 14, 9,
                    14, 11, 2, 12, 4, 7, 13, 1, 5, 0, 15, 10, 3, 9, 8, 6,
                    4, 2, 1, 11, 10, 13, 7, 8, 15, 9, 12, 5, 6, 3, 0, 14,
                    11, 8, 12, 7, 1, 14, 2, 13, 6, 15, 0, 9, 10, 4, 5, 3
            },
            {
                    12, 1, 10, 15, 9, 2, 6, 8, 0, 13, 3, 4, 14, 7, 5, 11,
                    10, 15, 4, 2, 7, 12, 9, 5, 6, 1, 13, 14, 0, 11, 3, 8,
                    9, 14, 15, 5, 2, 8, 12, 3, 7, 0, 4, 10, 1, 13, 11, 6,
                    4, 3, 2, 12, 9, 5, 15, 10, 11, 14, 1, 7, 6, 0, 8, 13
            },
            {
                    4, 11, 2, 14, 15, 0, 8, 13, 3, 12, 9, 7, 5, 10, 6, 1,
                    13, 0, 11, 7, 4, 9, 1, 10, 14, 3, 5, 12, 2, 15, 8, 6,
                    1, 4, 11, 13, 12, 3, 7, 14, 10, 15, 6, 8, 0, 5, 9, 2,
                    6, 11, 13, 8, 1, 4, 10, 7, 9, 5, 0, 15, 14, 2, 3, 12
            },
            {
                    13, 2, 8, 4, 6, 15, 11, 1, 10, 9, 3, 14, 5, 0, 12, 7,
                    1, 15, 13, 8, 10, 3, 7, 4, 12, 5, 6, 11, 0, 14, 9, 2,
                    7, 11, 4, 1, 9, 12, 14, 2, 0, 6, 10, 13, 15, 3, 5, 8,
                    2, 1, 14, 7, 4, 10, 8, 13, 15, 12, 9, 0, 3, 5, 6, 11
            }
    };

    private static final int[] P = {
            16, 7, 20, 21,
            29, 12, 28, 17,
            1, 15, 23, 26,
            5, 18, 31, 10,
            2, 8, 24, 14,
            32, 27, 3, 9,
            19, 13, 30, 6,
            22, 11, 4, 25
    };

    private static final int[] PC1 = {
            57, 49, 41, 33, 25, 17, 9,
            1, 58, 50, 42, 34, 26, 18,
            10, 2, 59, 51, 43, 35, 27,
            19, 11, 3, 60, 52, 44, 36,
            63, 55, 47, 39, 31, 23, 15,
            7, 62, 54, 46, 38, 30, 22,
            14, 6, 61, 53, 45, 37, 29,
            21, 13, 5, 28, 20, 12, 4
    };

    private static final int[] PC2 = {
            14, 17, 11, 24, 1, 5,
            3, 28, 15, 6, 21, 10,
            23, 19, 12, 4, 26, 8,
            16, 7, 27, 20, 13, 2,
            41, 52, 31, 37, 47, 55,
            30, 40, 51, 45, 33, 48,
            44, 49, 39, 56, 34, 53,
            46, 42, 50, 36, 29, 32
    };

    private static final int[] LEFT_SHIFTS = {
            1, 1, 2, 2, 2, 2, 2, 2,
            1, 2, 2, 2, 2, 2, 2, 1
    };

    public static void main(String[] args) {
        String key = "133457799BBCDFF1";
        String message = "0123456789ABCDEF";

        byte[] encrypted = desEncrypt(hexStringToByteArray(message), hexStringToByteArray(key));
        System.out.println("解密: " + byteArrayToHexString(encrypted));

        byte[] decrypted = desDecrypt(encrypted, hexStringToByteArray(key));
        System.out.println("加密: " + byteArrayToHexString(decrypted));
    }

    public static byte[] desEncrypt(byte[] message, byte[] key) {
        byte[] paddedMessage = padMessage(message);
        byte[] initialPermutation = permute(paddedMessage, IP);
        byte[] subKeys = generateSubKeys(key);
        byte[] result = desRound(initialPermutation, subKeys, true);
        return permute(result, FP);
    }

    public static byte[] desDecrypt(byte[] message, byte[] key) {
        byte[] initialPermutation = permute(message, IP);
        byte[] subKeys = generateSubKeys(key);
        byte[] result = desRound(initialPermutation, subKeys, false);
        return permute(result, FP);
    }

    private static byte[] permute(byte[] input, int[] table) {
        byte[] output = new byte[table.length / 8];
        for (int i = 0; i < table.length; i++) {
            int bit = (input[(table[i] - 1) / 8] >> (7 - ((table[i] - 1) % 8))) & 0x01;
            output[i / 8] |= bit << (7 - (i % 8));
        }
        return output;
    }

    private static byte[] generateSubKeys(byte[] key) {
        byte[] permutedKey = permute(key, PC1);
        byte[] left = Arrays.copyOfRange(permutedKey, 0, permutedKey.length / 2);
        byte[] right = Arrays.copyOfRange(permutedKey, permutedKey.length / 2, permutedKey.length);
        byte[] subKeys = new byte[16 * 6];

        for (int round = 0; round < 16; round++) {
            left = leftShift(left, LEFT_SHIFTS[round]);
            right = leftShift(right, LEFT_SHIFTS[round]);
            byte[] combined = new byte[left.length + right.length];
            System.arraycopy(left, 0, combined, 0, left.length);
            System.arraycopy(right, 0, combined, left.length, right.length);
            byte[] subKey = permute(combined, PC2);
            System.arraycopy(subKey, 0, subKeys, round * 6, 6);
        }

        return subKeys;
    }

    private static byte[] leftShift(byte[] block, int shifts) {
        int len = block.length * 8;
        for (int i = 0; i < shifts; i++) {
            int carry = (block[0] >> 7) & 0x01;
            for (int j = 0; j < block.length; j++) {
                int nextCarry = (block[j] >> 7) & 0x01;
                block[j] = (byte) ((block[j] << 1) & 0xFE);
                block[j] |= carry;
                carry = nextCarry;
            }
            block[block.length - 1] &= (0xFF << (8 - (len % 8)));
        }
        return block;
    }

    private static byte[] desRound(byte[] block, byte[] subKeys, boolean encrypt) {
        byte[] left = Arrays.copyOfRange(block, 0, block.length / 2);
        byte[] right = Arrays.copyOfRange(block, block.length / 2, block.length);

        for (int round = 0; round < 16; round++) {
            byte[] temp = right;
            right = xor(left, feistelFunction(right, subKeys, encrypt, round));
            left = temp;
        }

        byte[] combined = new byte[block.length];
        System.arraycopy(right, 0, combined, 0, right.length);
        System.arraycopy(left, 0, combined, right.length, left.length);
        return combined;
    }

    private static byte[] feistelFunction(byte[] block, byte[] subKeys, boolean encrypt, int round) {
        byte[] expandedBlock = permute(block, E);
        byte[] subKey = Arrays.copyOfRange(subKeys, round * 6, (round + 1) * 6);
        byte[] xorResult = xor(expandedBlock, subKey);
        byte[] sBoxResult = sBoxSubstitution(xorResult);
        return permute(sBoxResult, P);
    }

    private static byte[] xor(byte[] a, byte[] b) {
        byte[] result = new byte[a.length];
        for (int i = 0; i < a.length; i++) {
            result[i] = (byte) (a[i] ^ b[i]);
        }
        return result;
    }

    private static byte[] sBoxSubstitution(byte[] block) {
        byte[] result = new byte[4];
        for (int i = 0; i < 8; i++) {
            int row = ((block[i / 6] >> (7 - (i % 6))) & 0x01) << 1;
            row |= (block[(i / 6) + 1] >> (7 - (i % 6))) & 0x01;
            int col = (block[i / 6] >> (6 - (i % 6))) & 0x0F;
            int value = S_BOX[i][row * 16 + col];
            result[i / 2] |= value << (4 * (1 - (i % 2)));
        }
        return result;
    }

    private static byte[] padMessage(byte[] message) {
        int paddedLength = ((message.length + 7) / 8) * 8;
        byte[] paddedMessage = new byte[paddedLength];
        System.arraycopy(message, 0, paddedMessage, 0, message.length);
        return paddedMessage;
    }

    private static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

    private static String byteArrayToHexString(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (byte b : bytes) {
            sb.append(String.format("%02X", b));
        }
        return sb.toString();
    }
}
