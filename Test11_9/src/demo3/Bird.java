package demo3;

/**
 * @projectName: Test11_9
 * @package: demo3
 * @className: Bird
 * @author: Tom of loveing coding
 * @description: TODO
 * @date: 2023/11/9 22:59
 * @version: 1.0
 */
public class Bird extends Animal implements IFly,IRun{

    public Bird(String name) {
        super(name);
    }

    @Override
    public void fly() {
        System.out.println(this.name+" 正在用两个翅膀飞！");
    }

    @Override
    public void run() {
        System.out.println(this.name+" 正在用两个小腿腿跑！");
    }

    @Override
    public void eat() {
        System.out.println(this.name+" 正在吃鸟粮...");
    }
}
