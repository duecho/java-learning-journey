package com.xpu.demo.controller;

import com.xpu.demo.model.UserInfo;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @Author: zzj
 * @Description:
 */
@RequestMapping("/request")
@RestController
public class RequestController {
    @RequestMapping("/param1")
    public String param1(String userName) {
        System.out.println("接收到参数："+userName);
        return "接收到参数" + userName;
    }
    @RequestMapping("/param2")
    public String param2(Integer age) {
        return "2接收到参数" + age;
    }
    @RequestMapping("/param3")
    public String param3(int age) {
        return "3接收到参数" + age;
    }
    @RequestMapping("/param4")
    public String param4(String userName,String password) {
        return "4接收到参数userName" + userName+",password: "+ password;
    }
    @RequestMapping("/param5")
    public String param5(UserInfo userInfo) {
        return "5接收到参数userInfo" +userInfo;
    }
    @RequestMapping("/param6")
    public String param6(@RequestParam(value = "userName",required = false) String Name, String password) {
        return "4接收到参数userName" + Name+",password: "+ password;
    }
    @RequestMapping("/param7")
    public String param7(String[] arrParam) {
        return "接收到参数arrParam:" + Arrays.toString(arrParam);
    }
    @RequestMapping("/param8")
    public String param8(ArrayList<String> listParam) {
        return "接收到参数listParam"+listParam;
    }
    @RequestMapping("/param9")
    public String param9(@RequestParam("listParam") List<String> listParam) {
        return "接收到参数listParam"+listParam;
    }
    @RequestMapping("/param10")
    public String param10(@RequestBody UserInfo userInfo) {
        return "接收到参数userInfo"+userInfo;
    }
    @RequestMapping("/param11/{articleId}")
    public String param11(@PathVariable("articleId") Integer articleId) {
        return "接收到参数articleId"+articleId;
    }
    @RequestMapping("/param12/{id}/{name}")
    public String param12(@PathVariable("id") Integer id,@PathVariable("name") String name) {
        return "接收到参数id:"+ id + ",name:"+name;
    }
    @RequestMapping("/param13")
    public String param13(MultipartFile file) throws IOException {
        System.out.println(file.getName());
        System.out.println(file.getContentType());
        System.out.println(file.getOriginalFilename());
        file.transferTo(new File("D:/temp/"+file.getOriginalFilename()));
        return "接收到文件file:"+ file.getOriginalFilename();
    }
    @RequestMapping("/param14")
    public String getCookie(HttpServletRequest request) {
        Cookie[] cookies = request.getCookies();
        System.out.println("获取Cookie信息：");
        if (cookies != null) {
            for (Cookie cookie : cookies) {
//            if ("bite".equals(cookie.getName())) {
//
//            }
                System.out.println(cookie.getName()+":"+cookie.getValue());
            }

        }
        return "获取cookies成功";
    }
    @RequestMapping("/param15")
    public String getCookie2(@CookieValue ("bite") String bite) {
        System.out.println("从cookie中获取cookie:"+bite);
        return "获取成功";
    }
    @RequestMapping("/param16")
    public String setSession(HttpServletRequest request) {
        HttpSession session = request.getSession(true);
        session.setAttribute("name","张三");
        session.setAttribute("Id","100");
        return "设置Session成功！";
    }
    //获取Session
    @RequestMapping("/getSession")
    public String getSession(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session != null) {
            System.out.println(session.getAttribute("name"));
            System.out.println(session.getAttribute("Id"));
        }
        return "获取Session成功！";
    }
    @RequestMapping("/getSession2")
    public String getSession2(HttpSession session) {
        System.out.println("name:"+session.getAttribute("name"));
        System.out.println("Id"+session.getAttribute("Id"));
        return "获取Session成功";
    }
    @RequestMapping("/getSession3")
    public String getSession3(@SessionAttribute(value = "name",required = false) String name) {
        System.out.println("name"+name);
        return "获取session成功";
    }
    @RequestMapping("/getHeader")
    public String getHeader(HttpServletRequest request) {
        String userAgent = request.getHeader("User-Agent");
        return "userAgent:"+userAgent;
    }
    @RequestMapping("/getHeader2")
    public String getHeader2(@RequestHeader("User-Agent") String userAgent) {
        return "userAgent:"+userAgent;
    }

}
