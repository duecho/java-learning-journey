package operation;

import book.BookList;

/**
 * @Author: zzj
 * @Description:
 */
public class ExitOperation implements IOOperation {
    public void work(BookList bookList) {
        System.out.println("退出系统！！！");
        System.exit(0);
        //这里要对bookList进行资源的手动回收
    }
}
