import com.mysql.cj.jdbc.MysqlDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

/**
 * @Author: zzj
 * @Description:
 */
public class JDBCDemo {
    public static void main(String[] args) throws SQLException {

        Scanner scanner = new Scanner(System.in);
        //1.创建数据源
        DataSource dataSource = new MysqlDataSource();
        ((MysqlDataSource) dataSource).setUrl("jdbc:mysql://127.0.0.1:3306/java111?characterEncoding=UTF-8&&useSSL=false");
        ((MysqlDataSource) dataSource).setUser("root");
        ((MysqlDataSource) dataSource).setPassword("zzj20020515");
        //// 创建数据库连接
        //Connection connection =
        //DriverManager.getConnection("jdbc:mysql://localhost:3306/test?
        //user=root&password=root&useUnicode=true&characterEncoding=UTF-8");
        //2.和数据库建立连接
        Connection connection = dataSource.getConnection();
        //3.构造操作数据库的sql语句
        System.out.println("请输入学号：");
        int id = scanner.nextInt();
        System.out.println("请输入姓名：");
        String name = scanner.next();
//        String sql = "insert into test values(" + id + ",'"+ name +"')";//此处代码可读性很差，不安全，可能引起sql注入漏洞
        String sql = "insert into test values(?,?)";
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setInt(1,id);
        preparedStatement.setString(2,name);//下标从1开始
        //4.执行sql,将解析好的语句发给数据库
        int n = preparedStatement.executeUpdate();
        System.out.println("n = " + n);
        //5.执行完毕，收尾操作，释放前面创建的各种资源
        //主要释放语句对象和连接对象，DataSource不必释放
        preparedStatement.close();
        connection.close();
    }
}
