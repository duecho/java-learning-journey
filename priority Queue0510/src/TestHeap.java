import java.util.Arrays;

/**
 * @Author: zzj
 * @Description:
 */
public class TestHeap {

    //堆的创建
    public int[] elem;
    public int usedSize;

    public TestHeap() {
        this.elem = new int[10];
    }

    public void initElem(int[] array) {
        for (int i = 0; i < array.length; i++) {
            elem[i] = array[i];
            usedSize++;
        }
    }

    public void createBigHeap() {
        for (int parent = (usedSize-1)/2; parent >= 0 ; parent--) {
            siftDown(parent,usedSize);
        }
    }
    private void siftDown(int parent,int end) {
        int child = 2*parent + 1;
        while (child < end) {
            if (child+1 < usedSize && elem[child] < elem[child+1]) {
                child++;
            }
            if (elem[child] >elem[parent]) {
                swap(child,parent);
                parent =child;
                child = 2*parent + 1;
            }else {
                break;
            }
        }
    }

    private void swap(int i,int j) {
        int tmp = elem[i];
        elem[i] = elem[j];
        elem[j] = tmp;
    }

    public int poll() {
        int tmp = elem[0];
        swap(0,usedSize-1);
        usedSize--;
        siftDown(0,usedSize);
        return tmp;

    }
    public int peek() {
        return elem[0];
    }

    public void offer(int val) {
        //1、判断满
        if (isFull()) {
            //扩容
            this.elem = Arrays.copyOf(elem,2*elem.length);
        }
        //插入元素
        elem[usedSize] = val;
        usedSize++;
        //3、开始向上调整
        siftUp(usedSize-1);
    }
    private void siftUp(int child) {
        int parent = (child-1)/2;
        while (child > 0) {
            if (elem[child] > elem[parent]) {
                swap(child,parent);
                child = parent;
                parent = (child-1)/2;
            }
        }
    }
    public boolean isFull() {
        return usedSize == elem.length;
    }
    //O(N*logn)   空间复杂度O(1)
    //堆排序
    public void heapSort() {
        int end = usedSize-1;
        while (end > 0) {
            swap(0,end);
            siftDown(0,end-1);
            end--;
        }
    }

}
