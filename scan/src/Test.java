/**
 * @projectName: scan
 * @package: PACKAGE_NAME
 * @className: Test
 * @author: Tom of loveing coding
 * @description: TODO
 * @date: 2024/1/8 14:45
 * @version: 1.0
 */

import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Scanner;

public class Test {
    public static void main(String[] args) {
        String host = "localhost";

        Scanner scanner = new Scanner(System.in);
        int numPorts;
        System.out.print("输入要扫描端口的数量: ");
        numPorts = scanner.nextInt();

        for (int i = 0; i < numPorts; i++) {
            System.out.print("输入端口号" + (i+1) + ": ");
            int targetPort = scanner.nextInt();

            System.out.print("端口扫描 " + targetPort + " ");
            for (int j = 0; j < 10; j++) {
                try {
                    System.out.print(".");
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println();

            try {
                Socket socket = new Socket();
                socket.connect(new InetSocketAddress(host, targetPort), 1000);
                socket.close();
                System.out.println("端口 " + targetPort + " 已开放");

                // Get the service name using the port
                String serviceName = getServiceName(targetPort);
                System.out.println("端口 " + targetPort + " 正在被使用 " + serviceName);
            } catch (Exception e) {
                System.out.println("端口 " + targetPort + " 已关闭或无法到达！");
            }
        }
    }

    private static String getServiceName(int port) {
        switch (port) {
            case 20:
                return "TFTP (File Transfer Protocol)";
            case 21:
                return "FTP (File Transfer Protocol)";
            case 22:
                return "SSH (Secure Shell)";
            // Add more cases for other well-known ports
            default:
                return "Unknown service";
        }
    }
}

