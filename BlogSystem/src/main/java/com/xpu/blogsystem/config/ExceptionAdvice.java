package com.xpu.blogsystem.config;

import com.xpu.blogsystem.enums.ResultCodeEnum;
import com.xpu.blogsystem.model.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.resource.NoResourceFoundException;

import javax.naming.AuthenticationNotSupportedException;

/**
 * @Author: zzj
 * @Description:
 */
@Slf4j
@RestControllerAdvice
public class ExceptionAdvice {
    @ExceptionHandler
    public Result handler(Exception e){
        log.error("发生异常, e:", e);
        return Result.fail(e.getMessage());
    }


    @ExceptionHandler
    public Result handler(IllegalArgumentException e){
        log.error("发生异常, e:", e.getMessage());
        return Result.fail(ResultCodeEnum.PARAM_INVALID.getCode(), e.getMessage());
    }

    @ResponseStatus(code = HttpStatus.NOT_FOUND)
    @ExceptionHandler
    public Result handler(NoResourceFoundException e){
        log.error("文件不存在, e:", e.getResourcePath());
        return Result.fail(ResultCodeEnum.NO_RESOURCES.getCode(), e.getResourcePath());
    }

    @ExceptionHandler
    public Result handler(MethodArgumentNotValidException e){
        log.error("参数校验不通过, e:", e.getMessage());
        return Result.fail(ResultCodeEnum.PARAM_INVALID.getCode(), e.getMessage());
    }
}