package com.xpu.blogsystem.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.xpu.blogsystem.utils.DateUtils;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

import java.util.Date;

/**
 * @Author: zzj
 * @Description:
 */

@Data
public class BlogInfo {
    private Integer id;
    @NotBlank(message = "title不能为空")
    private String title;
    @NotBlank(message = "content 不能为空")
    private String content;
    private Integer likes;
    private Integer userId;
    private Integer deleteFlag;
    //当前作者是否为登录用户
    private Boolean isLoginUser;

    private Date createTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;

//    public String getUpdateTime() {
//        return DateUtils.format(updateTime);
//    }


}