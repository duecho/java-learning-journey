package com.xpu.springioc.ioc.v2;

/**
 * @Author: zzj
 * @Description:
 */
public class Framework {
    private Bottom bottom;

    public Framework(Bottom bottom) {
        this.bottom = bottom;
        System.out.println("Framework init....");
    }
}
