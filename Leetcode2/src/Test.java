/**
 * @Author: zzj
 * @Description:
 */
public class Test {
    class ListNode {
        int val;
        ListNode next;
        ListNode(int x) { val = x; }
    }

    //分割链表

        public ListNode partition(ListNode pHead, int x) {

            ListNode lessHead = new ListNode(0);
            ListNode greaterEqualHead = new ListNode(0);
            ListNode lessPtr = lessHead;
            ListNode greaterEqualPtr = greaterEqualHead;

            ListNode cur = pHead;
            while (cur != null) {
                if (cur.val < x) {
                    lessPtr.next = cur;
                    lessPtr = lessPtr.next;
                } else {
                    greaterEqualPtr.next = cur;
                    greaterEqualPtr = greaterEqualPtr.next;
                }
                cur = cur.next;
            }

            greaterEqualPtr.next = null;
            lessPtr.next = greaterEqualHead.next;

            ListNode newHead = lessHead.next;
            lessHead.next = null;
            greaterEqualHead.next = null;

            return newHead;
        }
        // write code here


    //判断回文链表

    public class PalindromeList {

        public boolean chkPalindrome( ListNode head) {
            if (head == null || head.next == null) {
                return true;
            }
            //1.找中间节点
            ListNode fast =head;
            ListNode slow = head;
            while (fast != null && fast.next != null) {
                fast =fast.next.next;
                slow = slow.next;
            }

            //2.翻转
            ListNode cur = slow.next;
            while (cur != null) {
                ListNode curNext = cur.next;
                cur.next = slow;
                slow = cur;
                cur = curNext;
            }

            //3.一个往前，一个往后
            while (slow != head) {
                if (slow.val != head.val) {
                    return false;
                }
                if (head.next == slow) {
                    return true;       //偶数情况
                }
                head = head.next;
                slow = slow.next;
            }
            return true;
        }
    }
    //判断环形链表
    public class Solution {
        public boolean hasCycle(ListNode head) {
            if (head == null || head.next == null) {
                return false;
            }

            ListNode slow = head;
            ListNode fast = head.next;

            while (slow != fast) {
                if (fast == null || fast.next == null) {
                    return false;
                }
                slow = slow.next;
                fast = fast.next.next;
            }

            return true;
        }
    }

    //判断链表相交
    public class Solution3 {
        public ListNode getIntersectionNode(ListNode headA, ListNode headB) {
            int lenA = 0;
            int lenB = 0;


            ListNode pl = headA;
            ListNode ps = headB;
            while(pl != null) {
                lenA++;
                pl = pl.next;
            }
            while(ps != null) {
                lenB++;
                ps = ps.next;
            }


            pl = headA;
            ps = headB;
            int len = lenA - lenB;
            if(len < 0) {
                pl = headB;
                ps = headA;
                len = lenB- lenA;
            } //保证指向正确，len为正数
            while(len != 0) {
                pl = pl.next;
                len--;
            }
            //保证pl ps 将来相遇 的路程一样
            while(pl != null && ps != null && pl != ps) {
                pl = pl.next;
                ps = ps.next;
            }
            if(pl == ps && pl == null) {
                return null;
            }
            return pl;
        }
    }

    //判断环形链表入口点
    public class Solution2 {
        public ListNode detectCycle(ListNode head) {
            if (head == null || head.next == null) {
                return null;
            }

            // 使用快慢指针判断是否存在环
            ListNode slow = head;
            ListNode fast = head;
            boolean hasCycle = false;
            while (fast != null && fast.next != null) {
                slow = slow.next;
                fast = fast.next.next;
                if (slow == fast) {
                    hasCycle = true;
                    break;
                }
            }

            // 如果不存在环,直接返回null
            if (!hasCycle) {
                return null;
            }

            // 找到环的入口节点
            slow = head;
            while (slow != fast) {
                slow = slow.next;
                fast = fast.next;
            }

            return slow;
        }
    }
}
