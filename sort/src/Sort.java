import java.util.Stack;

/**
 * @Author: zzj
 * @Description:
 */
public class Sort {


    /*
    时间复杂度：最好情况：O(N)--数据有序  ;;  最坏情况下：O(N^2) --数据逆序
    空间复杂度：O(1)
    稳定性：稳定的  //本身稳定，可实现不稳定/本身不稳定，不能实现
    特点：数据有序，插入越快，效率越高
     */
    public static void insertSort(int[] array) {
        for (int i = 1; i < array.length; i++) {
            int tmp = array[i];
            int j = i - 1;
            for (  ; j >= 0; j--) {
                if (array[j] > tmp) {
                    array[j+1] = array[j];
                }else {
                    break;
                }
            }
            array[j+1] = tmp;

        }
    }

    //不稳定的排序
    public static void shellSort(int[] array) {
        int gap = array.length;
        while(gap > 1) {
            gap = gap / 3 + 1;
            shell(array,gap);
        }

    }

    private static void shell(int[] array,int gap) {
        for (int i = gap; i < array.length; i++) {
            int tmp = array[i];
            int j = i - gap;
            for (  ; j >= 0; j-=gap) {
                if (array[j] > tmp) {
                    array[j+gap] = array[j];
                }else {
                    break;
                }
            }
            array[j+gap] = tmp;

        }
    }
    //选择排序 时间复杂度：O（N^2）  空间复杂度：O（1） 稳定性：不稳定

    public  static void selectSort(int[] array) {
        for (int i = 0; i < array.length; i++) {
            int minindex = i;
            int j = i+1;
            for (; j < array.length; j++) {
                if (array[j] < array[minindex]) {
                    minindex = j;
                }
            }
            swap(array,i,minindex);
        }
    }

    private static void swap(int[] array,int i,int j) {
        int tmp = array[j];
        array[j] = array[i];
        array[i] = tmp;

    }

    //第二种选择排序
    public static void selectSort2(int[] array) {
        int left = 0;
        int right = array.length-1;
        while (left < right) {
            int minIndex = left;
            int maxIndex = left;
            for (int i = left+1; i <= right; i++) {
                if (array[minIndex] > array[i]) {
                    minIndex = i;
                }
                if (array[maxIndex] < array[i]) {
                    maxIndex = i;
                }

            }
            swap(array,minIndex,left);
            //第一次交换，最大值可能被换走
            if (maxIndex == left) {
                maxIndex = minIndex;
            }
            swap(array,maxIndex,right);
            left++;
            right--;
        }
    }
    /*
    堆排序
    时间复杂度：O（N*logN）
    空间复杂度：o(1)
    稳定性：不稳定的
     */
    public static void heapSort(int[] array) {
        //创建大根堆
        createHeap(array);
        int end =array.length-1;
        while (end > 0) {
            swap(array,0,end);
            siftDown(array,0,end);
            end--;
        }

    }
    private static void createHeap(int[] array) {
        for (int parent = (array.length-1-1)/2; parent >= 0; parent--) {
            siftDown(array,parent,array.length);
        }
    }

    private static void siftDown(int[] array, int parent, int len) {
        int child = (2 * parent) + 1;
        while (child < len) {
            if (child + 1 < len && array[child] < array[child + 1]) {
                child = child + 1;
            }
            if (array[child] > array[parent]) {
                swap(array, child, parent);
                parent = child;
                child = 2 * parent + 1;
            } else {
                break;
            }
        }
    }

    /**
     * 冒泡排序
     * @param array
     */

    public static void bubbleSort(int[] array) {
        //i代表趟数
        for (int i = 0; i < array.length-1; i++) {
            boolean flg = false;
            for (int j = 0; j < array.length-1; j++) {
                if (array[j] > array[j+1]) {
                    swap(array,j,j+1);
                    flg = true;
                }
            }
            if (flg == false) {
                return;
            }
        }
    }

    /**
     * 快速排序 时间复杂度： 最好：O(N*logN)  最坏：O(N^2) 有序/逆序
     * 空间复杂度：最好：O(logN)  最坏：O（N）
     * 稳定性：不稳定的排序
     * @param array
     */

    public static void quickSort(int[] array) {
        quick(array,0,array.length-1);
    }
    private static void quick(int[] array,int start,int end) {
        if (start >= end) {
            return;
        }
        if (end - start+1 <= 10) {
            insertSort2(array,start,end);
            return;
        }
        //1.三数取中
        int index = middleNum(array,start,end);
        swap(array,index,start);
        int pivot = partitionHoare(array,start,end);
        quick(array,start,pivot-1);
        quick(array,pivot+1,end);
    }
    public static void insertSort2(int[] array,int start,int end) {
        for (int i = start+1; i < end; i++) {
            int tmp = array[i];
            int j = i - 1;
            for (  ; j >= start; j--) {
                if (array[j] > tmp) {
                    array[j+1] = array[j];
                }else {
                    break;
                }
            }
            array[j+1] = tmp;

        }
    }
    private static int middleNum(int[] array, int left,int right) {
        int mid = left+((right - left)>>1);
        if (array[left] < array[right]) {
            if (array[mid] <array[left]) {
                return left;
            } else if (array[mid] > array[right]) {
                return right;

            }else {
                return mid;
            }
        }else {
            if (array[mid] < array[right]) {
                return right;
            }else if (array[mid] > array[left]) {
                return left;
            }else {
                return mid;
            }
        }
    }
    //找基准
    //Hoare法
    private static int partitionHoare(int[] array, int left,int right) {
        int tmp = array[left];
        int i = left;
        while (left < right) {

            while (left < right && array[right] >= tmp) {
                right--;
            }
            while(left < right && array[left] <= tmp) {
                left++;
            }
            swap(array,left,right);
        }
        swap(array,i,left);
        return left;
    }
    //挖坑法
    private static int partition(int[] array, int left,int right) {
        int tmp = array[left];
        while (left < right) {
            while (left < right && array[right] >= tmp) {   //此处需有等号，不然会死循环
                right--;
            }
            array[left] = array[right];
            while (left < right && array[left] <= tmp) {
                left++;
            }
            if (left >= right) {
                break;
            }
            array[right] = array[left];
        }
        array[left] = tmp;
        return left;

    }
    //找基准时，以左边为基准时，先走右边是因为如果先走左边，最后相遇的位置比基准大
    //前后指针
    private static int partion1(int[] array,int left,int right) {
        int prev = left;
        int cur = left+1;
        while (cur <= right) {
            if (array[cur] < array[left] && array[++prev] != array[cur]) {
                swap(array,cur,prev);
            }
            cur++;
        }
        swap(array,prev,left);
        return prev;
    }

    //非递归快排
    public static void quickSortNor(int[] array) {
        int left = 0;
        int right = array.length-1;
        int pivot = partition(array,left,right);
        Stack<Integer> stack = new Stack<>();
        if (pivot-1 > left) {
            stack.push(left);
            stack.push(pivot-1);
        }
        if (pivot+1 < right) {
            stack.push(right);
            stack.push(pivot+1);
        }
        while (!stack.isEmpty()) {
            right = stack.pop();
            left =stack.pop();
            pivot = partition(array,left,right);
            if (pivot-1 > left) {
                stack.push(left);
                stack.push(pivot-1);
            }
            if (pivot+1 < right) {
                stack.push(pivot+1);
                stack.push(right);

            }
        }
    }

    //归并排序

    /**
     * 时间复杂度：O(N*logN)
     * 空间复杂度：O(N)
     * 稳定性：稳定
     * 稳定的排序：插入、冒泡、归并
     * @param array
     */
    public static void mergeSort(int[] array) {
        mergeFunc(array,0,array.length-1);
    }
    private static void mergeFunc(int[] array,int left,int right) {
        if (left >= right) {
            return;
        }
        int mid = left+((right-left) >> 1);
        mergeFunc(array,left,mid);
        mergeFunc(array,mid+1,right);
        //左右分解完，开始合并
        merge(array,left,mid,right);

    }
    //合并算法
    private static void merge(int[] array,int left,int mid,int right) {
        int s1 = left;
        int e1 = mid;
        int s2 = mid+1;
        int e2 = right;
        int[] tmpArr = new int[right-left+1];
        int k = 0;
        //保证两个表里都有数据
        while (s1 <= e1 && s2 <= e2) {
            if (array[s1] <= array[s2]) {
                tmpArr[k++] = array[s1++];
            }else {
                tmpArr[k++] = array[s2++];
            }
        }
        //看哪个数组，还有数据
        while (s1 <= e1) {
            tmpArr[k++] = array[s1++];
        }
        while (s2 <= e2) {
            tmpArr[k++] = array[s2++];
        }
        //拷贝到源数组
        for (int i = 0; i < k; i++) {
            array[i+left] =tmpArr[i];
        }
    }
    /**
     * 非递归归并排序
     */
    public static void mergeSortNor(int[] array) {
        int gap = 1;
        //最外层控制组数
        while (gap < array.length) {
            //每一组进行排序
            for (int i = 0; i < array.length; i=i+2*gap) {
                int left = i;
                int mid = left + gap -1;
                //处理边界，避免数组越界
                if (mid >= array.length) {
                    mid = array.length-1;
                }
                int right = mid + gap;
                if (right >= array.length) {
                    right = array.length-1;
                }
                merge(array,left,mid,right);
            }
            gap *= 2;
        }
    }

    /**
     * 时间复杂度：O(范围+N)
     * 范围=max-min -->最好数据集中一点
     * 是稳定的
     * @param array
     */

    public static void countSort(int[] array) {
        int min = array[0];
        int max = array[0];
        for (int i = 0; i < array.length; i++) {
            if (min > array[i]) {
                min = array[i];
            }
            if (max < array[i]) {
                max = array[i];
            }
        }
        //定义计数数组，进行初始化
        int[] count = new int[max-min+1];
        for (int i = 0; i < array.length; i++) {
            int index = array[i]-min;
            count[index]++;
        }

        //遍历计数数组
        int k = 0;//表示array数组的下标
        for (int i = 0; i < count.length; i++) {
            while (count[i] != 0) {
                array[k] = i + min;
                k++;
                count[i]--;
            }
        }
    }



}
