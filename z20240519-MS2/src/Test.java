import java.util.*;

/**
 * @Author: zzj
 * @Description:
 */
public class Test {

    //查询每个单词出现的次数

    public static Map<Integer,Integer> countNum(int[] array) {
        Map<Integer,Integer> map = new HashMap<>();
        for (int i = 0; i < array.length; i++) {
            if (map.get(array[i]) == null) {
                map.put(array[i],1);
            }else {
                int val = map.get(array[i]);
                map.put(array[i],val+1);
            }
        }
        return map;
    }
    //前K个高频单词
    public List<String> topKFrequent(String[] words, int k) {
        //统计每个单词出现的次数
        Map<String,Integer> map = new HashMap<>();
        for(String str : words) {
            if (map.get(str) == null) {
                map.put(str,1);
            }else {
                int val = map.get(str);
                map.put(str,val+1);
            }
        }
        //遍历Map 把前K个元素，放到小根堆中
        PriorityQueue<Map.Entry<String,Integer>> minHeap = new PriorityQueue<>(
                new Comparator<Map.Entry<String, Integer>>() {
                    @Override
                    public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
                        if (o1.getValue().compareTo(o2.getValue()) == 0) {
                            //存储的时候，应该找Key变为大根堆存储

                            return o2.getKey().compareTo(o1.getKey());
                        }
                        return o1.getValue().compareTo(o2.getValue());
                    }
                });
        for (Map.Entry<String,Integer> entry : map.entrySet()) {
            if (minHeap.size() < k) {
                //直接存到小根堆中
                minHeap.offer(entry);
            }else {
                Map.Entry<String,Integer> top = minHeap.peek();
                if (top.getValue().compareTo(entry.getValue()) < 0) {
                    minHeap.poll();
                    minHeap.offer(entry);
                }else {
                    if (top.getValue().compareTo(entry.getValue()) == 0) {
                        if (top.getKey().compareTo(entry.getKey()) > 0) {
                            //频率一样，字典序小的需要入堆
                            minHeap.poll();
                            minHeap.offer(entry);
                        }
                    }
                }
            }
        }
        List<String> ret = new ArrayList<>();
        for (int i = 0; i < k; i++) {
            Map.Entry<String,Integer> top = minHeap.poll();
            ret.add(top.getKey());//默认是尾插
        }
        Collections.reverse(ret);
        return ret;
    }

    public static void main1(String[] args) {
        int[] array = {1,2,2,3,3,3,4,4,6,6,67,77,0};
        Map<Integer,Integer> map = countNum(array);
        for (Map.Entry<Integer,Integer> entry : map.entrySet()) {
            System.out.println("数字："+entry.getKey()+" 出现了："+entry.getValue()+" 次！");
        }

    }

    //给你海量的数据，统计每个数字出现了多少次？  ---使用map


    //坏了的键盘，两次set使用


//        import java.util.Scanner;
//import java.util.*;

// 注意类名必须为 Main, 不要有任何 package xxx 信息
//public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        while (in.hasNextLine()) { // 注意 while 处理多个 case
            String str1 = in.nextLine();
            String str2 = in.nextLine();
            func(str1, str2);
        }

    }

    private static void func(String str1, String str2) {
        Set<Character> set = new HashSet<>();
        for (char s : str2.toUpperCase().toCharArray()) {
            set.add(s);
        }
        Set<Character> setBroken = new HashSet<>();
        for (char s : str1.toUpperCase().toCharArray()) {
            if (!set.contains(s) && !setBroken.contains(s)) {
                System.out.print(s);
                setBroken.add(s);
            }
        }
    }
}




    //随机链表的复制
    //1.使用map来做
        /*
        class Solution {
    public Node copyRandomList(Node head) {
        Map<Node,Node> map = new HashMap<>();
        Node cur = head;
        while(cur != null) {
            Node node = new Node(cur.val);
            map.put(cur,node);
            cur = cur.next;
        }
        cur = head;
        while(cur != null) {
            map.get(cur).next = map.get(cur.next);
            map.get(cur).random = map.get(cur.random);
            cur = cur.next;
        }
        return map.get(head);
    }
}
         */
    //利用链表构造来做
        /*
        class Solution {
    public Node copyRandomList(Node head) {
        if(head==null) {
            return null;
        }
        Node p = head;
        //第一步，在每个原节点后面创建一个新节点
        //1->1'->2->2'->3->3'
        while(p!=null) {
            Node newNode = new Node(p.val);
            newNode.next = p.next;
            p.next = newNode;
            p = newNode.next;
        }
        p = head;
        //第二步，设置新节点的随机节点
        while(p!=null) {
            if(p.random!=null) {
                p.next.random = p.random.next;
            }
            p = p.next.next;
        }
        Node dummy = new Node(-1);
        p = head;
        Node cur = dummy;
        //第三步，将两个链表分离
        while(p!=null) {
            cur.next = p.next;
            cur = cur.next;
            p.next = cur.next;
            p = p.next;
        }
        return dummy.next;
    }
}

         */
//}
