/**
 * @Author: zzj
 * @Description:
 */
public class Test {
    public static void main2(String[] args) {
        BinaryTree binaryTree = new BinaryTree();
        BinaryTree.TreeNode root = binaryTree.createTree();
        //System.out.println(root.val);
        binaryTree.preOrder(root);
//        System.out.println();
        System.out.println();
        binaryTree.inOrder(root);
        System.out.println();
        binaryTree.postOrder(root);
        System.out.println();
        System.out.println("层序遍历：");
        binaryTree.levelOrder(root);
        System.out.println();
        boolean flg = binaryTree.isCompleteTree(root);
        System.out.println(flg);

    }
    public static void main(String[] args) {
        BinaryTree binaryTree = new BinaryTree();
        BinaryTree.TreeNode root = binaryTree.createTree();
        //System.out.println(root.val);
        binaryTree.preOrder(root);
//        System.out.println();
        System.out.println();
        binaryTree.inOrder(root);
        System.out.println();
        binaryTree.postOrder(root);
        System.out.println();
        System.out.println("节点的个数：");
        binaryTree.size(root);
        System.out.println(binaryTree.nodeSize);
        System.out.println();
        System.out.println("节点的个数：");
        binaryTree.size2(root);
        System.out.println(binaryTree.size2(root));
        System.out.println("叶子节点的个数：");
        binaryTree.getLeafNodeCount(root);
        System.out.println(binaryTree.leafSize);
        binaryTree.getLeafNodeCount2(root);
        System.out.println(binaryTree.getLeafNodeCount2(root));
        System.out.println("第"+"k"+"层的节点个数为：");
        System.out.println(binaryTree.getKLevelNodeCount(root,3));
        System.out.println("二叉树的高度为：");
        System.out.println(binaryTree.getHeight(root));
        System.out.println("查找节点：");
        BinaryTree.TreeNode ret = binaryTree.find(root,'G');
        System.out.println(ret.val);

    }
}
