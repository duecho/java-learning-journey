/**
 * @Author: zzj
 * @Description:
 */

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import java.util.Base64;

public class Test {

    public static void main(String[] args) throws Exception {
        String plainText = "Hello, World!";
        String key = "12345678";

        // Create DES key
        DESKeySpec desKeySpec = new DESKeySpec(key.getBytes());
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
        SecretKey secretKey = keyFactory.generateSecret(desKeySpec);

        // Create DES cipher object for encryption
        Cipher desCipherEncrypt = Cipher.getInstance("DES/ECB/PKCS5Padding");
        desCipherEncrypt.init(Cipher.ENCRYPT_MODE, secretKey);

        // Encryption
        byte[] encryptedBytes = desCipherEncrypt.doFinal(plainText.getBytes());
        String encryptedText = Base64.getEncoder().encodeToString(encryptedBytes);
        System.out.println("Encrypted Text: " + encryptedText);

        // Create DES cipher object for decryption
        Cipher desCipherDecrypt = Cipher.getInstance("DES/ECB/PKCS5Padding");
        desCipherDecrypt.init(Cipher.DECRYPT_MODE, secretKey);

        // Decryption
        byte[] decryptedBytes = desCipherDecrypt.doFinal(Base64.getDecoder().decode(encryptedText));
        String decryptedText = new String(decryptedBytes);
        System.out.println("Decrypted Text: " + decryptedText);
    }
}
