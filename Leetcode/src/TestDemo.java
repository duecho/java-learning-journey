/**
 * @Author: zzj
 * @Description:
 */
public class TestDemo {
    static class ListNode{
        public int val;
        public ListNode next;
        public ListNode(int val) {
            this.val = val;
        }
    }
    public ListNode head; //链表的属性，链表头节点 //null


    //杨辉三角-----------力扣
//    class Solution4 {
//        public List<List<Integer>> generate(int numRows) {
//            List<List<Integer>> ret = new ArrayList<>();
//            List<Integer> list = new ArrayList<>();
//            list.add(1);//每一行的第一个元素
//            ret.add(list);
//            for (int i = 1; i < numRows; i++) {
//                //每一次循环就是一行
//                List<Integer> curRow = new ArrayList<>();
//                curRow.add(1);//每一行的第一个元素
//
//                //处理中间的数字
//                List<Integer> prevRow = ret.get(i-1);
//                for (int j = 1; j < i; j++) {
//                    int x = prevRow.get(j) + prevRow.get(j-1);
//                    curRow.add(x);
//                }
//                //处理最后一个数字
//                curRow.add(1);
//                ret.add(curRow);
//            }
//            return ret;
//        }
//    }


    //合并两个有序数组---------力扣
    class Solution3 {
        public void merge(int[] nums1, int m, int[] nums2, int n) {
            int index1 = m-1;
            int index2 = n-1;
            int indexMerge = m+n-1;
            while (index1 >= 0 && index2 >= 0) {
                if(nums1[index1] > nums2[index2]) {
                    nums1[indexMerge] = nums1[index1];
                    index1--;
                }else {
                    nums1[indexMerge] = nums2[index2];
                    index2--;
                }
                indexMerge--;
            }
            while(index2 >= 0) {
                nums1[indexMerge] = nums2[index2];
                index2--;
                indexMerge--;
            }

        }
    }


    //删除有序数组中的重复项-------力扣

    class Solution1 {
        public int removeDuplicates(int[] nums) {
            if(nums.length == 0) {
                return 0;
            }
            int index = 1;
            for(int i = 1; i < nums.length; i++) {
                if(nums[i] != nums[i-1]) {
                    nums[index] = nums[i];
                    index++;
                }
            }
            return index;

        }
    }

    //力扣-移除元素
    class Solution {
        public int removeElement(int[] nums, int val) {
            int i = 0;
            for(int j = 0; j < nums.length; j++ ) {
                if(nums[j] != val) {
                    nums[i] = nums[j];
                    i++;
                }
            }
            return i;
        }
    }

    //删除所有相同的链表元素
    class Solution2 {
        public ListNode removeElements(ListNode head,int val) {
            if (head == null) {
                return null;
            }
            ListNode prev = head;
            ListNode cur = head.next;
            while (cur != null) {
                if (cur.val == val) {
                    prev.next = cur.next;
                    cur = cur.next;
                }else {
                    prev.next = cur;
                    cur = cur.next;
                }
            }
            return head;
        }
    }

    //反转链表
    class Solution11 {
        public ListNode reverseList(ListNode head) {
            if(head == null) {
                return head;
            }
            if(head.next == null) {
                return head;
            }
            ListNode cur = head.next;
            head.next = null;
            while(cur != null) {
                ListNode curNext = cur.next;
                cur.next = head;
                head = cur;
                cur = curNext;
            }
            return head;
        }
    }

    //返回链表的中间节点
    class Solution112 {
        public ListNode middleNode(ListNode head) {
            ListNode fast = head;
            ListNode slow = head;
            while(fast != null && fast.next != null) {
                slow = slow.next;
                fast = fast.next.next;
            }
            return slow;
        }
    }


}
