package demo2;

class Shape {
    public void draw() {
        System.out.println("画图形！");
    }
}
class Cycle extends Shape {
    @Override
    public void draw() {
        System.out.println("○");

    }
}
class Rect extends Shape {
    @Override
    public void draw() {
        System.out.println("矩形");
    }
}
class Triangle extends Shape{
    @Override
    public void draw() {
        System.out.println("△");
    }
}
class Flower extends Shape {
    @Override
    public void draw() {
        System.out.println("❀");
    }
}
public class Test {

    public static void main(String[] args) {
        /*Rect rect = new  Rect();
        Cycle cycle = new Cycle();
        Triangle triangle = new Triangle();

        Shape[] shapes = {cycle,rect,cycle,rect,triangle};
*/

        Shape rect = new  Rect();
        Shape cycle = new Cycle();
        Shape triangle = new Triangle();
        Shape flower = new Flower();

        Shape[] shapes = {cycle,rect,cycle,rect,triangle,flower};

        for(Shape shape : shapes) {
            shape.draw();
        }
    }

    public static void main2(String[] args) {
        Rect rect = new  Rect();
        Cycle cycle = new Cycle();
        Triangle triangle = new Triangle();

        String[] strings = {"1","2","1","2","3"};
        for(String s : strings) {
            if(s.equals("1")) {
                cycle.draw();
            }else if(s.equals("2")) {
                rect.draw();
            }else {
                triangle.draw();
            }
        }
    }

    public static void drawMap(Shape shape) {
        shape.draw();
    }

    public static void main1(String[] args) {
        Rect rect = new  Rect();
        Cycle cycle = new Cycle();
        Triangle triangle = new Triangle();

        drawMap(rect);
        drawMap(cycle);
        drawMap(triangle);
    }
}