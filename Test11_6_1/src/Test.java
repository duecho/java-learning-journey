/*class Base {
    Base() {
        System.out.print("Base");
    }
}*/
/*class Base{
    public Base(String s){
        System.out.print("B");
    }
}

public class Test extends Base{
    public Test (String s) {
        System.out.print("D");
    }
    public static void main(String[] args){
        new Derived("C");
    }
}*/

   /* public class Test extends Base {

        public static void main( String[] args ) {
            new Test();//1
            //调用父类无参的构造方法
            new Base();//2
        }
    }*/

class X{
    Y y=new Y();//1
    public X(){//2
        System.out.print("X");
    }
}
class Y{
    public Y(){//3
        System.out.print("Y");
    }
}
public class Test extends X{
    Y y=new Y();//4
    public Test(){//5
        System.out.print("Z");
    }
    public static void main1(String[] args) {
        new Test();
    }
}