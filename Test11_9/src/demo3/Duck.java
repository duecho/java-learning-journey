package demo3;

/**
 * @projectName: Test11_9
 * @package: demo3
 * @className: Duck
 * @author: Tom of loveing coding
 * @description: TODO
 * @date: 2023/11/9 23:01
 * @version: 1.0
 */
public class Duck extends Animal implements IFly,IRun,ISwim{

    public Duck(String name) {
        super(name);
    }

    @Override
    public void eat() {
        System.out.println(this.name+" 正在吃鸭粮...");
    }

    @Override
    public void fly() {
        System.out.println(this.name+" 正在用翅膀飞...");
    }

    @Override
    public void run() {
        System.out.println(this.name+" 正在用鸭腿跑...");
    }

    @Override
    public void swim() {
        System.out.println(this.name+" 正在用鸭腿游泳...");
    }
}