package operation;

import book.Book;
import book.BookList;

import java.util.Scanner;

/**
 * @Author: zzj
 * @Description:
 */
public class FindOpera implements IOOperation {
    public void work(BookList bookList) {
        System.out.println("查找图书！！！");
        System.out.println("请输入要查找的书名： ");
        Scanner scanner = new Scanner(System.in);
        String name = scanner.nextLine();

        int currentSize = bookList.getUsedSize();

        for(int i = 0; i <currentSize; i++) {
            Book book = bookList.getBook(i);
            if (book.getName().equals(name)) {
                System.out.println("这本书存在，信息如下：");
                System.out.println(book);
                return;
            }
        }
        System.out.println("没有找到您所需的书籍，书名为: " + name);
    }
}
