import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class PortScanner {
    private static final int TIMEOUT = 200;
    private static final int MIN_PORT = 1;
    private static final int MAX_PORT = 65535;

    public static void main(String[] args) {
        String host = "127.0.0.1"; // 目标主机IP地址
        int port = 80; // 要扫描的端口号

        try {
            Socket socket = new Socket();
            socket.connect(new InetSocketAddress(host, port), TIMEOUT);
            System.out.println("端口 " + port + " 被服务 " + getServiceName(port) + " 使用");
            socket.close();
        } catch (IOException e) {
            System.out.println("端口 " + port + " 未被服务使用");
        }
    }

    private static String getServiceName(int port) {
        // 根据端口号返回对应的服务名称，这里仅作示例，实际应用中需要查询数据库或其他方式获取服务名称
        switch (port) {
            case 80:
                return "HTTP";
            case 443:
                return "HTTPS";
            case 21:
                return "FTP";
            case 22:
                return "SSH";
            default:
                return "未知服务";
        }
    }

    public static List<Integer> scanPorts(String host, int minPort, int maxPort) {
        List<Integer> openPorts = new ArrayList<>();
        for (int port = minPort; port <= maxPort; port++) {
            try {
                Socket socket = new Socket();
                socket.connect(new InetSocketAddress(host, port), TIMEOUT);
                openPorts.add(port);
                System.out.println("端口 " + port + " 被服务 " + getServiceName(port) + " 使用");
                socket.close();
            } catch (IOException e) {
                System.out.println("端口 " + port + " 未被服务使用");
            }
        }
        return openPorts;
    }
}
