import java.util.Arrays;
import java.util.Random;

/**
 * @Author: zzj
 * @Description:
 */
public class Test {
    public static void inorderArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = i;
        }
    }
    public static void notInorderArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = array.length-i;
        }
    }
    public static void InorderArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = array.length-i;
        }
    }
    public static void initArray(int[] array) {
        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(array.length);
        }
    }
    public static void testInsertSort(int[] array) {
        int[] tmpArray = Arrays.copyOf(array,array.length);
        Long startTime = System.currentTimeMillis();
        Sort.insertSort(tmpArray);
        Long endTime = System.currentTimeMillis();
        System.out.println("插入排序："+(endTime-startTime));
    }
    public static void testShellSort(int[] array) {
        int[] tmpArray = Arrays.copyOf(array,array.length);
        Long startTime = System.currentTimeMillis();
        Sort.shellSort(tmpArray);
        Long endTime = System.currentTimeMillis();
        System.out.println("希尔排序："+(endTime-startTime));
    }
    public static void testSelectSort(int[] array) {
        int[] tmpArray = Arrays.copyOf(array,array.length);
        Long startTime = System.currentTimeMillis();
        Sort.selectSort(tmpArray);
        Long endTime = System.currentTimeMillis();
        System.out.println("选择排序："+(endTime-startTime));
    }
    public static void testHeapSort(int[] array) {
        int[] tmpArray = Arrays.copyOf(array,array.length);
        Long startTime = System.currentTimeMillis();
        Sort.heapSort(tmpArray);
        Long endTime = System.currentTimeMillis();
        System.out.println("堆排序："+(endTime-startTime));
    }
    public static void testQuickSort(int[] array) {
        int[] tmpArray = Arrays.copyOf(array,array.length);
        Long startTime = System.currentTimeMillis();
        Sort.quickSort(tmpArray);
        Long endTime = System.currentTimeMillis();
        System.out.println("快速排序："+(endTime-startTime));
    }
    //测试一些排序的方法
    public static void main2(String[] args) {
        int[] array = new int[10_0000];
//        inorderArray(array);
        notInorderArray(array);
//        initArray(array);
//        testInsertSort(array);
        testShellSort(array);
//        testSelectSort(array);
        testHeapSort(array);


    }
    public static void main(String[] args) {
        int[] array = {33,5,2,10,9,8,17};
        Sort.selectSort(array);
        System.out.println(Arrays.toString(array));
        Sort.heapSort(array);
//        Sort.mergeSort(array);
        System.out.println(Arrays.toString(array));
//        System.out.println("======");
//        Sort.mergeSortNor(array);
//        System.out.println(Arrays.toString(array));
//        System.out.println("========");
//        Sort.countSort(array);

    }
}
