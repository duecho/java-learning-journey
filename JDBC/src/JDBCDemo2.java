import com.mysql.cj.jdbc.MysqlDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * @Author: zzj
 * @Description:
 */

//修改操作
public class JDBCDemo2 {
    public static void main(String[] args) throws SQLException {
        //1.创建数据源
        DataSource dataSource = new MysqlDataSource();
        ((MysqlDataSource) dataSource).setUrl("jdbc:mysql://127.0.0.1:3306/java111?characterEncoding=UTF-8&&useSSL=false");
        ((MysqlDataSource) dataSource).setUser("root");
        ((MysqlDataSource) dataSource).setPassword("zzj20020515");

        //2.建立连接
        Connection connection = dataSource.getConnection();

        //3.构造sql
        String sql = "update test set name = '哈哈哈' where id = 1";
        PreparedStatement statement = connection.prepareStatement(sql);

        //4.执行sql
        int n = statement.executeUpdate();
        System.out.println("n = " + n);

        //5.释放资源
        statement.close();
        connection.close();
    }
}
