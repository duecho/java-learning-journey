package enumtest;

/**
 * @Author: zzj
 * @Description:
 */
public enum TestEnum {
    //枚举对象
    RED(1,"RED"),
    BLACK(2,"BLACK"),
    GREEN(3,"GREEN");


    public String color;
    public int ordinal;
    //默认是private方法
    TestEnum(int ordinal,String color) {
        this.color = color;
        this.ordinal = ordinal;
    }


    public static void main2(String[] args) {

        TestEnum[] testEnums = TestEnum.values();
        for (int i = 0; i < testEnums.length; i++) {
            System.out.println(testEnums[i]
                    +" " + testEnums[i].ordinal());
        }
        System.out.println("====");
        TestEnum testEnum = TestEnum.valueOf("GREEN");
        System.out.println(testEnum);
        System.out.println("====");
        System.out.println(RED.compareTo(GREEN));
    }

    public static void main1(String[] args) {
        TestEnum testEnum = TestEnum.RED;
       /* switch (testEnum) {
            case BLACK :
                System.out.println("BLACK");
                break;
            case RED:
                System.out.println("RED");
                break;
            case GREEN:
                System.out.println("GREEN");
                break;
            default:
        }*/
        switch (RED) {
            case RED -> System.out.println("RED");
            case BLACK -> System.out.println("BLACK");
            case GREEN -> System.out.println("GREEN");
        }
    }
}
