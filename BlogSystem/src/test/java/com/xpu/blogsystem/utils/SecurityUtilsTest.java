package com.xpu.blogsystem.utils;

/**
 * @Author: zzj
 * @Description:
 */
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.DigestUtils;

import java.io.UnsupportedEncodingException;
import java.util.UUID;
public class SecurityUtilsTest {
    public static void main(String[] args) {
        //md5加密
        System.out.println(DigestUtils.md5DigestAsHex("123456".getBytes()));
        //盐值
        System.out.println(UUID.randomUUID().toString().replace("-",""));
    }

    /**
     * 加密
     */
    @Test
    void encrypt() throws UnsupportedEncodingException {
        String password ="123456";
        String salt = UUID.randomUUID().toString().replace("-","");
        //md5(salt+password)
        String secretString = DigestUtils.md5DigestAsHex((salt+password).getBytes("UTF-8"));
        //数据库存储  盐值+密文
        String sqlPassword = salt+ secretString;
        System.out.println(sqlPassword);
    }

    @Test
    void verify() throws UnsupportedEncodingException {
        String inputPassword = "123456";
        String sqlPassword = "754f4f0e3c2544308eaadf0a1989b23f7a4035c35e08b3723b5dc7b578ddfed6";

        String salt = sqlPassword.substring(0,32);
        //md5(salt+password)
        String secretString = DigestUtils.md5DigestAsHex((salt+inputPassword).getBytes("UTF-8"));
        //数据库存储  盐值+密文
        boolean result = (salt+secretString).equals(sqlPassword);
        System.out.println(result);



    }
}
