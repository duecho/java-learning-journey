import com.mysql.cj.jdbc.MysqlDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @Author: zzj
 * @Description:
 */
public class JDBCDemo4 {
    public static void main(String[] args) throws SQLException {
        //1.创建数据源
        DataSource dataSource = new MysqlDataSource();
        ((MysqlDataSource) dataSource).setUrl("jdbc:mysql://127.0.0.1:3306/java111?characterEncoding=UTF-8&&useSSL=false");
        ((MysqlDataSource) dataSource).setUser("root");
        ((MysqlDataSource) dataSource).setPassword("zzj20020515");

        //2. 建立连接
        Connection connection = dataSource.getConnection();

        //3.构造sql
        String sql = "select * from test";
        PreparedStatement statement = connection.prepareStatement(sql);

        //4.执行sql
        ResultSet resultSet = statement.executeQuery();

        //5.遍历结果集合
        while (resultSet.next()) {
            int id = resultSet.getInt("id");
            String name = resultSet.getString("name");
            System.out.println("id =" + id +", name = " + name );
        }

        //6.释放资源
        statement.close();
        connection.close();
        resultSet.close();
    }
}
