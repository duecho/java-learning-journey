package com.xpu.springaoc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringAocApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringAocApplication.class, args);
    }

}
