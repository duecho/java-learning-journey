import java.util.Arrays;

/**
 * 类和对象
 */
//WashMachine 大驼峰形式
class  WashMachine {
    //成员变量，即属性字段，定义在方法外部，类的内部
    public String brand; // 品牌
    public String type; // 型号
    public double weight; // 重量
    public double length; // 长
    public double width; // 宽
    public double height; // 高
    public String color; // 颜色


    //成员方法
    public void washClothes(){ // 洗衣服
        System.out.println("洗衣功能");
    }
    public void dryClothes(){ // 脱水
        System.out.println("脱水功能");
    }
    public void setTime(){ // 定时
        System.out.println("定时功能");
    }
}
class Dog {
    public String name;
    public String color;
    public int age;
    // 狗的属性
    public void barks() {
        System.out.println(name + ": 旺旺旺~~~");
    }
    // 狗的行为
    public void wag() {
        System.out.println(name + ": 摇尾巴~~~");
    }
}
public class Test {
    public static void main(String[] args) {

    }
    public static void main5(String[] args) {
        Dog dog1 = new Dog();
        Dog dog2 = dog1;
        //引用只能指向对象，二者同时指向dog1所指向的对象
        // 一个引用不能同时指向多个对象，即dog1为引用，不能同时new多个。
    }
    public static void main4(String[] args) {
        Dog dog1 = new Dog();
        //实例化对象
        dog1.name = "大黄";
        dog1.age = 2;
        dog1.color = "黄色";
        System.out.println("年龄："+dog1.age);
        System.out.println("名称："+dog1.name);
        System.out.println("颜色："+dog1.color);
        dog1.barks();
        dog1.wag();


        Dog dog2 = new Dog();
        dog2.name = "多多";
        dog2.age = 3;
        dog2.color = "白色";
        System.out.println("年龄："+dog2.age);
        System.out.println("名称："+dog2.name);
        System.out.println("颜色："+dog2.color);
        dog2.barks();
        dog2.wag();

    }
    public static void main3(String[] args) {
        int[][] array3 = new int[2][];
        array3[0] = new int[3];
        array3[1] = new int[5];
        System.out.println(array3[1].length);//不规则数组的诞生

        /*System.out.println(array3[1]);
        System.out.println(array3[0]);
        System.out.println(array3[1][2]);*/
    }
    public static void main2(String[] args) {
        int[][] array = {{1,2,3,4},{1,2,3,0}};
        System.out.println(array.length);
        System.out.println(array[0].length);
        System.out.println(array[1].length);
        for (int[] tmpArray : array ) {
            for (int x : tmpArray) {
                System.out.print(x+" ");
            }
            System.out.println();
        }
        System.out.println();
        String ret = Arrays.deepToString(array);
        System.out.println(ret);
    }
    public static void main1(String[] args) {
        int[][] array = {{1,2,3,4},{1,2,3,0}};
        for(int i = 0; i < 2; i++) {
            for (int j = 0; j < 4; j++) {
                System.out.print(array[i][j]+" ");
            }
            System.out.println();
        }
        int[][] array2 = new int[][]{{1,2,3},{4,3,2}};
        int[][] array3 = new int[2][3];//默认为0
    }
}
