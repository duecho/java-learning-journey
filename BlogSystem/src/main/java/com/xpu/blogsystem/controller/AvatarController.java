package com.xpu.blogsystem.controller;

import com.xpu.blogsystem.mapper.UserInfoMapper;
import com.xpu.blogsystem.model.UserInfo;
import jakarta.servlet.http.HttpSession;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;

import java.util.Collections;
@Slf4j
@RequestMapping("/user")
@RestController
public class AvatarController {

    @Autowired
    private UserInfoMapper userInfoMapper;

    @PostMapping("/uploadAvatar")
    public ResponseEntity<?> uploadAvatar(@RequestParam("avatar") MultipartFile avatarFile, HttpSession session) {
        if (avatarFile.isEmpty()) {
            return ResponseEntity.badRequest().body("文件为空");
        }

        // 获取上传文件的原始名称
        String originalFilename = avatarFile.getOriginalFilename();

        // 生成新的文件名（可以加上时间戳或UUID，以避免重名）
        String newFileName = System.currentTimeMillis() + "_" + originalFilename;

        // 文件存储路径
        String uploadDir = "D:/cangku/java-learning-journey/BlogSystem/uploads";  // 确保路径存在

        File destFile = new File(uploadDir, newFileName);
        if (!destFile.getParentFile().exists()) {
            destFile.getParentFile().mkdirs();  // 如果目录不存在，创建目录
        }

        try {
            // 将文件保存到服务器
            avatarFile.transferTo(destFile);

            // 构建访问头像的URL（根据实际情况调整）
            String avatarUrl = "/uploads/" + newFileName;

            // 从 session 中获取当前用户信息
            UserInfo userInfo = (UserInfo) session.getAttribute("userInfo");
            if (userInfo != null) {
                // 更新用户信息中的头像URL
                userInfo.setAvatarUrl(avatarUrl);
                userInfoMapper.updateUserInfo(userInfo);  // 更新数据库中的用户信息

                // 返回头像URL
                return ResponseEntity.ok().body(Collections.singletonMap("avatarUrl", avatarUrl));
            } else {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("用户未登录");
            }

        } catch (Exception e) {
            log.error("上传头像发生异常, e: ", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("上传失败：" + e.getMessage());
        }

    }
}
