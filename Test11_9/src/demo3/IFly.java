package demo3;

/**
 * @projectName: Test11_9
 * @package: demo3
 * @className: IFly
 * @author: Tom of loveing coding
 * @description: TODO
 * @date: 2023/11/9 23:03
 * @version: 1.0
 */
public interface IFly {
    void fly();
}