package demo2;

/**
 * @projectName: Test11_9
 * @package: demo2
 * @className: Computer
 * @author: Tom of loveing coding
 * @description: TODO
 * @date: 2023/11/9 22:55
 * @version: 1.0
 */
public class Computer {
    public void powerOn() {
        System.out.println("打开电脑！");
    }
    public void powerOff() {
        System.out.println("关闭电脑！");
    }

    public void useDevice(IUSB iusb) {
        iusb.openDevice();

        if(iusb instanceof Mouse) {
            Mouse mouse = (Mouse)iusb;
            mouse.click();
        }else if(iusb instanceof KeyBoard) {
            KeyBoard keyBoard = (KeyBoard)iusb;
            keyBoard.inPut();
        }
        iusb.closeDevice();
    }
}