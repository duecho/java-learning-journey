package operation;

import book.Book;
import book.BookList;

import java.util.Scanner;

/**
 * @Author: zzj
 * @Description:
 */
public class DelOperation implements IOOperation {
    public void work(BookList bookList) {
        System.out.println("删除图书！！！");
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入您要删除图书的书名：");

        int currentSize = bookList.getUsedSize();
        int index = -1;
        String name = scanner.nextLine();
        int i = 0;
        for(; i < currentSize; i++) {
            Book tmp = bookList.getBook(i);
            if (tmp.getName().equals(name)) {
                index = i;
                break;
            }
        }
        if(i >= currentSize) {
            System.out.println("没有您要删除的图书！！！");
            return;
        }
        for (int j  = index; j < currentSize-1; j++) {
            Book book = bookList.getBook(j+1);
            bookList.setBook(book,j);
        }
        bookList.setBook(null,currentSize-1);
        bookList.setUsedSize(currentSize-1);//资源回收
        System.out.println("删除成功！！！");
    }
}
