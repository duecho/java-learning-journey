/**
 * @Author: zzj
 * @Description:
 */
public class Test {
    public static void main(String[] args) {
        BinarySearchTree bTree = new BinarySearchTree();
        bTree.insert(10);
        bTree.insert(5);
        bTree.insert(19);
        bTree.remove(5);
        System.out.println(bTree.search(10));
    }
}
