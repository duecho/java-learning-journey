/**
 * @Author: zzj
 * @Description:
 */
public class AffineCipher {
    //仿射密码： 仿射密码是多项式代换密码的另一种特例，其加密方程为：
    //f(l)=(kl+b)mod q
        public static int affineCipherEncrypt(int l, int k, int b, int q) {
            return (k * l + b) % q;
        }

        public static int affineCipherDecrypt(int c, int k, int b, int q) {
            int kInv = 1;
            while ((k * kInv) % q != 1) {
                kInv++;
            }
            return (kInv * (c - b + q)) % q;
        }

        public static void main(String[] args) {
            int l = 6;
            int k = 3;
            int b = 5;
            int q = 26;

            int encrypted = affineCipherEncrypt(l, k, b, q);
            System.out.println("加密数据为: " + encrypted);

            int decrypted = affineCipherDecrypt(encrypted, k, b, q);
            System.out.println("解密数据为: " + decrypted);
        }
}
