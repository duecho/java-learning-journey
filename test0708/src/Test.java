import java.util.*;

/**
 * @Author: zzj
 * @Description:0708第一次笔试题
 */

public class Test {
    /**
     * 点击消除
     * @param args
     */
    public static void main3(String[] args) {
        Stack<Character> stack = new Stack<>();
        Scanner sc = new Scanner(System.in);
        String str = sc.nextLine();
        for (int i = 0; i < str.length(); i++) {
            if (stack.empty()) {
                stack.push(str.charAt(i));
            } else if (str.charAt(i) == stack.peek()) {
                stack.pop();
            }else {
                stack.push(str.charAt(i));
            }
        }
        if (stack.empty()) {
            System.out.println(0);
        }
        String s = "";
        while (!stack.empty()) {
            s = s + stack.pop();
        }
        for (int i = s.length()-1; i >= 0 ; i--) {
            System.out.print(s.charAt(i));
        }
    }

    /**
     *
     * @param nums1
     * @param nums2
     * @return
     * 查找公共元素
     */
    public ArrayList<Integer> intersection(ArrayList<Integer> nums1, ArrayList<Integer> nums2) {
        // write code here
        HashSet<Integer> set1 = new HashSet<>(nums1);
        HashSet<Integer> set2 = new HashSet<>(nums2);
        ArrayList<Integer> result = new ArrayList<>();
        for (Integer num : set1) {
            if (set2.contains(num)) {
                result.add(num);
            }
        }
        return result;
    }
    //错误解法
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int start = scanner.nextInt();
        int end  = scanner.nextInt();
        Set<Integer> set = new HashSet<>();
        for (int i = start; i < end; i++) {
            if (i == 2 || String.valueOf(i).contains("2")) {
                set.add(i);
            }
        }
        System.out.println(set.size());
    }

    // 注意类名必须为 Main, 不要有任何 package xxx 信息
        public static void main1(String[] args) {
            Scanner scanner = new Scanner(System.in);
            int start = scanner.nextInt();
            int end = scanner.nextInt();
            int count  = 0;
            for(int i = start; i <= end; i++) {
                count += countNum(i);
            }
            System.out.println(count);

        }
        public static int countNum(int num) {
            int count = 0;
            while (num > 0) {
                if (num % 10 == 2) {
                    count++;
                }
                num /= 10;
            }

            return count;
        }
}

