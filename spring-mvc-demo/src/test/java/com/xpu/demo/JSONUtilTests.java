package com.xpu.demo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.xpu.demo.model.UserInfo;

/**
 * @Author: zzj
 * @Description:
 */
public class JSONUtilTests {
    private static ObjectMapper objectMapper = new ObjectMapper();

//    public static void main1(String[] args) throws JsonProcessingException {
////        JSON转对象
//        UserInfo userInfo = new UserInfo();
//        userInfo.setUserName("zzj");
//        userInfo.setAge(18);
//        userInfo.setPassword("123");
//        String userJson = objectMapper.writeValueAsString(userInfo);
//        System.out.println(userJson);
//        //对象转JSON
//        String jsonStr = "{\"username\":\"zhangsan\",\"age\":18,\"password\":\"123456\"}";
//        UserInfo userInfo1 = objectMapper.readValue(jsonStr,UserInfo.class);
//        System.out.println(userInfo1);
//    }

    public static void main(String[] args) throws JsonProcessingException {
        UserInfo userInfo = new UserInfo("zzj","123",18);
        String userJson = objectMapper.writeValueAsString(userInfo);
        System.out.println(userJson);
        //对象转JSON
        String jsonStr = "{\"username\":\"zhangsan\",\"age\":18,\"password\":\"123456\"}";
        UserInfo userInfo1 = objectMapper.readValue(jsonStr,UserInfo.class);
        System.out.println(userInfo1);
    }
}
