package com.xpu.blogsystem.config;

import com.xpu.blogsystem.utils.JwtUtils;
import io.jsonwebtoken.Claims;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

/**
 * @Author: zzj
 * @Description:
 */
@Slf4j
@Component
public class LoginInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //验证token是否合法
        String userToken = request.getHeader("user_token_header");
        log.info("从header中获取信息, token:"+ userToken);
        Claims claims = JwtUtils.verify(userToken);
        if (claims==null){
            //token校验失败
            response.setStatus(401);
//            response.getOutputStream().write();
//            response.setContentType();
            return false;
        }
        return true;
    }
}
