package com.xpu.blogsystem.model;

import lombok.Data;

import java.util.Date;

/**
 * @Author: zzj
 * @Description:
 */
@Data
public class UserInfo {
    private Integer id;
    private String userName;
    private String password;
    private String githubUrl;
    private Integer deleteFlag;
    private Date createTime;
    private Date updateTime;
    private String avatarUrl; // 头像URL
}

