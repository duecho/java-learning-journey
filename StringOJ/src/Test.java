import java.sql.SQLOutput;
import java.util.Arrays;
import java.util.Scanner;

/**
 * @Author: zzj
 * @Description:
 */


import java.util.Scanner;
class Login {
    private String useName = "admin";
    private String passWord = "1234566789";
    public   void logIn(String useName,String passWord) {
        if(!this.useName.equals(useName)) {
            //System.out.println("用户名错误");
            throw new userNameException("用户名错误");
        }
        if(!this.passWord.equals(passWord)) {
            throw new passWordException("密码错误！");
        }
    }
}
public class Test {


    public static void main(String[] args) {
        try {
            Login login = new Login();
            login.logIn("admin", "12345");
        }catch (userNameException e) {
            e.printStackTrace();
            System.out.println("捕捉到了用户名错误异常、、、、");
        }catch (passWordException e) {
            e.printStackTrace();
            System.out.println("捕捉到了密码错误异常、、、、");
        }
        System.out.println("程序继续执行 ");
    }
    public static void func() throws NullPointerException{
//        if(array == null) {
//            throw new NullPointerException("woqilianghenxiaoxiaodaozhinengyouyigeren,woqilianghendadadaokeyirongdexiasuoyouren!");
//
//        }
        int[] array = null;
        System.out.println(array.length);
    }
    public static void main5(String[] args) {
        //func(null);1.异常处理
        try {
            func();
        }catch (NullPointerException e) {
            e.printStackTrace();
            System.out.println("异常处理！！！");
        }
        System.out.println("没有然后！！！");
    }
    public static boolean isPalindrome(String s) {
        s = s.toLowerCase();
        int left = 0;
        int right = s.length()-1;//范围为0~length-1
        while (left < right) {
            while (left < right && !isCharacterNum(s.charAt(left))) {
                left++;
            }
            while (left < right && !isCharacterNum( s.charAt(right)) ) {
                right--;
            }
            if(s.charAt(left) == s.charAt(right)) {
                left++;
                right--;
            }else {
                return false;
            }
        }
        return true;
    }
    private static boolean isCharacterNum(char ch) {
        if (Character.isDigit(ch) || Character.isLetter(ch)) { //isDigit:字母判断   isLetter:数字判断
            return true;
        }
        return  false;
    }
    //回文串的判断
    public static void main4(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        boolean n = isPalindrome(s);
        System.out.println(n);

    }




    //求字符串最后一个单词的长度  2
    public static void main3(String[] args) {
        Scanner sc= new Scanner(System.in);
        while(sc.hasNextLine()) {
            String s = sc.nextLine();
            int dex = s.lastIndexOf(" ");
            String str = s.substring(dex+1);
            System.out.println(str.length());

        }

    }

    //求字符串最后一个单词的长度  1
    public static void main2(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        String[] str =  s.split(" ");
        System.out.print(str[str.length-1].length());
    }



    //找出一段数据中重复输出的第一个字段的位置
    public static void main1(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input = "13422";
        int result = solution(input);
        if(result != -1) {
            System.out.println("The position of the first non-repeating character in the input string is: " + result);
        } else {
            System.out.println("No non-repeating character found in the input string.");
        }
    }


    public static int solution(String s) {
        int[] arr = new int[26];
        for(int i = 0; i<s.length(); i++) {
            char ch = s.charAt(i);
            arr[ch-'a']++;
        }
        for(int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);
            if(arr[ch-'a'] == 1) {
                return i; // Return the position of the character in the string
            }
        }
        return -1;
    }
}

