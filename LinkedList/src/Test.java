import java.util.ArrayList;
import java.util.LinkedList;
import java.util.ListIterator;

/**
 * @Author: zzj
 * @Description:
 */
public class Test {
    public static void main88(String[] args) {
        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.add(1);
        arrayList.add(2);
        LinkedList<Integer> list = new LinkedList<>(arrayList);
        System.out.println(list);  //第一种打印
        System.out.println("===");
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i)+" ");
        }             //第二种打印
        System.out.println();
        System.out.println("====");
        for (int x : list) {
            System.out.println(x + " ");
        }                  //第三种打印
        System.out.println();
        System.out.println("===");
        ListIterator<Integer> it = list.listIterator();
        while (it.hasNext()) {
            System.out.println(it.next()+" ");
        }                  //第四种打印----正向迭代器
        System.out.println();
        System.out.println("====");
        ListIterator<Integer> it2 = list.listIterator();
        while (it2.hasPrevious()) {
            System.out.println(it.previous()+" ");
        }                   //第五种打印----逆向迭代器打印
        System.out.println();
    }


    public static void main8(String[] args) {
        LinkedList<Integer> list = new LinkedList<>();


        MyLinkedList mylinkedList = new MyLinkedList();
        mylinkedList.addFirst(1);
        mylinkedList.addFirst(2);
        mylinkedList.addFirst(3);
        mylinkedList.addFirst(4);
        mylinkedList.addFirst(5);
        mylinkedList.display();
    }

    private  void createCut(MySingleList.ListNode headA,
                            MySingleList.ListNode headB) {
        headB.next = headA.next.next;
    }
    public static void main7(String[] args) {
        MySingleList mySingleList = new MySingleList();
        mySingleList.addLast(1);
        mySingleList.addLast(25);
        mySingleList.addLast(35);
        mySingleList.addLast(45);
        mySingleList.addLast(55);
        mySingleList.display();

        MySingleList mySingleList1 = new MySingleList();
        mySingleList1.addLast(11);
        mySingleList1.addLast(22);
        mySingleList1.addLast(35);
        mySingleList1.addLast(45);
        mySingleList1.addLast(55);
        mySingleList1.display();

        MySingleList.ListNode ret = getIntersectionNode(mySingleList1.head, mySingleList.head);
        mySingleList1.display(ret);
    }
    public static MySingleList.ListNode getIntersectionNode(MySingleList.ListNode headA, MySingleList.ListNode headB) {
        int lenA = 0;
        int lenB = 0;

        MySingleList.ListNode pl = headA;
        MySingleList.ListNode ps = headB;
        while(pl != null) {
            lenA++;
            pl = pl.next;
        }
        while(ps != null) {
            lenB++;
            ps = ps.next;
        }

        pl = headA;
        ps = headB;
        int len = lenA - lenB;
        if(len < 0) {
            pl = headB;
            ps = headA;
            len = lenB- lenA;
        } //保证指向正确，len为正数
        while(len != 0) {
            pl = pl.next;
            len--;
        }
        //保证pl ps 将来相遇 的路程一样
        while(pl != null && ps != null && pl != ps) {
            pl = pl.next;
            ps = ps.next;
        }
        if(pl == ps && pl == null) {
            return null;
        }
        return pl;
    }

    public static void main6(String[] args) {
        MySingleList mySingleList = new MySingleList();
        mySingleList.addLast(1);
        mySingleList.addLast(46);
        mySingleList.addLast(35);
        mySingleList.addLast(45);
        mySingleList.addLast(6);
//        myLinkedList.display();
        MySingleList.ListNode ret = mySingleList.partition(30);
        mySingleList.display();
        System.out.println(ret.val);
    }
        public static MySingleList.ListNode  mergeTwoLists(MySingleList.ListNode headA,
                                                           MySingleList.ListNode headB) {  // 合并两个链表
            MySingleList.ListNode newHead = new MySingleList.ListNode(-1);
            MySingleList.ListNode tmp = newHead;
            while (headA != null && headB != null) {
                if(headA.val < headB.val){
                    tmp.next = headA;
                    headA = headA.next;
                    tmp = tmp.next;
                }else {
                    tmp.next = headB;
                    headB = headB.next;
                    tmp = tmp.next;
                }
            }
            if (headA != null) {
                tmp.next = headA;
            }
            if (headB != null) {
                tmp.next = headB;
            }
            return newHead.next;
        }

    public static void main5(String[] args) {
        MySingleList mySingleList = new MySingleList();
        mySingleList.addLast(1);
        mySingleList.addLast(25);
        mySingleList.addLast(35);
        mySingleList.addLast(45);
        mySingleList.addLast(55);
        mySingleList.display();

        MySingleList mySingleList1 = new MySingleList();
        mySingleList1.addLast(11);
        mySingleList1.addLast(22);
        mySingleList1.addLast(33);
        mySingleList1.addLast(44);
        mySingleList1.addLast(55);
        mySingleList1.display();

        MySingleList.ListNode ret = mergeTwoLists(mySingleList1.head, mySingleList.head);
        mySingleList1.display(ret);
    }
    public static void main(String[] args) {
        MySingleList mySingleList = new MySingleList();
        mySingleList.addLast(1);
        mySingleList.addLast(2);
        mySingleList.addLast(3);
        mySingleList.addLast(4);
        mySingleList.addLast(5);
        mySingleList.display();
        MySingleList.ListNode ret = mySingleList.FindKthToTail(3);
        System.out.println(ret.val);


    }
    public static void main3(String[] args) {
        MySingleList mySingleList = new MySingleList();
        mySingleList.addLast(1);
        mySingleList.addLast(2);
        mySingleList.addLast(3);
        mySingleList.addLast(4);
        mySingleList.addLast(5);
        mySingleList.display();
        MySingleList.ListNode ret = mySingleList.middleNode();
        mySingleList.display(ret);
        System.out.println(ret.val);
    }

    public static void main2(String[] args) {
        MySingleList mySingleList = new MySingleList();
        mySingleList.addLast(1);
        mySingleList.addLast(2);
        mySingleList.addLast(3);
        mySingleList.addLast(4);
        mySingleList.addLast(5);
        mySingleList.display();
        System.out.println("反转！！！");
        MySingleList.ListNode ret = mySingleList.reverseList();
        mySingleList.display();
    }
    public static void main1(String[] args) {
        MySingleList mySingleList = new MySingleList();
//        myLinkedList.createList();
        mySingleList.addIndex(3,10000);
        mySingleList.addFirst(12);
        mySingleList.addFirst(24);
        mySingleList.addFirst(22);
        mySingleList.addFirst(44);
        System.out.println("====");

        mySingleList.display();
        System.out.println(mySingleList.contains(12));
        System.out.println(mySingleList.size());

    }


}
