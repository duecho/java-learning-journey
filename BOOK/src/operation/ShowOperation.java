package operation;

import book.Book;
import book.BookList;

/**
 * @Author: zzj
 * @Description:
 */
public class ShowOperation implements IOOperation {
    public void work(BookList bookList) {
        System.out.println("显示图书！！！");

        int currentSize = bookList.getUsedSize();

        for(int i = 0; i <currentSize; i++) {
            Book book = bookList.getBook(i);
                System.out.println(book);
            }
        }
}
