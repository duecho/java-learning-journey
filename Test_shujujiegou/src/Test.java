import java.util.Arrays;

/**
 * @Author: zzj
 * @Description:
 */
class Solutions {
    public int[] votate(int[] nums, int k) {
        int n = nums.length;
        k = k % n;
        int count = 0;
        for (int start = 0;count < n;start ++) {
            int current = start;
            int prev = nums[start];
            do {
                int next = (current + k) % n;
                int temp = nums[next];
                nums[next] = prev;
                prev= temp;
                current = next;
                count++;
            }while (start != current);
        }
        return nums;
    }
}



public class Test {
    public static void main(String[] args) {
            int[] nums = {1,2,3,4,5,5,4,3,2,1};
            Solutions solutions = new Solutions();
            int[] ret = solutions.votate(nums,4);
            System.out.println(Arrays.toString(ret));
    }


    public static void main2(String[] args) {
            int[] nums = {0,1,2,3,4,6};
            Solution solution = new Solution();
            int ret = solution.missingNumber(nums);
            System.out.println(ret);
    }
    public static void main1(String[] args) {
        MyArray <String> myArray = new MyArray<>();
        myArray.setArray(2,"hahaha");
        String ret = myArray.getArray(2);
        System.out.println(ret);
    }


}
class Solution {
    //消失的数字题解
    public int missingNumber(int[] nums) {
        int n = nums.length;
        int sum = n * (n + 1) / 2; //利用数学公式前n项和
        int sums = 0;
        for (int num : nums) {
            sums += num;
        }
        return sum - sums;
    }
}
class MyArray<T> {
    public  Object[] array =  new Object[10];

    public T getArray(int pos) {
        return (T) array[pos];
    }

    public void setArray(int pos, T val) {
        array[pos] = val;
    }
}
