package demo;

import org.w3c.dom.ls.LSOutput;

import java.util.List;

/**
 * @Author: zzj
 * @Description:
 */
public class Main {
    public static void main(String[] args) {
        CardGame cardGame = new CardGame();
        List<Card> ret = cardGame.buyCard();
        System.out.println("买牌！");
        System.out.println(ret);

        System.out.println("洗牌！");
        cardGame.shuffle(ret);
        System.out.println(ret);

        System.out.println("揭牌！");
        List<List<Card>>  hand = cardGame.getCard(ret);
        for (int i = 0; i < hand.size(); i++) {
            System.out.println("第 "+(i+1)+"个人的手牌："+hand.get(i));
        }
        System.out.println("剩下的牌：");
        System.out.println(ret);
    }


}
