/**
 * @Author: zzj
 * @Description:
 */
public class Test {


    //点击消除算法

    /*
    import java.util.*;

// 注意类名必须为 Main, 不要有任何 package xxx 信息
public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        while (in.hasNextLine()) { // 注意 while 处理多个 case
            String str = in.nextLine();
            Stack<Character>  stack1 = new Stack<>();

            for(int i = 0; i < str.length();i++) {
                char ch = str.charAt(i);
                if(stack1.empty() || ch != stack1.peek()) {
                    stack1.push(ch);
                }else {
                    stack1.pop();
            }


        }
        if(stack1.empty()) {
            System.out.println("0");
        }else {
            Stack<Character> stack2 = new Stack();
            while(!stack1.empty()) {
                stack2.push(stack1.pop());
            }
            while(! stack2.empty()) {
                System.out.print(stack2.pop());

            }
        }

        }
    }
}
     */

    class Solution {
        public boolean isSubsequence(String s, String t) {
            int lens = s.length();
            int lent = t.length();
            int i = 0;
            int j = 0;
            while(i < lens && j < lent) {
                if(s.charAt(i) == t.charAt(j)) {
                    i++;
                    j++;
                }else {
                    j++;
                }
            }


            if(i < lens) {
                return false;
            }else {
                return true;
            }
        }
    }
}
