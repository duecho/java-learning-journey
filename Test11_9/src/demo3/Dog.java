package demo3;

/**
 * @projectName: Test11_9
 * @package: demo3
 * @className: Dog
 * @author: Tom of loveing coding
 * @description: TODO
 * @date: 2023/11/9 23:00
 * @version: 1.0
 */
public class Dog extends Animal implements ISwim,IRun{

    public Dog(String name) {
        super(name);
    }

    @Override
    public void swim() {
        System.out.println(this.name+" 正在用4条腿游泳！");
    }

    @Override
    public void run() {
        System.out.println(this.name+" 正在用4条腿跑！");
    }

    @Override
    public void eat() {
        System.out.println(this.name+" 正在吃狗粮...");
    }
}