package com.xpu.blogsystem.mapper;

import com.xpu.blogsystem.model.BlogInfo;
import org.apache.ibatis.annotations.*;

import java.util.List;


@Mapper
public interface BlogInfoMapper {
    /**
     * 查询所有博客
     */
    @Select("select id, title, content, user_id, update_time from `blog` where delete_flag=0 order by id desc")
    List<BlogInfo> selectAll();

    /**
     * 根据博客ID, 查询博客信息
     */
    @Select("select id, title, content, user_id, update_time from `blog` where id = #{id} and delete_flag=0")
    BlogInfo selectById(Integer id);

    /**
     * 插入博客
     */
    @Insert("insert into `blog` (title, content, user_id) values (#{title}, #{content}, #{userId})")
    Integer insertBlog(BlogInfo blogInfo);

    /**
     * 更新博客
     */
    Integer updateBlog(BlogInfo blogInfo);

//    // 增加点赞数量
//    @Update("UPDATE `blog` SET likes = likes + 1 WHERE id = #{blogId}")
//    int incrementLikes(@Param("blogId") Integer blogId);
}
