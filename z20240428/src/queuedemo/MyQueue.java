package queuedemo;

/**
 * @Author: zzj
 * @Description:
 */
public class MyQueue {
    static class ListNode {
        public int val;
        public ListNode prev;
        public ListNode next;

        public ListNode(int val) {
            this.val = val;
        }
    }

    public ListNode head;
    public ListNode last;

    //入队
    public void offer(int val) {
        ListNode node = new ListNode(val);
        if (head == null) {
            head = last = node;
        }else {
            last.next = node;
            node.prev = last;
            last = node;
        }
    }
    //出队
    public int poll() {
        if(head == null) {
            return -1;
        }
        int val = -1;
        if (head.next == null) {
            val = head.val;
            head = null;
            last = null;
            return val;
        }
        val = head.val;
        head = head.next;
        head.prev = null;
        return val;
    }

    public boolean empty() {
        return head == null;
    }

    public int peek() {
        if (head == null) {
            return -1;
        }
        return head.val;
    }
}
