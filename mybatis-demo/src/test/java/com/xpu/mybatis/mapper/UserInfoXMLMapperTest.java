package com.xpu.mybatis.mapper;

import com.xpu.mybatis.model.UserInfo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @Author: zzj
 * @Description:
 */
@SpringBootTest
class UserInfoXMLMapperTest {

    @Autowired
    private UserInfoXMLMapper userInfoXMLMapper;

//    @Test
//    void queryUserInfos() {
//        userInfoXMLMapper.queryUserInfos().forEach(System.out::println);
//    }
//
//    @Test
//    void queryUserInfos2() {
//        userInfoXMLMapper.queryUserInfos2().forEach(System.out::println);
//    }

    @Test
    void insertUserInfo() {
        UserInfo userInfo = new UserInfo();
        userInfo.setUsername("zhangwu");
        userInfo.setPassword("zhangwu");
        userInfo.setGender(0);
        userInfo.setAge(16);
        userInfoXMLMapper.insertUserInfo(userInfo);
    }

    @Test
    void updateUserInfo() {
        UserInfo userInfo = new UserInfo();
        userInfo.setPhone("1234567899");
        userInfo.setId(11);
        userInfoXMLMapper.updateUserInfo(userInfo);
    }

    @Test
    void deleteUser() {
        userInfoXMLMapper.deleteUser(11);
    }

}