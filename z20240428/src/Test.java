import queuedemo.MyQueue;
import stackdemo.MyStack;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Queue;

/**
 * @Author: zzj
 * @Description:
 */
public class Test {
    public static void main(String[] args) {
        MyQueue myQueue = new MyQueue();
        myQueue.offer(1);
        myQueue.offer(2);
        myQueue.offer(3);
        myQueue.offer(4);
        myQueue.offer(5);
        System.out.println(myQueue.poll());
        System.out.println(myQueue.poll());
        Queue<Integer> queue = new LinkedList<>();

        //双端队列
        Deque<Integer> deque = new LinkedList<>();

        Deque<Integer> deque2 = new ArrayDeque<>();

    }


    public static void main1(String[] args) {
        MyStack stack = new MyStack();
        stack.push(2);
        stack.push(3);
        stack.push(4);
        stack.push(5);
        stack.push(6);


        //pop :删除元素
        int ret = stack.pop();
        System.out.println(ret);

        //peek :获取栈顶元素
        int ret2 = stack.peek();
        System.out.println(ret2);
        System.out.println(stack.isEmpty());
    }
}
