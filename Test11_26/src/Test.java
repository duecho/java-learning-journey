import java.util.Scanner;

/**
 * @projectName: Test11_26
 * @package: PACKAGE_NAME
 * @className: Test
 * @author: Tom of loveing coding
 * @description: TODO
 * @date: 2023/11/26 13:36
 * @version: 1.0
 */
import java.util.Scanner;

public class Test {

        public static void main(String[] args) {
            try (Scanner scanner = new Scanner(System.in)) {
                String a=scanner.nextLine();
                char [] str=a.toCharArray();
                char[] s=new char[100000];
                int length=a.length();
                int j=0;
                for(int i=0;i<length;i++){
                    if(str[i]=='a'||str[i]=='e'||str[i]=='i'||str[i]=='o'||str[i]=='u')
                    {
                        s[j++]=str[i];
                    }
                }
                System.out.println(s[j-1]);
            }
        }


        public static void main666(String[] args) {
            Scanner sc = new Scanner(System.in);
            int n = sc.nextInt();
            int k = sc.nextInt();
            int[] a = new int[n];
            for (int i = 0; i < n; i++) {
                a[i] = sc.nextInt();
            }
            System.out.println(maxSumOfK(a, k));
        }

        public static int maxSumOfK(int[] a, int k) {
            if (a == null || a.length < k) {
                return -1;
            }

            int n = a.length;
            int sum = 0;
            for (int i = 0; i < k; i++) {
                sum += a[i];
            }

            int maxSum = sum;
            for (int i = k; i < n; i++) {
                sum = sum - a[i - k] + a[i];
                maxSum = Math.max(maxSum, sum);
            }

            return maxSum;
        }




    public static void main99(String[] args) {
            Scanner sc = new Scanner(System.in);
            int n = sc.nextInt();
            int m = sc.nextInt();
            int[][] matrix = new int[n][m];
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < m; j++) {
                    matrix[i][j] = sc.nextInt();
                }
            }
            int r = sc.nextInt();
            int c = sc.nextInt();
            sc.close();

            int count = 0;
            for (int i = -1; i <= 1; i++) {
                for (int j = -1; j <= 1; j++) {
                    if (i == 0 && j == 0) {
                        continue;
                    }
                    int newRow = r + i;
                    int newCol = c + j;
                    if (newRow >= 1 && newRow <= n && newCol >= 1 && newCol <= m) {
                        count++;
                    }
                }
            }
            System.out.println(count);
        }




    public static void main555(String[] args) {
            Scanner scanner = new Scanner(System.in);
            System.out.print("请输入一个整数： ");
            int num = scanner.nextInt();
            int result = 1;
            while (num >= 10) {
                for (char c : String.valueOf(num).toCharArray()) {
                    if (c != '0') {
                        result *= Character.getNumericValue(c);
                    }
                }
                System.out.println(result);
                num = result;
                result = 1;
            }
        }



    public static void main55(String[] args) {
            Scanner scanner = new Scanner(System.in);
            while (scanner.hasNext()) {
                int num = scanner.nextInt();
                System.out.println(transform(num));
            }
        }

        public static int transform(int num) {
            int result = 1;
            while (num > 0) {
                int digit = num % 10;
                if (digit != 0) {
                    result *= digit;
                }
                num /= 10;
            }
            return result;
        }


    public static void main6(String[] args) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("请输入一个字符串：");
            String input = scanner.nextLine();
            char lastVowel = findLastVowel(input);
            if (lastVowel == '\0') {
                System.out.println("没有找到元音字母。");
            } else {
                System.out.println("最后一个元音字母是：" + lastVowel);
            }
        }

        public static char findLastVowel(String input) {
            String vowels = "aeiou";
            for (int i = input.length() - 1; i >= 0; i--) {
                if (vowels.indexOf(input.charAt(i)) != -1) {
                    return input.charAt(i);
                }
            }
            return '\0';
        }



       /* public static void main(String[] args) {
            Scanner scanner = new Scanner(System.in);
            System.out.print("请输入一个整数： ");
            int x = scanner.nextInt();
            String xStr = Integer.toString(x);
            String shiftedStr = shiftLeft(xStr);
            int shiftedInt = Integer.parseInt(shiftedStr);
            System.out.println("左移后的整数为： " + shiftedInt);
        }

        public static String shiftLeft(String str) {
            char firstChar = str.charAt(0);
            String shiftedStr = str.substring(1) + firstChar;
            return shiftedStr;
        }*/


    /*public static void main7(String[] args) {
            int x = 194910;
            String xStr = Integer.toString(x);
            String shiftedStr = shiftLeft(xStr);
            int shiftedInt = Integer.parseInt(shiftedStr);
            System.out.println(shiftedInt);
        }

        public static String shiftLeft(String str) {
            char firstChar = str.charAt(0);
            String shiftedStr = str.substring(1) + firstChar;
            return shiftedStr;
        }*/


    /* public static void main22(String[] args) {
        int[][] numbers = {
                {0,0,0,0,1,0,0,0,1,0,0,0,0,0,0,1,1,0,1,0,1,0,1,0,1,0,0,1,0,0,1,1,0,0,0,0,0,0,1,1},
                {0,1,0,1,1,1,1,0,0,1,1,1,1,1,0,1,1,1,0,1,1,1,1,0,0,0,0,0,1,0,1,0,1,0,0,1,1,1,1,1},
                {1,0,0,0,0,1,0,0,0,0,0,1,1,1,0,1,0,1,0,1,1,0,0,0,0,0,0,0,0,0,1,0,1,1,0,1,0,1,0,0}
　　             {0,1,1,0,1,0,1,0,1,0,1,1,0,0,0,0,0,0,0,1,0,1,1,0,0,1,0,0,0,0,0,1,0,1,0,0,1,0,0,1},
　　             {0,0,0,0,0,1,1,0,1,0,1,0,0,0,0,0,1,1,1,1,1,1,0,0,1,1,0,1,1,0,0,0,1,0,1,0,1,0,0,1},
　　             {0,1,1,0,0,0,0,1,1,0,0,0,0,0,0,0,1,1,0,1,0,0,0,0,0,0,0,0,0,1,0,0,1,0,1,0,0,0,1,1},
　　             {0,1,0,0,1,1,0,0,1,0,0,0,0,1,1,0,0,0,0,0,0,0,1,0,0,0,1,0,0,0,0,1,0,1,1,1,0,0,0,0},
　　             {0,0,1,0,0,1,1,0,1,0,1,0,0,1,1,0,0,0,1,1,1,1,0,0,1,1,0,1,1,0,0,1,1,0,1,0,0,0,1,0}
　　             {1,1,1,1,0,0,0,1,1,1,1,0,1,0,0,0,0,0,1,1,1,0,0,1,0,0,0,1,0,0,1,0,1,1,1,0,1,1,0,1}
　　             {0,0,1,1,1,1,0,1,0,0,0,1,1,0,0,0,0,0,0,0,0,1,1,0,1,0,0,1,1,0,1,1,1,0,1,0,0,0,0,1}
　　             {0,0,0,0,0,0,0,1,0,1,0,1,1,0,0,0,0,1,0,0,1,1,1,1,1,0,0,1,0,1,0,0,1,1,0,1,1,1,0,0}
　　             {0,0,0,0,1,0,0,0,0,0,0,1,1,0,0,1,0,0,0,1,0,0,1,0,1,0,0,0,1,1,1,0,1,1,1,0,1,1,0,0}
　　             {0,0,1,0,1,1,0,0,0,0,0,0,1,0,0,0,0,0,1,0,1,0,1,0,0,0,1,1,0,0,0,0,1,0,1,0,0,0,1,1}
　　             {0,1,1,0,1,1,0,0,0,0,1,0,0,0,1,1,0,1,1,0,1,0,0,1,1,0,1,0,0,0,1,1,0,1,0,1,1,0,1,1}
　             　{0,0,0,0,1,0,0,1,0,0,0,0,0,0,0,1,0,1,0,0,0,0,1,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,1,0}
　　             {0,0,1,1,0,0,1,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0,0,1,1,0,0,1,1,0,0,1,0,1,0,0,0,1,1,0}
　　             {1,1,1,0,1,0,1,0,0,0,0,1,1,0,0,0,0,0,0,1,0,0,0,1,1,0,0,1,0,0,1,1,0,0,1,1,1,0,1,0}
　　             {0,0,0,0,1,0,0,1,0,0,1,1,1,0,0,0,0,0,1,1,0,1,0,0,1,0,0,0,0,0,1,0,1,0,0,1,0,0,0,1}
　　             {0,1,0,0,0,1,0,0,1,0,0,0,0,1,1,0,1,0,0,0,0,1,1,0,0,0,0,0,1,1,0,1,1,1,1,1,0,1,0,1}
　　             {1,0,0,0,0,0,1,0,0,1,1,0,0,0,1,0,0,1,1,0,0,1,1,1,1,1,0,1,0,1,1,0,0,1,1,1,0,0,0,1}
　　             {0,0,0,0,0,0,0,0,1,0,1,0,0,1,0,1,0,0,0,0,0,0,1,1,1,1,0,0,1,1,0,0,1,0,1,0,1,1,0,1}
　　             {0,0,1,0,1,1,0,1,0,1,0,0,1,1,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,1,0,1,1,0,0,1,1}
　　             {0,0,0,0,0,1,1,1,0,1,0,0,1,0,0,1,0,0,0,1,1,1,0,1,1,0,0,0,1,0,0,1,1,1,0,1,0,1,0,0}
　　             {0,0,1,0,0,0,1,1,0,0,1,0,0,0,0,0,0,1,1,0,0,0,1,0,1,0,1,1,0,0,0,0,0,0,0,1,0,1,0,1}
　　             {1,0,0,1,1,1,1,0,1,0,0,1,0,1,1,0,0,1,1,0,1,0,1,0,1,1,1,0,0,0,0,0,0,0,1,0,1,1,1,0}
　　             {0,1,1,0,0,1,1,1,0,1,0,0,0,0,1,0,1,0,0,0,0,1,0,0,0,1,0,1,0,0,1,0,0,1,1,0,0,0,1,0}
　　             {1,1,0,1,0,0,0,0,0,0,0,1,0,0,1,0,0,1,1,0,0,1,0,0,0,1,0,0,1,1,0,0,1,0,0,0,0,1,0,1}
　　             {1,0,0,1,1,0,0,0,1,0,1,0,0,0,1,0,0,0,0,1,0,0,0,0,0,1,0,1,1,1,1,1,1,1,1,1,1,1,0,0}
　　             {1,0,0,1,0,1,1,0,1,0,1,0,1,1,0,0,0,0,1,0,0,0,0,0,0,0,1,1,0,0,0,1,1,0,1,1,0,0,0,0}
　             　{0,0,1,1,0,0,0,1,0,0,0,1,1,0,0,0,0,1,0,1,1,1,1,0,1,0,0,0,1,0,1,1,1,0,1,1,0,0,0,1}
        }
    }*/
        public static void main5(String[] args) {
            int[][] numbers = {
                    {393353, 901440, 123481, 850930, 423154, 240461},
                    {373746, 232926, 396677, 486579, 744860, 468782},
                    {941389, 777714, 992588, 343292, 385198, 876426},
                    {483857, 241899, 544851, 647930, 772403, 109929},
                    {882745, 372491, 877710, 340000, 659788, 658675},
                    {296521, 491295, 609764, 718967, 842000, 670302}
            };

            int maxDivisors = 0;
            int minNumber = Integer.MAX_VALUE;
            for (int i = 0; i < numbers.length; i++) {
                for (int j = 0; j < numbers[i].length; j++) {
                    int divisors = countDivisors(numbers[i][j]);
                    if (divisors > maxDivisors || (divisors == maxDivisors && numbers[i][j] < minNumber)) {
                        maxDivisors = divisors;
                        minNumber = numbers[i][j];
                    }
                }
            }

            System.out.println("约数个数最多的数是：" + minNumber);
        }

        public static int countDivisors(int num) {
            int count = 0;
            for (int i = 1; i <= Math.sqrt(num); i++) {
                if (num % i == 0) {
                    count++;
                    if (i != num / i) {
                        count++;
                    }
                }
            }
            return count;
        }



    public static void main33(String[] args) {
            int count = 0;
            int num = 1;
            while (count < 23) {
                int sumBinary = getSumBinary(num);
                int sumOctal = getSumOctal(num);
                if (sumBinary == sumOctal) {
                    count++;
                    if (count == 23) {
                        System.out.println(num);
                    }
                }
                num++;
            }
        }

        // 获取一个正整数转化成二进制后所有数位的数字之和
        public static int getSumBinary(int num) {
            int sum = 0;
            while (num > 0) {
                sum += num % 2;
                num /= 2;
            }
            return sum;
        }

        // 获取一个正整数转化成八进制后所有数位的数字之和
        public static int getSumOctal(int num) {
            int sum = 0;        while (num > 0) {
                sum += num % 8;
                num /= 8;
            }
            return sum;
        }




    public static void main3(String[] args) {
            int base = 2;
            int exponent = 2023;
            int modulus = 1000;
            long result = fastPower(base, exponent, modulus);
            System.out.println(result);
        }

        public static long fastPower(int base, int exponent, int modulus) {
            long result = 1;
            while (exponent > 0) {
                if (exponent % 2 == 1) {
                    result = (result * base) % modulus;
                }
                exponent /= 2;
                base = (base * base) % modulus;
            }
            return result;
        }

        public static void main2(String[] args) {
            long result = 1L;
            for (int i = 0; i < 2023; i++) {
                result = (result * 2) % 1000;
            }
            System.out.println(result);
        }


    public static void main1(String[] args) {
            int charWidth = 10;
            int totalWidth = 30 * charWidth;
            int numOfChars = totalWidth / charWidth;
            System.out.println("一行可以放下 " + numOfChars + " 个字");
        }

}
