package thread;

/**
 * @Author: zzj
 * @Description:
 */
public class Demo8 {

    private static boolean isRunning = true;
    public static void main(String[] args) {
        Thread t = new Thread(() -> {
           while (isRunning) {
               System.out.println("hello thread");
               try {
                   Thread.sleep(1000);
               } catch (InterruptedException e) {
                   throw new RuntimeException(e);
               }
           }
            System.out.println("t 线程已经结束");
        });
        t.start();

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        //3s之后，主线程修改isRunning 的值，从而通知t结束
        System.out.println("控制t线程结束");
        isRunning = false;
    }
}
