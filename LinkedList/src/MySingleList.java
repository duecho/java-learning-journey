/**
 * @Author: zzj
 * @Description:
 */
public class MySingleList implements IList {
    static class ListNode{
        public int val;
        public ListNode next;
        public ListNode(int val) {
            this.val = val;
        }
    }
    public ListNode head; //链表的属性，链表头节点 //null


    public void createList() {
        ListNode node1 = new ListNode(22);
        ListNode node2 = new ListNode(33);
        ListNode node3 = new ListNode(44);
        ListNode node4 = new ListNode(55);

        node1.next = node2;
        node2.next = node3;
        node3.next = node4;
        this.head = node1;
    }
    @Override
    public void addFirst(int data) {
        ListNode node = new ListNode(data);
        node.next = this.head;
        this.head = node;

    }

    @Override
    public void addLast(int data) {
        ListNode node = new ListNode(data);
        if (head == null) {
            head = node;
        }else {
            ListNode cur = head;
            while (cur.next != null) {
                cur = cur.next;
            }
            cur.next = node;
        }
    }

    @Override
    public void addIndex(int index, int data) throws IndexExcption{
            if (index > 0 || index <size() ){
                throw new IndexExcption("index位置不合法"+index);
            }
            ListNode node = new ListNode(data);
            if (head == null){
                head = node;
                return;
            }
            if (index == 0){
                addFirst(data);
                return;
            }
            if (index == size()){
                addLast(data);
                return;
            }
            //中间插入
            ListNode cur =SearchPrevIndex(index);
            node.next = cur.next;
            cur.next = node;

    }
    private ListNode SearchPrevIndex(int index){
            ListNode cur =head;
            int count = 0;
            while (count != index-1){
                cur = cur.next;
                count++;
            }
            return cur;
    }
    //求当前链表是否存在Key
    @Override
    public boolean contains(int key) {
        ListNode cur = head;
        while (cur != null){
            if (cur.val == key) {
                return true;
            }
            cur = cur.next;
        }
        return false;
    }

    @Override
    public void remove(int key) {
        if(head == null){
            return;
        }

        if (head.val == key){
            head = head.next;
            return;
        }
        ListNode cur = findPrevKey(key);
        if (cur == null){
            return;
        }
        ListNode del = cur.next;
        cur.next = del.next;

    }
    private ListNode findPrevKey(int key){
        ListNode cur = head;
        if(head == null){
            return null;
        }
        while(cur.next != null){
            if (cur.next.val == key){
                return cur;
            }else {
                cur = cur.next;
            }
        }
        return null;
    }

    @Override
    public void removeAllKey(int key) {
        if (head == null){
            return;
        }
        ListNode prev = head;
        ListNode cur = head.next;
        while (cur != null){
            if (cur.val ==key){
                prev.next = cur.next;
                cur = cur.next;
            }else {
                prev = cur;
                cur = cur.next;
            }
        }
        if (head.val == key){
            head = head.next;

        }

    }
    //求当前链表有多少个节点
    @Override
    public int size() {
        int count = 0;
        ListNode cur = head;
        while (cur != null) {
            count++;
            cur = cur.next;
        }
        return count;
    }

    @Override
    public void clear() {
        head = null;
    }

    @Override
    public void display() {
        ListNode cur = head;
        while (cur != null) {
            System.out.print(cur.val+" ");
            cur = cur.next;
        }
        System.out.println();
    }
    public void display(ListNode newHead) {
        ListNode cur = newHead;
        while (cur != null) {
            System.out.print(cur.val+" ");
            cur = cur.next;
        }
        System.out.println();
    }

    public ListNode reverseList() {
        if(head == null){
            return head;
        }
        if(head.next == null){
            return head;
        }
        ListNode cur = head.next;
        head.next = null;
        while(cur != null){
            ListNode curNext = cur.next;
            cur.next = head;
            head = cur;
            cur = curNext;
        }
        return head;
    }
    public ListNode middleNode() { //返回链表的中间节点
        ListNode fast = head;
        ListNode slow = head;
        while(fast != null && fast.next != null){
            slow = slow.next;
            fast = fast.next.next;
        }
        return slow;
    }

    public ListNode FindKthToTail(int k){ //找到倒数第K个节点
        if(k <= 0){
            return null;

        }
        if (head == null){
            return null;
        }
        ListNode fast = head;
        ListNode slow = head;
        int count = 0;
        while(count != k-1) {
            if (fast.next != null) {
                fast = fast.next;
                count++;
            }else {
                return null;
            }
        }
        while (fast.next != null){
            fast = fast.next;
            slow = slow.next;
        }
        return  slow;
    }

    public ListNode partition(int x){
        ListNode cur =head;
        ListNode bs = null;
        ListNode be = null;
        ListNode as = null;
        ListNode ae = null;
        while (cur != null) {
            if (cur.val < x) {
                if (bs == null) {
                    bs = cur;
                    be = cur;
                }else {
                    be.next = cur;
                    be = be.next;
                }
            }else {
                if (as == null) {
                    as = cur;
                    ae = cur;
                }else {
                    ae.next = cur;
                    ae = ae.next;
                }
            }
            cur = cur.next;
        }
        if (bs == null){
            return as;
        }
        be.next = as;
        if (as != null) {
            ae.next = null;
        }
        return bs;
    }
    //回文链表
    public boolean chkPalindrome() {
        if (head == null) {
            return true;
        }
        //1.找中间节点
        ListNode fast =head;
        ListNode slow = head;
        while (fast != null && fast.next != null) {
            fast =fast.next.next;
            slow = slow.next;
        }

        //2.翻转
        ListNode cur = slow.next;
        while (cur != null) {
            ListNode curNext = cur.next;
            cur.next = slow;
            slow = cur;
            cur = curNext;
        }

        //3.一个往前，一个往后
        while (slow != head) {
            if (slow.val != head.val) {
                return false;
            }
            if (head.next == slow) {
                return true;       //偶数情况
            }
            head = head.next;
            slow = slow.next;
        }
        return true;
    }




}
