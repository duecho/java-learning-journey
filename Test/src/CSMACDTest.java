import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CSMACDTest {
    public static void main(String[] args) {
        JFrame frame = new JFrame("CSMA/CD Simulator");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(400, 300);

        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

        JTextField hostNumberField = new JTextField(10);
        JTextField transNumberField = new JTextField(10);
        JTextField delayTimeField = new JTextField(10);
        JButton startButton = new JButton("Start Simulation");
        JTextArea outputArea = new JTextArea(10, 30);

        panel.add(new JLabel("Number of Hosts:"));
        panel.add(hostNumberField);
        panel.add(new JLabel("Number of Transmissions per Host:"));
        panel.add(transNumberField);
        panel.add(new JLabel("Propagation Delay Time:"));
        panel.add(delayTimeField);
        panel.add(startButton);
        panel.add(new JScrollPane(outputArea));

        frame.add(panel);
        frame.setVisible(true);

        startButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int hostNumber = Integer.parseInt(hostNumberField.getText());
                int transNumber = Integer.parseInt(transNumberField.getText());
                int delayTime = Integer.parseInt(delayTimeField.getText());

                VirtualHost[] virtualHosts = new VirtualHost[hostNumber];
                for (int j = 0; j < hostNumber; j++) {
                    virtualHosts[j] = new VirtualHost("Host " + j, transNumber, delayTime, outputArea);
                }

                for (int i = 0; i < hostNumber; i++) {
                    virtualHosts[i].start();
                }
            }
        });
    }
}

class VirtualHost extends Thread {
    private static int bus = 0;
    private String name;
    private int num;
    private int propDelay;
    private JTextArea outputArea;

    public VirtualHost(String name, int total, int propDelay, JTextArea outputArea) {
        this.name = name;
        num = total;
        this.propDelay = propDelay;
        this.outputArea = outputArea;
    }

    public void run() {
        for (int n = 1; n <= num; n++) {
            try {
                Thread.sleep((int) (Math.random() * propDelay) / 2);
            } catch (InterruptedException e) {
                outputArea.append("Interrupted: Interrupt exception\n");
            }

            int count = 0;
            while (count < 16) {
                if (bus == 0) {
                    try {
                        Thread.sleep(propDelay);
                    } catch (InterruptedException e) {
                        outputArea.append("Interrupted: Interrupt exception\n");
                    }
                    bus++;
                } else {
                    outputArea.append("总线忙！！！\n");
                    continue;
                }

                try {
                    Thread.sleep(propDelay);
                } catch (InterruptedException e) {
                    outputArea.append("Interrupted: Interrupt exception\n");
                }

                if (bus == 1) {
                    outputArea.append("Host[" + name + "] 第 " + n + " 个数据帧发送成功!\n");
                    bus = 0;
                    break;
                }

                if (bus >= 2) {
                    count++;
                    outputArea.append("Host[" + name + "] 第 " + n + " 个数据帧第 " + count + " 次发送碰撞!\n");
                    bus = 0;
                    try {
                        BackoffTimer timer = new BackoffTimer();
                        Thread.sleep(2 * propDelay * timer.backoffTime(count));
                    } catch (InterruptedException e) {
                        outputArea.append("Interrupted: Interrupt exception\n");
                    }
                }
            }

            if (count >= 16) {
                outputArea.append("Host[" + name + "] 第 " + n + " 个数据帧发送失败！\n");
            }
        }
    }
}

class BackoffTimer {
    public int backoffTime(int transNum) {
        int random;
        int temp;
        temp = Math.min(transNum, 10);
        random = (int) (Math.random() * Math.pow(2, temp) - 1);
        return random;
    }
}