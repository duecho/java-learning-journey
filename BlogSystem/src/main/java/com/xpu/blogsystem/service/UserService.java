package com.xpu.blogsystem.service;

import com.xpu.blogsystem.mapper.BlogInfoMapper;
import com.xpu.blogsystem.mapper.UserInfoMapper;
import com.xpu.blogsystem.model.BlogInfo;
import com.xpu.blogsystem.model.UserInfo;
import com.xpu.blogsystem.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;

/**
 * @Author: zzj
 * @Description:
 */

@Service
public class UserService {
    @Autowired
    private UserInfoMapper userInfoMapper;
    @Autowired
    private BlogInfoMapper blogInfoMapper;
    public UserInfo selectUserInfoByName(String userName) {
        return userInfoMapper.selectByName(userName);
    }

    public UserInfo selectUserInfoById(Integer userId) {
        return userInfoMapper.selectById(userId);
    }

    public UserInfo selectAuthorInfoByBlogId(Integer blogId) {
        //1. 根据博客ID, 获取作者ID
        BlogInfo blogInfo = blogInfoMapper.selectById(blogId);
        //2. 根据作者ID, 获取作者信息
        if (blogInfo == null || blogInfo.getUserId()==null){
            return null;
        }
        UserInfo userInfo = userInfoMapper.selectById(blogInfo.getUserId());
        return userInfo;
    }
    public boolean registerUser(UserInfo userInfo) {
        int rowsAffected = userInfoMapper.insert(userInfo);
        return rowsAffected > 0;
    }
}