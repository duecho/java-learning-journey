/**
 * @Author: zzj
 * @Description:
 */


public class MultiplyCipher {
    //乘数密码： 乘数密码是多项式代换密码的另一种特例，其加密方程为：
    //f(l)=(kl)mod q
        public static int multiplyCipherEncrypt(int l, int k, int q) {
            return (k * l) % q;
        }

        public static int multiplyCipherDecrypt(int c, int k, int q) {
            int kInv = 1;
            while ((k * kInv) % q != 1) {
                kInv++;
            }
            return (kInv * c) % q;
        }

        public static void main(String[] args) {
            int l = 7;
            int k = 5;
            int q = 26;

            int encrypted = multiplyCipherEncrypt(l, k, q);
            System.out.println("加密数据为: " + encrypted);

            int decrypted = multiplyCipherDecrypt(encrypted, k, q);
            System.out.println("解密数据为: " + decrypted);
        }
    }
