package demoinnerclass;

/**
 * @projectName: Test11_9
 * @package: demoinnerclass
 * @className: Test
 * @author: Tom of loveing coding
 * @description: TODO
 * @date: 2023/11/9 23:07
 * @version: 1.0
 */
class Outer {
    public int data1 = 1;
    private int data2 = 2;
    public static int data3 = 3;
    //静态内部类
    static class InnerClass {
        public int data4 = 4;
        public int data5 = 5;
        public static int data6 = 6;
        public void test() {
            System.out.println("InnerClass::test()");
            Outer outer = new Outer();
            System.out.println(outer.data1);
            System.out.println(outer.data2);
            System.out.println(data3);
            System.out.println(data4);
            System.out.println(data5);
            System.out.println(data6);
        }
    }

    public void testFunc() {
        System.out.println("Outer::testFunc()");
    }
}
public class Test {
    public static void main(String[] args) {
        //如何获取静态内部类对象
        //InnerClass innerClass = new InnerClass();
        Outer.InnerClass inn = new Outer.InnerClass();
        inn.test();
    }
}