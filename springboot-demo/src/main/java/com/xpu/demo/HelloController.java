package com.xpu.demo;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: zzj
 * @Description:
 */
@RestController
public class HelloController {
        @RequestMapping("/sayHi")
        public String sayHi(){
            return "hello,Spring";
        }
}
