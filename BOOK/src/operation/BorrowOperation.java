package operation;

import book.Book;
import book.BookList;

import java.util.Scanner;

/**
 * @Author: zzj
 * @Description:
 */
public class BorrowOperation implements  IOOperation {
    public void work(BookList bookList) {
        System.out.println("借阅图书！！！");
        System.out.println("请输入要借阅的书名： ");
        Scanner scanner = new Scanner(System.in);
        String name = scanner.nextLine();
        int currentSize = bookList.getUsedSize();

        for(int i = 0; i <currentSize; i++) {
            Book book = bookList.getBook(i);
            if (book.getName().equals(name)) {
               book.setBorrowed(true);
                System.out.println("借阅成功！！！");
                return;
            }
        }
        System.out.println("暂时没有您要借阅的书籍！"+ name);
    }
}
