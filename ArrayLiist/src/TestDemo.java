import java.util.ArrayList;
import java.util.List;

/**
 * @Author: zzj
 * @Description:
 */
public class TestDemo {


    //删除有序数组中的重复项-------力扣

    class Solution1 {
        public int removeDuplicates(int[] nums) {
            if(nums.length == 0) {
                return 0;
            }
            int index = 1;
            for(int i = 1; i < nums.length; i++) {
                if(nums[i] != nums[i-1]) {
                    nums[index] = nums[i];
                    index++;
                }
            }
            return index;

        }
    }

    //力扣-移除元素
    class Solution {
        public int removeElement(int[] nums, int val) {
            int i = 0;
            for(int j = 0; j < nums.length; j++ ) {
                if(nums[j] != val) {
                    nums[i] = nums[j];
                    i++;
                }
            }
            return i;
        }
    }
    public static List<List<Integer>> generate(int numRows) {
        //定义一个二维数组
        List<List<Integer>> ret = new ArrayList<>();
        List<Integer> list = new ArrayList<>();
        list.add(1);//每一行的第一个元素
        ret.add(list);
        for (int i = 1; i < numRows; i++) {
            //每一次循环就是一行
            List<Integer> curRow = new ArrayList<>();
            curRow.add(1);//每一行的第一个元素

            //处理中间的数字
            List<Integer> prevRow = ret.get(i-1);
            for (int j = 1; j < i; j++) {
                int x = prevRow.get(j) + prevRow.get(j-1);
                curRow.add(x);
            }
            //处理最后一个数字
            curRow.add(1);
            ret.add(curRow);
        }
            return ret;
    }

    public static void main5(String[] args) {
        int a = 10;
        List<List<Integer>> temp = generate(a);
//        System.out.print(temp);
//        System.out.println();
        for (List<Integer> ret : temp)
            System.out.println(ret);
    }


    //CETE笔试题目
    public static List<Character> func(String str1,String str2) {
        List<Character> list= new ArrayList<>();
        for (int i = 0; i < str1.length(); i++) {
            char ch = str1.charAt(i);
            if (!str2.contains(ch+ " ")){
                list.add(ch);
            }
        }
        return list;
    }

    public static void main2(String[] args) {
        List<Character>  ret = func("ha h aha ha","hehe");
//        System.out.println(ret);
        for ( char ch : ret) {
            System.out.print(ch);
        }
    }
    public static void main(String[] args) {
        MyArrayList myArrayList = new MyArrayList();
        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.add(10);
        arrayList.add(20);
        arrayList.add(30);
        arrayList.add(9999);
//        arrayList.add(5,2);
        ArrayList<Integer> arrayList2 = new ArrayList<>();
        ArrayList<Number> arrayList1 = new ArrayList<>(arrayList);//这里与源码比较：collection<? extends E>
        List<Number> list = arrayList1.subList(0,2);
        System.out.println(arrayList1);
        System.out.println(list);
        System.out.println("=============");
        list.set(0,100);
        System.out.println(arrayList1);
        System.out.println(list);
        myArrayList.add(1);
        myArrayList.add(2);
        myArrayList.add(3);
        myArrayList.add(4);
        myArrayList.add(5);
        myArrayList.add(6);
        myArrayList.contains(5);
        myArrayList.indexOf(5);
        myArrayList.remove(5);
        myArrayList.display();
    }
}
