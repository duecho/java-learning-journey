/**
 * @projectName: xieyikaifa
 * @package: PACKAGE_NAME
 * @className: Test
 * @author: Tom of loveing coding
 * @description: TODO
 * @date: 2023/12/19 19:49
 * @version: 1.0
 */
import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

// 模拟数据链路层带窗口协议
public class DataLinkLayer {
    private static final int WINDOW_SIZE = 4;
    private ArrayList<String> senderBuffer;
    private ArrayList<String> receiverBuffer;
    private int nextSeqNum;
    private int expectedSeqNum;
    private JTextArea senderTextArea;
    private JTextArea receiverTextArea;

    public DataLinkLayer() {
        senderBuffer = new ArrayList<>();
        receiverBuffer = new ArrayList<>();
        nextSeqNum = 0;
        expectedSeqNum = 0;

        // 创建窗口
        JFrame frame = new JFrame("数据链路层模拟");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new GridLayout(1, 2));

        // 创建发送方窗口
        senderTextArea = new JTextArea();
        JScrollPane senderScrollPane = new JScrollPane(senderTextArea);
        frame.add(senderScrollPane);

        // 创建接收方窗口
        receiverTextArea = new JTextArea();
        JScrollPane receiverScrollPane = new JScrollPane(receiverTextArea);
        frame.add(receiverScrollPane);

        frame.setSize(800, 400);
        frame.setVisible(true);
    }

    // 发送数据
    public void sendData(String data) {
        if (senderBuffer.size() < WINDOW_SIZE) {
            senderBuffer.add(data);
            senderTextArea.setText("发送端窗口:\n" + senderBuffer.toString());
            // 模拟传输过程中的错误
            if (Math.random() < 0.2) {
                senderTextArea.append("\n数据在传输、重新传输过程中损坏");
                senderBuffer.remove(data);
            }
        } else {
            senderTextArea.append("\n窗口已满，无法发送数据");
        }
    }

    // 接收数据
    public void receiveData(String data) {
        if (receiverBuffer.size() < WINDOW_SIZE) {
            if (Integer.parseInt(data.split(":")[0]) == expectedSeqNum) {
                receiverBuffer.add(data);
                receiverTextArea.setText("接收端窗口:\n" + receiverBuffer.toString());
                expectedSeqNum++;
                // 模拟传输过程中的错误
                if (Math.random() < 0.2) {
                    receiverTextArea.append("\n数据在传输过程中损坏，请求重新传输");
                    receiverBuffer.remove(data);
                    expectedSeqNum--;
                }
            } else {
                receiverTextArea.append("\n接收到的乱序数据并丢弃");
            }
        } else {
            receiverTextArea.append("\n窗口已满，无法接收数据");
        }
    }

    public static void main(String[] args) {
        DataLinkLayer dataLinkLayer = new DataLinkLayer();

        // 模拟发送数据
        for (int i = 0; i < 6; i++) {
            dataLinkLayer.sendData(i + ":Data");
        }

        // 模拟接收数据
        for (int i = 0; i < 6; i++) {
            dataLinkLayer.receiveData(i + ":Data");
        }
    }
}


