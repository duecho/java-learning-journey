package com.xpu.mybatis.mapper;
import com.xpu.mybatis.model.UserInfo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @Author: zzj
 * @Description:
 */
@SpringBootTest
class UserInfoMapperTest {
    @Autowired
    private UserInfoMapper userInfoMapper;
    @Test
    void queryUserInfos() {
        userInfoMapper.queryUserInfos().forEach(System.out::println);
    }
    @Test
    void insert() {
        UserInfo userInfo = new UserInfo();
        userInfo.setUsername("zhaoliu");
        userInfo.setPassword("zhaoliu");
        userInfo.setGender(2);
        userInfo.setAge(21);
        userInfo.setPhone("18612340005");
        Integer result = userInfoMapper.insert(userInfo);
//        Integer result1 = userInfoMapper.insert2(userInfo);
        System.out.println("result:"+result);
    }


    @Test
    void delete() {
        System.out.println(userInfoMapper.delete(5));
    }

    @Test
    void update() {
        UserInfo userInfo = new UserInfo();
        userInfo.setId(7);
        userInfo.setUsername("z");
        userInfo.setPassword("123456");
        System.out.println("result:"+userInfo);
    }


    @Test
    void queryUserInfos1() {
    }

    @Test
    void queryUserInfo() {
        List<UserInfo> userInfoList = userInfoMapper.queryUserInfos4();
        System.out.println(userInfoList);
    }

}