package com.bit.www;

public class Dog {
    private String name;//姓名
    private int age;//年龄
    private String color;//颜色

    public Dog() {

    }

    /**
     * 构造方法 用来实例化 狗这个对象
     *
     * @param name
     * @param age
     * @param color
     */
    public Dog(String name, int age, String color) {
        this.name = name;
        this.age = age;
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void barks() {
        System.out.println(this.name + " 正在汪汪汪叫。。。。");
    }

    public void eat() {
        System.out.println(this.name + " 正在吃狗粮。。。。");
    }

    public static void staticFun() {
        System.out.println("staticFn......");
    }

    public void show() {
        System.out.println("狗名： " + this.name + "年龄： " + this.age + "颜色; " + this.color);
    }

    @Override
    public String toString() {
       String ret =  "Dog{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", color='" + color + '\'' +
                '}';
       return ret;
    }
}

