package com.xpu.blogsystem.mapper;

import com.xpu.blogsystem.model.UserInfo;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

@Mapper
public interface UserInfoMapper {
    /**
     * 根据名称, 查询用户信息
     * @return
     */
    @Select("select id, user_name,password, github_url from `user` where user_name = #{name} and delete_flag=0")
    UserInfo selectByName(String name);

    /**
     * 根据ID, 查询用户信息
     * @return
     */
    @Select("select id, user_name, github_url from `user` where id = #{id} and delete_flag=0")
    UserInfo selectById(Integer id);
    @Update("UPDATE `user` SET avatar_url = #{avatarUrl} WHERE id = #{id} and delete_flag=0")
    UserInfo updateUserInfo(UserInfo userInfo);
    @Insert("INSERT INTO `user` (user_name, password) VALUES (#{userName}, #{password})")
    Integer insert(UserInfo userInfo);
}
