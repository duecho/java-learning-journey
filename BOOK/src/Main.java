import book.BookList;
import user.AdminUser;
import user.NormalUser;
import user.User;

import java.util.Scanner;

/**
 * &#064;Author:  zzj
 * &#064;Description:
 */
public class Main {


    public static User login() {
        System.out.println("请输入你的姓名：");
        Scanner scanner = new Scanner(System.in);
        String name = scanner.nextLine();
        System.out.println("请选择您的身份：1.管理员  2.用户");
        int choice = scanner.nextInt();
        if(choice == 1) {
            return new AdminUser(name);
        }else {
            return new NormalUser(name);
        }
    }


    public static void main(String[] args) {

        BookList bookList = new BookList();
        User user = login();//此时不确定使用者是谁，AdminUser(name);或者NormalUser(name)
        while (true) {
            int choice = user.menu();//根据菜单返回的choice来执行对应操作
            user.doOperation(choice, bookList);
        }
    }
}
