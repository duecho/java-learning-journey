package demo1;

/**
 *
 */

class Animal {
    public String name;
    public int age;

    public Animal(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public void eat() {
        System.out.println(this.name+" 正在吃....");
    }
}
class Dog extends Animal {
    public String color;

    public Dog(String name,int age,String color) {
        super(name,age);
        this.color = color;
    }
    @Override
    public void eat() {
        System.out.println(this.name+" 正在吃狗粮....");
    }

    public void barks() {
        System.out.println(this.name+" 正在汪汪叫....");
    }

    /*public void barks(int age) {
        System.out.println(this.name+" 正在汪汪叫....");
    }*/
}
class Bird extends Animal {

    public Bird(String name,int age) {
        super(name,age);
    }

    public void fly() {
        System.out.println(this.name +" 正在飞....");
    }

    public void eat() {
        System.out.println(this.name+" 正在吃鸟粮....");
    }
}

public class Test {
    public static void main(String[] args) {




       /* Animal animal2 = new Bird("布谷",1);
        //animal2.eat();
        //animal2.fly();

        Bird bird = (Bird)animal2;
        bird.fly();*/

        Animal animal1 = new Dog("旺财",3,"红色");
        //
        if(animal1 instanceof Bird) {
            Bird bird2 = (Bird) animal1;
            bird2.fly();
        }else {
            System.out.println("hello");
        }

    }

    public static void fun(Animal animal) {
        animal.eat();
    }

    public static Animal fun2() {
        return new Bird("布谷",1);
    }

    public static void main3(String[] args) {
        Dog dog = new Dog("旺财",3,"红色");
        //Animal animal = dog;
        fun(dog);

        Bird bird = new Bird("布谷",1);
        Animal animal2 = bird;

        //animal2.fly();

        fun(bird);
    }

    public static void main2(String[] args) {
       /* Dog dog = new Dog("旺财",3,"红色");
        Animal animal = dog;
*/
        Animal animal = new Dog("旺财",3,"红色");
        animal.eat();
        //animal.barks();

    }
    public static void main1(String[] args) {
        Dog dog = new Dog("旺财",3,"红色");
        dog.barks();
        //dog.barks(10);
        dog.eat();

    }
}