package enumtest;

/**
 * @Author: zzj
 * @Description:
 */

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class Test {


    public static void main(String[] args) {
        try {
            Class<?> c = Class.forName("enumtest.TestEnum");

            Constructor constructor =
                    c.getDeclaredConstructor(String.class,int.class,int.class,String.class);
            constructor.setAccessible(true);


            TestEnum testEnum = (TestEnum)constructor.newInstance("fffff",9,10,"white");

            System.out.println(testEnum);

        }catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e);
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }
}