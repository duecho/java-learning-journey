package queuedemo;

/**
 * @Author: zzj
 * @Description:
 */
public class MyCircularQueue {

    public int[] elem;

    public int front;
    public int rear;
    public MyCircularQueue(int k) {
        elem = new int[k+1];
    }
    //进入队列
    public boolean enQueue(int value) {
        if (isFull()) {
            return  false;
        }
        elem[rear] = value;
        rear = (rear+1) % elem.length;
        return true;
    }
    //删除队头元素
    public boolean deQueue() {
        if (isEmpty()) {
            return false;
        }
        front = (front+1) % elem.length;
        return true;
    }

    //得到队头元素，不删除
    public int Front() {
        if (isEmpty()) {
            return -1;
        }
        return elem[front];
    }
    //得到队尾元素，不删除
    public int Rear() {
        if (isEmpty()) {
            return -1;
        }
        int index = (rear == 0) ? elem.length-1 : rear-1;
        return elem[index];
    }
    //判空front和rear相遇
    public boolean isEmpty() {
        return front == rear;
    }
    public boolean isFull() {
        return (rear+1) %elem.length == front;
    }
}
