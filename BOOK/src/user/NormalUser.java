package user;

import operation.*;

import java.util.Scanner;

/**
 * @Author: zzj
 * @Description:
 */
public class NormalUser extends User {
   public NormalUser(String name) {
       super(name);
       this.ioOperations = new IOOperation[]{
               new ExitOperation(),
               new FindOpera(),
               new BorrowOperation(),
               new ReturnOperation(),
       };
   }

    public int menu()  {
        System.out.println("********用户菜单********");
        System.out.println("*****  1.查找图书  *****");
        System.out.println("*****  2.借阅图书  *****");
        System.out.println("*****  3.归还图书  *****");
        System.out.println("*****  0.退出系统  *****");
        System.out.println("**********************");
        System.out.println("请选择你的操作： ");
        Scanner scanner = new Scanner(System.in);
        int choice = scanner.nextInt();
        return choice;
    }
}
