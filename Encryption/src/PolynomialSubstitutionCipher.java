/**
 * @Author: zzj
 * @Description:
 */
public class PolynomialSubstitutionCipher {

    // 加密函数
    public static int encrypt(int l, int[] coefficients, int q) {
        int result = 0;
        for (int i = 0; i < coefficients.length; i++) {
            result = (result + coefficients[i] * power(l, i, q)) % q;
        }
        return result;
    }

    // 解密函数
    public static int decrypt(int c, int[] coefficients, int q) {
        int result = 0;
        int inverse = modInverse(coefficients[1], q);
        for (int i = 0; i < coefficients.length; i++) {
            result = (result + coefficients[i] * power(c, i, q)) % q;
        }
        return (result * inverse) % q;
    }

    // 计算 l^i mod q
    private static int power(int l, int i, int q) {
        if (i == 0) {
            return 1;
        }
        int result = l;
        for (int j = 1; j < i; j++) {
            result = (result * l) % q;
        }
        return result;
    }

    // 计算模逆
    private static int modInverse(int a, int q) {
        a = a % q;
        for (int i = 1; i < q; i++) {
            if ((a * i) % q == 1) {
                return i;
            }
        }
        return 1;
    }

    public static void main(String[] args) {
        int q = 26; // 字母表大小
        int[] shiftCoefficients = {1, 2}; // 移位密码参数
        int[] multiplyCoefficients = {3}; // 乘数密码参数
        int[] affineCoefficients = {5, 7}; // 仿射密码参数

        int plaintext = 10; // 明文
        int shiftCipher = encrypt(plaintext, shiftCoefficients, q);
        int multiplyCipher = encrypt(plaintext, multiplyCoefficients, q);
        int affineCipher = encrypt(plaintext, affineCoefficients, q);

        System.out.println("Plaintext: " + plaintext);
        System.out.println("Shift Cipher: " + shiftCipher);
        System.out.println("Multiply Cipher: " + multiplyCipher);
        System.out.println("Affine Cipher: " + affineCipher);

        int shiftDecrypted = decrypt(shiftCipher, shiftCoefficients, q);
        int multiplyDecrypted = decrypt(multiplyCipher, multiplyCoefficients, q);
        int affineDecrypted = decrypt(affineCipher, affineCoefficients, q);

        System.out.println("Shift Decrypted: " + shiftDecrypted);
        System.out.println("Multiply Decrypted: " + multiplyDecrypted);
        System.out.println("Affine Decrypted: " + affineDecrypted);
    }
}

