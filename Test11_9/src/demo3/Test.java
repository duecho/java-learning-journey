package demo3;

/**
 * @projectName: Test11_9
 * @package: demo3
 * @className: Test
 * @author: Tom of loveing coding
 * @description: TODO
 * @date: 2023/11/9 23:05
 * @version: 1.0
 */
public class Test {

    public static void func1(Animal animal) {
        animal.eat();
    }

    public static void testFly(IFly iFly) {
        iFly.fly();
    }

    public static void testRun(IRun iRun) {
        iRun.run();
    }

    public static void testSwim(ISwim iSwim) {
        iSwim.swim();
    }

    public static void main(String[] args) {
        testSwim(new Duck("唐老鸭"));
        testSwim(new Dog("旺财"));

        testRun(new Roboot());


        IRun iRun = new Roboot();
        iRun.run();


    }

    public static void main3(String[] args) {
        testRun(new Bird("布谷"));
        testRun(new Duck("唐老鸭"));
    }

    public static void main2(String[] args) {
        testFly(new Bird("布谷"));
        testFly(new Duck("唐老鸭"));
        //testFly(new Dog("旺财"));

    }

    public static void main1(String[] args) {

        func1(new Dog("旺财"));
        func1(new Bird("布谷"));
    }
}