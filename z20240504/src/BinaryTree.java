

import java.util.*;

/**
 * @Author: zzj
 * @Description:
 */
public class BinaryTree {
    static class TreeNode {
        public char val;
        public TreeNode left;
        public TreeNode right;

        public TreeNode(char val) {
            this.val = val;
        }
    }

    public TreeNode createTree() {
        TreeNode A = new TreeNode('A');
        TreeNode B = new TreeNode('B');
        TreeNode C = new TreeNode('C');
        TreeNode D = new TreeNode('D');
        TreeNode E = new TreeNode('E');
        TreeNode F = new TreeNode('F');
        TreeNode G = new TreeNode('G');
        TreeNode H = new TreeNode('H');
        A.left = B;
        A.right = C;
        B.left = D;
        B.right = E;
        C.left = F;
        C.right = G;
        E.right = H;

        return A;
    }

    // 前序遍历
    public void preOrder(TreeNode root) {
        if (root == null) {
            return;
        }
        System.out.print(root.val + " ");
        preOrder(root.left);
        preOrder(root.right);
    }

    // 中序遍历
    public void inOrder(TreeNode root) {
        if (root == null) {
            return;
        }
        inOrder(root.left);
        System.out.print(root.val + " ");
        inOrder(root.right);
    }

    // 后序遍历
    public void postOrder(TreeNode root) {
        if (root == null) {
            return;
        }
        postOrder(root.left);
        postOrder(root.right);
        System.out.print(root.val + " ");
    }

    public int nodeSize;

    public void size(TreeNode root) {
        if (root == null) {
            return;
        }
        nodeSize++;
        size(root.left);
        size(root.right);
    }

    //子问题

    public int size2(TreeNode root) {
        if (root == null) {
            return 0;
        }
        int tmp = size2(root.left) + size2(root.right) + 1;
        return tmp;
    }

    //上述两种求节点个数

    public int leafSize;

    public void getLeafNodeCount(TreeNode root) {
        if (root == null) {
            return;
        }
        if (root.left == null && root.right == null) {
            leafSize++;
        }
        getLeafNodeCount(root.left);
        getLeafNodeCount(root.right);
    }

    public int getLeafNodeCount2(TreeNode root) {
        if (root == null) {
            return 0;
        }
        if (root.left == null && root.right == null) {
            return 1;
        }
        return getLeafNodeCount2(root.left)
                + getLeafNodeCount2(root.right);
    }


    //子问题
    // 获取第K层节点的个数
    public int getKLevelNodeCount(TreeNode root, int k) {
        if (root == null) {
            return 0;
        }
        if (k == 1) {
            return 1;
        }
        return getKLevelNodeCount(root.right, k - 1) + getKLevelNodeCount(root.left, k - 1);
    }

    // 获取二叉树的高度
    //子问题求解
    public int getHeight(TreeNode root) {
        if (root == null) {
            return 0;
        }
        int leftHeight = getHeight(root.left);
        int rightHeight = getHeight(root.right);
        //return (leftHeight > rightHeight ? leftHeight : rightHeight) + 1;
        return Math.max(leftHeight, rightHeight) + 1;
    }

    // 检测值为value的元素是否存在
    public TreeNode find(TreeNode root, int val) {
        if (root == null) {
            return null;
        }
        if (root.val == val) {
            return root;
        }
        TreeNode leftVal = find(root.left, val);
        if (leftVal != null) {
            return leftVal;
        }
        TreeNode rightVal = find(root.right, val);
        if (rightVal != null) {
            return rightVal;
        }
        return null;
    }

    //时间复杂度：O(min(M,N))

    //相同的树
    class Solution {
        public boolean isSameTree(TreeNode p, TreeNode q) {
            if (p != null && q == null || p == null && q != null) {
                return false;
            }
            //上述代码排除两个非同的
            if (p == null && q == null) {
                return true;
            }
            if (p.val != q.val) {
                return false;
            }
            return isSameTree(p.left, q.left) && isSameTree(p.right, q.right);
        }
    }
    //翻转二叉树
    class Solution1 {
        public TreeNode invertTree(TreeNode root) {
            if (root == null) {
                return null;
            }
            if (root.left == null && root.right == null) {
                return root;
            }
            TreeNode tmp = root.right;
            root.right = root.left;
            root.left = tmp;

            invertTree(root.left);
            invertTree(root.right);
            return root;
        }
    }
    //另一棵树的子树
    class Solution123 {
        public boolean isSubtree(TreeNode root, TreeNode subRoot) {
            if(root == null ) return false;
            if(isSameTree(root,subRoot)) {
                return true;
            }
            if(isSubtree(root.left,subRoot)) {
                return true;
            }
            if(isSubtree(root.right,subRoot)) {
                return true;
            }
            return false;
        }
        public boolean isSameTree(TreeNode p, TreeNode q) {
            if(p != null && q == null || p == null && q != null) {
                return false;
            }
            //上述代码排除两个非同的
            if(p == null && q == null) {
                return true;
            }
            if(p.val != q.val) {
                return false;
            }
            return isSameTree(p.left,q.left) && isSameTree(p.right,q.right);
        }
    }

    //时间复杂度 O(N^2)
    //求高度平衡的二叉树（重点！！！！！）
//    class Solution3 {
//        public boolean isBalanced(TreeNode root) {
//            if(root == null) {
//                return true;
//            }
//
//            int leftH = getHeight(root.left);
//            int rightH = getHeight(root.right);
//            if(Math.abs(leftH - rightH) < 2 && isBalanced(root.left)
//                    && isBalanced(root.right)) {
//                return true;
//            }
//            return false;
//        }
//        public int getHeight(TreeNode root) {
//            if (root == null) {
//                return 0;
//            }
//            int leftHeight = getHeight(root.left);
//            int rightHeight = getHeight(root.right);
//            //return (leftHeight > rightHeight ? leftHeight : rightHeight) + 1;
//            return Math.max(leftHeight,rightHeight) + 1;
//        }
//    }


    //平衡二叉树的O(N)算法----大厂常考题目
    class Solution6 {
        public boolean isBalanced(TreeNode root) {
            if (root == null) {
                return true;
            }

            return getHeight2(root) >= 0;
        }

        public int getHeight2(TreeNode root) {
            if (root == null) {
                return 0;
            }
            int leftHeight = getHeight2(root.left);
            if (leftHeight < 0) {
                return -1;
            }
            int rightHeight = getHeight2(root.right);
            if (leftHeight >= 0 && rightHeight >= 0
                    && Math.abs(leftHeight - rightHeight) <= 1) {
                return Math.max(leftHeight, rightHeight) + 1;
            } else {
                return -1;
            }
            //return (leftHeight > rightHeight ? leftHeight : rightHeight) + 1;
        }

    }

    //对称二叉树
    class Solution7{
        public boolean isSymmetric(TreeNode root) {
            if(root == null) {
                return true;
            }
            return isSymmetricChild(root.left,root.right);
        }

        public boolean isSymmetricChild(TreeNode leftTree,TreeNode rightTree) {
            //1.结构上
            if(leftTree == null && rightTree != null ||
                    leftTree != null && rightTree == null) {
                return false;
            }
            if(leftTree == null && rightTree == null) {
                return true;
            }
            //2.值上
            if(leftTree.val != rightTree.val) {
                return false;
            }
            return isSymmetricChild(leftTree.left,rightTree.right)
                    && isSymmetricChild(leftTree.right,rightTree.left);
        }
    }

    //二叉树的创建和遍历
     /*
    class TreeNode {
    public char val;
    public TreeNode left;
    public TreeNode right;
    public TreeNode(char val) {
        this.val = val;
    }
}

// 注意类名必须为 Main, 不要有任何 package xxx 信息
public class Main {

    public static int i = 0;

    private static TreeNode  createTree(String str) {
        TreeNode root = null;
        char ch = str.charAt(i);
        if (ch != '#') {
            root = new TreeNode(ch);
            i++;
            root.left = createTree(str);
            root.right = createTree(str);
        } else {
            i++;
        }
        return root;

    }
    public static void inOrder(TreeNode root) {
        if (root == null) {
            return;
        }
        inOrder(root.left);
        System.out.print(root.val + " ");
        inOrder(root.right);
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        while (in.hasNextLine()) { // 注意 while 处理多个 case
            String str = in.nextLine();
            TreeNode root = createTree(str);
            inOrder(root);
        }
    }
}
     */
    //层序遍历，利用队列思想
    public void levelOrder(TreeNode root) {
        if (root == null) {
            return;
        }
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        while (!queue.isEmpty()) {
            TreeNode cur  = queue.poll();
            System.out.print(cur.val+" ");
            if (cur.left != null) {
                queue.offer(cur.left);
            }
            if (cur.right != null) {
                queue.offer(cur.right);
            }
        }
    }

    //Leetcode的层序遍历
    class Solution8 {
        public List<List<Character>> levelOrder(TreeNode root) {
            List<List<Character>> retList = new ArrayList<>();
            if (root == null) {
                return retList;
            }
            Queue<TreeNode> queue = new LinkedList<>();
            queue.offer(root);
            while (!queue.isEmpty()) {
                int size = queue.size();
                List<Character> list = new ArrayList<>();
                while (size != 0) {
                    TreeNode cur = queue.poll();
                    list.add(cur.val);
                    size--;

                    if (cur.left != null) {
                        queue.offer(cur.left);
                    }

                    if (cur.right != null) {
                        queue.offer(cur.right);
                    }

                }
                retList.add(list);
            }
            return retList;
        }
    }
    // 判断一棵树是不是完全二叉树
   public boolean isCompleteTree(TreeNode root) {
       if (root == null) {
           return true;
       }
       Queue<TreeNode> queue = new LinkedList<>();
       queue.offer(root);
       while (!queue.isEmpty()) {
           TreeNode cur = queue.poll();
           if (cur != null) {
               queue.offer(cur.left);
               queue.offer(cur.right);
           } else {
               break;
           }
       }
       while (!queue.isEmpty()) {
           TreeNode cur = queue.poll();
           if (cur != null) {
               return false;
           }
       }
       return true;
   }

   //给定一个二叉树, 找到该树中两个指定节点的最近公共祖先
   class Solution33 {
       public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
           if(root == null) {
               return null;
           }
           if(root == p || root == q) {
               return root;
           }
           TreeNode left = lowestCommonAncestor(root.left,p,q);
           TreeNode right = lowestCommonAncestor(root.right,p,q);
           if(left != null && right != null) {
               return root;
           }else if(left != null) {
               return left;
           }else {
               return right;
           }

       }
   }

   //另外一种找到path的上述题解，找最近公共祖先
   public boolean getPath(TreeNode root,TreeNode node,Stack<TreeNode> stack) {
        if (root == null) {
            return false;
        }
        stack.push(root);
        if (root == node) {
            return true;
        }

        boolean flgLeft = getPath(root.left,node,stack);
       if(flgLeft){
            return true;
       }
       boolean flgRight = getPath(root.right,node,stack);
       if(flgRight){
           return true;
       }
       stack.pop();
       return false;
   }
    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        if (root == null) {
            return null;
        }
        Stack<TreeNode> stack1 = new Stack<>();
        Stack<TreeNode> stack2 = new Stack<>();
        getPath(root,p,stack1);
        getPath(root,q,stack2);
        int size1 = stack1.size();
        int size2 = stack2.size();
        if (size1 >size2) {
            int size = size1- size2;
            while (size != 0) {
                stack1.pop();
                size--;
            }
        }else  {
            int size = size2- size1;
            while (size != 0) {
                stack2.pop();
                size--;
            }
        }

        while (!stack1.isEmpty() && !stack2.isEmpty()) {
            if (stack1.peek().equals(stack2.peek())) {
                return stack1.pop();
            }else {
                stack1.pop();
                stack2.pop();
            }
        }
        return null;
    }

    //从前序与中序遍历序列构造二叉树
    class Solution55 {
        public int preIndex;
        public TreeNode buildTree(int[] preorder, int[] inorder) {
            return buildTreeChild(preorder,inorder,0,inorder.length-1);
        }
        private TreeNode buildTreeChild(int[] preorder, int[] inorder,int inBegin,int inEnd) {
            if(inBegin >inEnd) {
                return null;
            }
            //1.创建根节点
            TreeNode root = new TreeNode((char) preorder[preIndex]);
            //2.中序遍历的字符串当中，找到当前根节点的下标
            int rootIndex = findRootIndex(inorder,inBegin,inEnd,preorder[preIndex]);
            preIndex++;
            //分别创建左子树和右子树
            root.left = buildTreeChild(preorder,inorder,inBegin,rootIndex-1);
            root.right = buildTreeChild(preorder,inorder,rootIndex+1,inEnd);
            return root;
        }
        private int findRootIndex(int[] inorder,int inBegin,int inEnd,int key) {
            for(int i = inBegin; i <= inEnd;i++) {
                if(inorder[i] == key) {
                    return i;
                }
            }
            return -1;
        }
    }

    //根据一棵树的中序遍历与后序遍历构造二叉树
    class Solution442 {
        public int postIndex;
        public TreeNode buildTree(int[] inorder,int[] postorder) {
            postIndex = postorder.length-1;
            return buildTreeChild(postorder,inorder,0,inorder.length-1);
        }
        private TreeNode buildTreeChild(int[] postorder,int[] inorder,int inBegin,int inEnd) {
            if(inBegin >inEnd) {
                return null;
            }
            //1.创建根节点
            TreeNode root = new TreeNode((char) postorder[postIndex]);
            //2.中序遍历的字符串当中，找到当前根节点的下标
            int rootIndex = findRootIndex(inorder,inBegin,inEnd,postorder[postIndex]);
            postIndex--;
            //分别创建右子树和左子树
            root.right = buildTreeChild(postorder,inorder,rootIndex+1,inEnd);
            root.left = buildTreeChild(postorder,inorder,inBegin,rootIndex-1);
            return root;
        }
        private int findRootIndex(int[] inorder,int inBegin,int inEnd,int key) {
            for(int i = inBegin; i <= inEnd;i++) {
                if(inorder[i] == key) {
                    return i;
                }
            }
            return -1;
        }
    }


    //根据二叉树创建字符串
    class Solution11 {
        public String tree2str(TreeNode root) {
            StringBuilder sbu = new StringBuilder();
            tree2strChild(root, sbu);
            return sbu.toString();

        }

        public void tree2strChild(TreeNode root, StringBuilder sbu) {
            if (root == null) {
                return;
            }
            sbu.append(root.val);
            // 递归左树
            if (root.left != null) {
                sbu.append("(");
                tree2strChild(root.left, sbu);
                sbu.append(")");
            } else {

                //root等于最底部的数
                if (root.right == null) {
                    return;
                } else {
                    sbu.append("()");
                }
            }
            // 递归右树
            if (root.right != null) {
                sbu.append("(");
                tree2strChild(root.right, sbu);
                sbu.append(")");
            } else {
                return;
            }
        }

    }

    //非递归的前序遍历
    class Solution2 {
        public List<Integer> preorderTraversal(TreeNode root) {
            List<Integer> list = new ArrayList<>();
            if (root == null) {
                return list;
            }

            TreeNode cur = root;
            Stack<TreeNode> stack = new Stack<>();
            while (cur != null || !stack.isEmpty()) {
                while (cur != null) {
                    stack.push(cur);
                    list.add((int) cur.val);
                    cur = cur.left;
                }
                TreeNode top = stack.pop();
                cur = top.right;
            }

            return list;
        }
    }
    //非递归的中序遍历
    class Solution22 {
        public List<Integer> inorderTraversal(TreeNode root) {
            List<Integer> list = new ArrayList<>();
            if (root == null) {
                return list;
            }

            TreeNode cur = root;
            Stack<TreeNode> stack = new Stack<>();
            while (cur != null || !stack.isEmpty()) {
                while (cur != null) {
                    stack.push(cur);

                    cur = cur.left;
                }
                TreeNode top = stack.pop();
                list.add((int) top.val);
                cur = top.right;
            }

            return list;
        }
    }

    //非递归的后序遍历
    class Solution553 {
        public List<Integer> postorderTraversal(TreeNode root) {
            List<Integer> list = new ArrayList<>();
            if (root == null) {
                return list;
            }

            TreeNode cur = root;
            TreeNode prev = null;
            Stack<TreeNode> stack = new Stack<>();
            while (cur != null || !stack.isEmpty()) {
                while (cur != null) {
                    stack.push(cur);
                    cur = cur.left;
                }
                TreeNode top = stack.peek();
                if(top.right == null || top.right == prev) {
                    stack.pop();
                    list.add((int) top.val);
                    prev = top;
                }else {
                    cur = top.right;
                }

            }

            return list;
        }
    }


}


