package demo;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @Author: zzj
 * @Description:
 */
public class CardGame {
    public static final String[] suits = {"♥","♣","♦","♠"};
    /**
     * 生成一副扑克牌
     * 52张
     */

    //买牌,买了52张牌，不包含大小王
    public List<Card> buyCard() {
        List<Card> cardList =new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 13; j++) {
                String suit = suits[i];
                Card card = new Card(suit,j);
                cardList.add(card);
                cardList.add( new Card(suits[i],j));
            }
        }
        return cardList;
    }

    //洗牌
    public void shuffle(List<Card> cardList){
        Random random = new Random();
        for (int i = cardList.size()-1; i > 0 ; i--) {
        int index = random.nextInt(i);
        swap(cardList,i,index);
        }
    }

    private static void swap(List<Card> cardList,int i,int j){
        Card tmp = cardList.get(i);
        cardList.set(i,cardList.get(j));
        cardList.set(j,tmp);
    }

    //发牌：每个人轮流摸五张牌

    public List<List<Card>> getCard(List<Card> cardList){
        List<Card> hand1 = new ArrayList<>();
        List<Card> hand2 = new ArrayList<>();
        List<Card> hand3 = new ArrayList<>();
        List<List<Card>>  hand = new ArrayList<>();
        hand.add(hand1);
        hand.add(hand2);
        hand.add(hand3);
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 3; j++) {
                //此处模拟揭牌，相当于删除下标为0的第一个元素
                Card card = cardList.remove(0);
                //模拟对应的牌放到了对应的手里
                hand.get(j).add(card);
            }
        }
        return hand;
    }
}
