/**
 * @Author: zzj
 * @Description:
 */
public class MyLinkedList implements IList {

    static class ListNode{
        public int val;
        public ListNode next;
        public ListNode prev;

        public ListNode(int val) {
            this.val = val;
        }
    }

    public ListNode head;
    public ListNode last;
    @Override
    public void addFirst(int data) {
        ListNode node = new ListNode(data);
        if (head == null) {
            head = node;
            last = node;
        }else {
            node.next = head;
            head.prev = node;
            head = node;
        }
    }

    @Override
    public void addLast(int data) {
        ListNode node = new ListNode(data);
        if (head == null) {
            last = node;
            head = node;
        }else {
            last.next = node;
            node.prev = last;
            last = node;
        }
    }

    @Override
    public void addIndex(int index, int data) {
        if (index < 0 || index > size()) {
            throw new IndexExcption("双向链表插入，index不合法" + index);
        }
        if (index == 0) {
            addFirst(index);
            return;
        }
        if (index == size()) {
            addLast(index);
            return;
        }
        ListNode cur = findIndex(index);
        ListNode node = new ListNode(data);
        node.next = cur;
        cur.prev.next = node;
        node.prev = cur.prev;
        cur.prev = node;
    }

    private ListNode findIndex(int index) {
        ListNode cur = head;
        while(index != 0) {
            cur = cur.next;
            index--;
        }
        return cur;
    }

    @Override
    public boolean contains(int key) {
        ListNode cur = head;
        while (cur != null) {
            if (cur.val == key) {
                return true;
            }
            cur = cur.next;
        }
        return false;
    }

    @Override
    public void remove(int key) {
        ListNode cur = head;

        while (cur != null) {
            if (cur.val == key) {

                if(cur == head) {
                    head = head.next;
                    if (head != null) {
                        head.prev = null;
                    }else {
                        last =null;
                    }
                }else {
                    cur.prev.next = cur.next;
                    if (cur.next != null) {  //删除中间代码
                        cur.next.prev = cur.prev;
                        //cur.prev.next = cur.next;
                    }else {
                        //cur.prev.next = cur.next;
                        last = last.prev;  //删除最后节点代码
                    }
                }
                return;// 删除单个数据
            }
            cur = cur.next;

        }
    }

    @Override
    public void removeAllKey(int key) {
        ListNode cur = head;

        while (cur != null) {
            if (cur.val == key) {

                if(cur == head) {
                    head = head.next;
                    if (head != null) {
                        head.prev = null;
                    }else {
                        last =null;
                    }
                }else {
                    cur.prev.next = cur.next;
                    if (cur.next != null) {  //删除中间代码
                        cur.next.prev = cur.prev;
                        //cur.prev.next = cur.next;
                    }else {
                        //cur.prev.next = cur.next;
                        last = last.prev;  //删除最后节点代码
                    }
                }
                //此处省略删除所有的key
                //return;// 删除单个数据
            }
            cur = cur.next;

        }
    }

    @Override
    public int size() {
        int count = 0;
        ListNode cur = head;
        while(cur != null) {
            System.out.println(cur.val+" ");
            count++;
            cur = cur.next;
        }
        System.out.println();
        return count;
    }

    @Override
    public void clear() {
        ListNode cur = head;
        while (cur != null) {
            ListNode curNext = cur.next;
            cur.next =null;
            cur.prev = null;
            cur = curNext;
        }//上述代码表示一个个释放
        this.head = null;
        this.last = null;  //整体释放
    }

    @Override
    public void display() {
        ListNode cur = head;
        while (cur != null) {
            System.out.println(cur.val+" ");
            cur = cur.next;
        }
        System.out.println();
    }
}
