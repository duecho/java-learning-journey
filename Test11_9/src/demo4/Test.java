package demo4;

import java.util.Objects;

/**
 * @projectName: Test11_9
 * @package: demo4
 * @className: Test
 * @author: Tom of loveing coding
 * @description: TODO
 * @date: 2023/11/9 23:06
 * @version: 1.0
 */
class Person {
    public String name ;

    public Person(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "name: "+name;
    }
/*
    @Override
    public boolean equals(Object obj) {
        Person tmp = (Person)obj;
        return tmp.name.equals(this.name);
        *//*return true;*//*
    }*/

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return Objects.equals(name, person.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}

public class Test {
    public static void main(String[] args) {
        Person person1 = new Person("zhangsan");
        Person person2 = new Person("zhangsan");

        System.out.println(person1.hashCode());
        System.out.println(person2.hashCode());

        System.out.println(person1.equals(person2));

        /*System.out.println(person1 == person2);

        System.out.println(person1.equals(person2));//调用了object的方法

        String str1 = "zhangsan";
        String str2 = "zhangsan";

        System.out.println(str1.equals(str2));*/

    }

}