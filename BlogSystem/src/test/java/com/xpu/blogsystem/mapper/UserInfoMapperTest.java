package com.xpu.blogsystem.mapper;

import com.xpu.blogsystem.model.UserInfo;
import lombok.val;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @Author: zzj
 * @Description:
 */
@SpringBootTest
class UserInfoMapperTest {
    @Autowired
    private UserInfoMapper userInfoMapper;
    @Test
    void selectByName() {
        System.out.println(userInfoMapper.selectByName("zhangsan"));
    }

    @Test
    void selectByID() {
        System.out.println(userInfoMapper.selectById(2));
    }
}