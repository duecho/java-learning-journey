package com.xpu.demo.controller;

import org.springframework.web.bind.annotation.*;

/**
 * @Author: zzj
 * @Description:
 */
@RestController
public class HelloController {
    @RequestMapping("/hello")
    public String hello() {
        return  "hello spring mvc";
    }
    //支持get请求
    @RequestMapping(value = "/hello2",method = {RequestMethod.GET,RequestMethod.POST})
    public String hello2() {
        return "he is superman!";
    }
    @GetMapping("/hello3")
    public String hello3() {
        return "hello spring mvc";
    }
    @PostMapping("/hello4")
    public String hello4() {
        return "hahaha";
    }
}
