package user;

import book.BookList;
import operation.IOOperation;

/**
 * @Author: zzj
 * @Description:
 */
abstract public class User {
    protected  String name;
    protected IOOperation[] ioOperations;
    public  User(String name) {
        this.name = name;
    }
    public abstract int menu();

    public void doOperation(int choice, BookList bookList) {
        this.ioOperations[choice].work(bookList);
        //IOOperation ioOperation = this.ioOperations[choice];
        //ioOperation.work(bookList);
    }
}
