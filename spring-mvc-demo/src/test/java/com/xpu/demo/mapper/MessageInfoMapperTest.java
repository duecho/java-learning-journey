package com.xpu.demo.mapper;

import com.xpu.demo.model.MessageInfo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @Author: zzj
 * @Description:
 */
@SpringBootTest
class MessageInfoMapperTest {

    @Autowired
    private MessageInfoMapper messageInfoMapper;

    @Test
    void insertMessage() {
        MessageInfo messageInfo = new MessageInfo();
        messageInfo.setFrom("泡泡姐");
        messageInfo.setTo("同学们");
        messageInfo.setMessage("写完这个代码就下课");
        messageInfoMapper.insertMessage(messageInfo);
    }

    @Test
    void queryMessage() {
        messageInfoMapper.queryMessage().forEach(System.out::println);
    }
}