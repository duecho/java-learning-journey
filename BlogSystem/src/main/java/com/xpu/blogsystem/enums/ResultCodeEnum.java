package com.xpu.blogsystem.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;


/**
 * @Author: zzj
 * @Description:
 */

@AllArgsConstructor
public enum ResultCodeEnum {
    SUCCESS(200),
    NO_LOGIN(-1),
    FAIL(-2),
    PARAM_INVALID(-3),
    NO_RESOURCES(-4);

    @Getter
    int code;

}