package operation;

import book.BookList;

/**
 * @Author: zzj
 * @Description:
 */
public interface IOOperation {
    void work(BookList bookList);
}
