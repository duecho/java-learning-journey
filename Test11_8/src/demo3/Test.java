package demo3;

abstract class  Shape {

    /* public String name;
    public int age;

   public Shape(String name, int age) {
        this.name = name;
        this.age = age;
    }*/

    //抽象方法
    public abstract void draw();

    /*public void func() {
        System.out.println("func");
    }*/

}
class Cycle extends Shape {
    @Override
    public void draw() {
        System.out.println("○");
    }
}
class Rect extends Shape {
    @Override
    public void draw() {
        System.out.println("矩形");
    }
}

public class Test {
    public static void drawMap(Shape shape) {
        shape.draw();
    }
    public static void main(String[] args) {
        //Shape shape = new Shape();
        Shape shape = new Cycle();
        Shape shape1 = new Rect();
        drawMap(shape);
        drawMap(shape1);
    }
}