package com.xpu.demo.mapper;

import com.xpu.demo.model.MessageInfo;
import org.springframework.web.bind.annotation.RequestMapping;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @Author: zzj
 * @Description:
 */
@Mapper
public interface MessageInfoMapper {
    /**
    * 添加留言
    */
    @Insert("insert into message_info (`from`, `to`, `message`) values " +
            "(#{from}, #{to}, #{message})")
    Integer insertMessage(MessageInfo messageInfo);
    /**
     * 查询留言列表
     */
    @Select("select * from message_info where delete_flag=0")
    List<MessageInfo> queryMessage();
}
