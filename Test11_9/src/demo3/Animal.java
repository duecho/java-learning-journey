package demo3;

/**
 * @projectName: Test11_9
 * @package: demo3
 * @className: Animal
 * @author: Tom of loveing coding
 * @description: TODO
 * @date: 2023/11/9 22:59
 * @version: 1.0
 */
public abstract class Animal {
    public String name;

    public Animal(String name) {
        this.name = name;
    }
    //
    public abstract void eat();
}
