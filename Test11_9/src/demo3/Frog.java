package demo3;

/**
 * @projectName: Test11_9
 * @package: demo3
 * @className: Frog
 * @author: Tom of loveing coding
 * @description: TODO
 * @date: 2023/11/9 23:01
 * @version: 1.0
 */
public class Frog extends Animal implements IAmphibious{


    public Frog(String name) {
        super(name);
    }

    @Override
    public void eat() {

    }

    @Override
    public void test1() {

    }

    @Override
    public void run() {

    }

    @Override
    public void swim() {

    }
}
