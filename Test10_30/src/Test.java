import java.util.Scanner;
public class Test {

    public static int add(int n) {
        if(n == 1) {
            return 1;
        }
        return  n+add(n-1);
    }

    public static void main222(String[] args) {
        System.out.println(add(5));
    }


    /**
     *
     * @param n
     * @param pos1
     * @param pos2
     * @param pos3
     */
    //汉诺塔问题
    public static void hanoi(int n,char pos1,char pos2,char pos3) {
        if(n == 1){
            move(pos1,pos3);
            return ;
        }
        hanoi(n-1,pos1,pos2,pos3);
        move(pos1,pos3);
        hanoi(n-1,pos2,pos1,pos3);
    }

    public static void move(char pos1,char pos2) {
        System.out.print(pos1+ "->"+pos2+" ");
    }
    public static void main(String[] args) {
        hanoi(3,'A','B','C');
    }

    //方法重载的练习
    public static int max(int a,int b) {
        return Math.max(a,b);
    }

    public static double max(double a, double b, double c) {
       double m= Math.max(a,b);
       return Math.max(m,c);
    }
    public static void main56(String[] args) {
        System.out.println(max(3,4));
        System.out.println(max(4.0,6.0,7.7));
    }

    //题目练习
    public static int sum(int a,int b) {
        return a+b;
    }

    public static double sum(double a,double b,double c) {
        return a+b+c;
    }
    public static void main99(String[] args) {
        sum(4,6);
        System.out.println(sum(4,6));
        sum(1.0,2.0,3.0);
        System.out.println(sum(1.0,2.0,3.0));
    }
    //递归求N的阶乘！
    public static int fac(int n) {
        if(n == 1) {
            return 1;
        }
        int tmp = n * fac(n-1);
        System.out.println(tmp);
        return tmp;
    }
    public static void main555(String[] args) {
        fac(4);
    }

    //斐波那契数列
    //迭代循环
    public static int fib(int n) {
        if(n == 1 || n == 2){
            return 1;
        }
        int f1 = 1;
        int f2 = 1;
        int f3 = 1;
        for(int i = 3; i <= n; i++){
            f3 = f1 + f2;
            f1 = f2;
            f2 = f3;
        }
        System.out.println(f3);
        return f3;
    }
    //递归算法
    public static int count = 0;
    public static int fib2(int n) {
        if(n == 1 ||n == 2){
            count++;
            return 1;
        }
        return fib2(n-1)+fib2(n-2);

    }
    public static void main8(String[] args) {
        System.out.println(fib2(5));
    }

    //递归练习
    public static void main7(String[] args) {
        print(54321);
    }


    public static void print(int n) {
        if(n < 10) {
            System.out.print(n+" ");
            return;
        }
        print(n/10);
        System.out.print(n%10+" ");
    }

    public static void main666(String[] args) {
        System.out.println(add1(123));
    }

    public static int add1(int n) {
        if(n>10){
            return add1(n/10)+n%10;
        }
        return n%10;
    }

    //方法嵌套
    public static int max2(int a, int b) {
        return a>b ? a : b;
    }

    public static int max3(int a, int b, int c) {
        int Max = max2(a, b);
        return Max > c ? Max:c;
    }
    public static void main4(String[] args) {
        System.out.println(max3(5,7,9));
    }



    //模拟登录
    public static void main5(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int count = 3;
        while (count != 0){
            System.out.println("请输入你的密码： ");
            String password = scanner.nextLine();
            if(password.equals("123")){
                System.out.println("登陆成功！");
                break;
            }else{
                count--;
                System.out.println("登录失败，你还有"+count+"次的机会! ");
                System.out.println("请重新输入：");
            }

        }
    }





    public static void main3(String[] args) {
        int n = 12345;
        while ( n != 0){
            System.out.print( n % 10 +" ");
            n /= 10;
        }
    }




    public static void main2(String[] args) {
        double sum = 0;
        for(int i = 1; i < 100; i++){
            if(i % 2 == 0){
                sum -= 1.0 / i;
            }else{
                sum += 1.0/i;
            }
        }
        System.out.println("结果为： " + sum);
    }
















        //打印叉形
        public static void main1(String[] args) {


            Scanner sc = new Scanner(System.in);
            // 注意 hasNext 和 hasNextLine 的区别
            while (sc.hasNextInt()) { // 注意 while 处理多个 case
                int a = sc.nextInt();
                for(int i = 0; i < a; i++){
                    for(int j = 0; j < a; j++){
                        if(i == j || (i+j) == a-1){
                            System.out.print("*");
                        }else{
                            System.out.print(" ");
                        }
                    }
                    System.out.println();
                }
            }
        }
}

