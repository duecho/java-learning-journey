package operation;

import book.Book;
import book.BookList;

import java.util.Scanner;

/**
 * @Author: zzj
 * @Description:
 */
public class ReturnOperation implements IOOperation {
    public void work(BookList bookList) {
        System.out.println("归还图书！！！");
        System.out.println("请输入要归还的图书书名： ");
        Scanner scanner = new Scanner(System.in);
        String name = scanner.nextLine();
        int currentSize = bookList.getUsedSize();

        for(int i = 0; i <currentSize; i++) {
            Book book = bookList.getBook(i);
            if (book.getName().equals(name)) {
                book.setBorrowed(false);
                System.out.println("归还成功！！！");
                return;
            }
        }
        System.out.println("您暂时还没有归还图书哦！！！" + name);
    }
}
