import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class Supplier {
    private String id;
    private String name;
    private String encryptedContactInfo;

    public Supplier(String id, String name, String contactInfo) {
        this.id = id;
        this.name = name;
        this.encryptedContactInfo = encryptContactInfo(contactInfo);
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEncryptedContactInfo() {
        return encryptedContactInfo;
    }

    private String encryptContactInfo(String contactInfo) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(contactInfo.getBytes());
            StringBuilder hexString = new StringBuilder();
            for (byte b : hash) {
                String hex = Integer.toHexString(0xff & b);
                if (hex.length() == 1) {
                    hexString.append('0');
                }
                hexString.append(hex);
            }
            return hexString.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
    }
}

class SupplyChainManagement {
    private Map<String, Supplier> suppliers;

    public SupplyChainManagement() {
        suppliers = new HashMap<>();
    }

    public void addSupplier(Supplier supplier) {
        suppliers.put(supplier.getId(), supplier);
    }

    public Supplier getSupplier(String id) {
        return suppliers.get(id);
    }

    public List<Supplier> searchSuppliers(String keyword) {
        List<Supplier> foundSuppliers = new ArrayList<>();
        for (Supplier supplier : suppliers.values()) {
            if (supplier.getName().contains(keyword)) {
                foundSuppliers.add(supplier);
            }
        }
        return foundSuppliers;
    }
}

public class PrivacyProtectingSupplyChainManagement {
    public static void main(String[] args) {
        SupplyChainManagement management = new SupplyChainManagement();

        Supplier supplier1 = new Supplier("S001", "ABC Company", "1234567890");
        Supplier supplier2 = new Supplier("S002", "DEF Company", "0987654321");

        management.addSupplier(supplier1);
        management.addSupplier(supplier2);

        Supplier foundSupplier = management.getSupplier("S001");
        if (foundSupplier!= null) {
            System.out.println("Found Supplier: " + foundSupplier.getName());
        }

        List<Supplier> searchResults = management.searchSuppliers("ABC");
        for (Supplier result : searchResults) {
            System.out.println("Search Result: " + result.getName());
        }
    }
}