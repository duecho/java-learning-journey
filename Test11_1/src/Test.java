import java.util.Arrays;
import javax.swing.*;
/**
 * @Author
 */

public class Test {
    public static void main(String[] args) {
        int[] array = {1,12,31,4,5};
        System.out.println(Arrays.toString(array));
        bubbleSort(array);
        System.out.println(Arrays.toString(array));
    }

    public static void bubbleSort(int[] array) {
        for (int i = 0 ; i < array.length-1; i++) {
            boolean flg = false;
            for (int j = 0; j < array.length-1-i; j++) {
                if (array[j]>array[j+1]) {
                    int tmp = array[j];
                    array[j] = array[j+1];
                    array[j+1] = tmp;
                    flg = true;
                }
            }
            if (!flg) {
                return;//说明已经有序
            }
        }

    }
    public static void main1111(String[] args) {
        int[] array = new int[10];

        Arrays.fill(array,1,3,99);//局部填充

        System.out.println(Arrays.toString(array));



     /*   int[] array1 = {1,2,31,14,5};
        int[] array2 = {1,2,31777,14,5};

        System.out.println(Arrays.equals(array1, array2));
*/


        /*Arrays.sort(array);
        System.out.println(Arrays.toString(array));//[1, 2, 5, 14, 31]
        //System.out.println(binarySearch(array, 111));
        System.out.println(Arrays.binarySearch(array, 51));
*/
       /* reverse(array);
        System.out.println(Arrays.toString(array));*/

    }
    public static void reverse(int[] array) {
        int i = 0;
        int j = array.length-1;
        while (i < j) {
            int tmp = array[i];
            array[i] = array[j];
            array[j] = tmp;
            i++;
            j--;
        }
    }
    public static void main111(String[] args) {
        int[] array = {1,2,31,14,5};
        Arrays.sort(array);

    }

    public static int binarySearch(int[] array,int key) {
        int i = 0;
        int j = array.length-1;
        while(i <= j) {
            int mid = (i+j) / 2;
            if (array[mid] < key) {
// 去右侧区间找
                i= mid + 1;
            } else if (array[mid] == key) {
// 相等, 说明找到了
                return mid;
            } else {
// 去左侧区间找
                j = mid -1;
            }
        }
// 循环结束, 说明没找到
        return -1;
    }
    //顺序查找
    public static int find(int[] arr, int data) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == data) {
                return i;
            }
        }
        return -1; // 表示没有找到
    }
    public static void main11(String[] args) {
        int[] array = {1,2,3,4,5};
        int[] dest = new int[array.length];
        System.arraycopy(array,0,dest,0,array.length);

        //int[] copy = Arrays.copyOfRange(array,1,3);//[1,3)
        System.out.println(Arrays.toString(dest));
    }


    public static void main10(String[] args) {
        int[] array = {1,2,3,4,5};
        //拷贝 array 这个数组  ，新的长度为 array.length *2
        array = Arrays.copyOf(array,array.length*2);
        //array 指向了 新的数组空间
        System.out.println(Arrays.toString(array));

    }
    public static void main7(String[] args) {
        int[] array = {1,2,3,4};
        int[] copy = Arrays.copyOf(array,array.length);
        System.out.println(Arrays.toString(copy));
    }
    public static void main77(String[] args) {
       /* int[] array = {1,2,3,4};//局部变量  引用变量
        func1(array);
        System.out.println(Arrays.toString(array));

        func2(array);
        System.out.println(Arrays.toString(array));
*/
        //第一个：   第二个：
        /*int x = 10;
        func4(x);
        System.out.println(x);*/

        int[] array = null;
        //String ret = Arrays.toString(array);
        String ret = myToString(array);
        System.out.println(ret);

    }

    public static void main5(String[] args) {
        //int[] array = null;
        int[] array = {1,2,3,4};
        String ret =myToString(array);
        System.out.println(ret);
    }

    /**
     * 把一个整形数组 转化为字符串
     * @param array  数组参数
     * @return   将数组 转变为字符串进行返回
     */
    public static String myToString(int[] array) {
        assert  array != null;
        if(array == null) {
            return "null";
        }
        String ret = "[";
        for (int i = 0; i < array.length; i++) {
            ret += array[i];
            if (i != array.length-1) {
                ret += ",";
            }
        }
        ret += "]";
        return ret;
    }






    public static void main4(String[] args) {
        int[] array = {1,2,3,4};   //局部变量，引用类型
        func1(array);
        System.out.println(Arrays.toString(array));  //将数组变为字符串打印
        func2(array);
        System.out.println(Arrays.toString(array));
    }

    public static void func1(int[] array) {
        array = new int[]{11,22,33,44,55};
    }
    public static void func2(int[] array) {
        array[0] = 99;
    }

    public static int[] func5() {
        int[] array = {1,2,3,4};
        return array;
    }



    public static void func4(int a) {
        a = 20;
    }



    public static void func3(int[] array) {
        int tmp = array[0];
        array[0] = array[1];
        array[1] = tmp;
    }



    public static void print(int[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.println(array[i]+" ");
        }
    }
    public static void main3(String[] args) {
        int[] array = {1,2,3,4};
        print(array);
    }


    /**
     *
    //数组的三种遍历方法
     */
    public static void main2(String[] args) {
        int[] array = {1,2,3,4};  //局部变量，引用类型
        int a = 0;  //基本变量
        for (int i = 0; i < array.length; i++) {
            System.out.println(array[i]+" ");
        }
        System.out.println();
        //for each  若将来只是遍历程序，则可用
        for (int x : array) {
            System.out.println(x+" ");
        }
        System.out.println();

        //第三种模式
        String ret =  Arrays.toString(array);
        System.out.println(ret);
    }


    public static void main1(String[] args) {
        int[] array = null;
        System.out.println("数组的长度："+array.length);
        System.out.println();
    }
}
