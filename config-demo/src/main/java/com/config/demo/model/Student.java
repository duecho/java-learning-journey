package com.config.demo.model;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * @Author: zzj
 * @Description:
 */
@ConfigurationProperties(prefix = "student")
@Component
@Data

public class Student {
    private Integer id;
    private String name;
    private Integer age;
    private List<String> dbtypes;
    private HashMap<String,String> map;
}
