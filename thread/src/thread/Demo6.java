package thread;

/**
 * @Author: zzj
 * @Description:
 */
public class Demo6 {
    public static void main(String[] args) {
        Thread t = new Thread(() -> {
           while (true) {
               System.out.println("hello");
               try {
                   Thread.sleep(1000);
               } catch (InterruptedException e) {
                   throw new RuntimeException(e);
               }
           }
        },"自定义线程");
        t.start();
    }
}
