package demo2;

/**
 * @projectName: Test11_9
 * @package: demo2
 * @className: Test
 * @author: Tom of loveing coding
 * &#064;description:  TODO
 * @date: 2023/11/9 22:54
 * @version: 1.0
 */
interface IShape {
    /*
     int a = 10;
     int aa = 100;
     int bbb = 1000;

    public default void test(){
        System.out.println("fsadfsa");
    }

    public static void func() {

    }*/

    void draw();

}
class Rect implements IShape {
    @Override
    public void draw() {
        System.out.println("画矩形");
    }
}
class Cycle implements IShape {
    @Override
    public void draw() {
        System.out.println("画圆圈");
    }
}
class Flower implements IShape {
    @Override
    public void draw() {
        System.out.println("画❀");
    }
}
public class Test {

    public static void main(String[] args) {
        Computer computer = new Computer();
        computer.powerOn();

        computer.useDevice(new Mouse());
        computer.useDevice(new KeyBoard());

        computer.powerOff();
    }

    public static void func(IShape iShape) {
        iShape.draw();
    }
    public static void main1(String[] args) {
        //IShape iShape = new IShape();
        IShape iShape1 = new Flower();
        IShape iShape2 = new Cycle();
        IShape iShape3 = new Rect();

        func(iShape1);
        func(iShape2);
        func(iShape3);

        IShape[] iShapes = {iShape1,iShape2};

    }
}
