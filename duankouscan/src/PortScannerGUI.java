import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class PortScannerGUI extends JFrame {
    private DefaultListModel<String> resultList = new DefaultListModel<>();

    public PortScannerGUI() {
        super("端口扫描器");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(300, 200);

        setLayout(new BorderLayout());

        JList<String> resultList = new JList<>(this.resultList);
        add(new JScrollPane(resultList), BorderLayout.CENTER);

        JButton scanButton = new JButton("启动扫描");
        add(scanButton, BorderLayout.SOUTH);

        scanButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String host = "localhost";
                int startPort = 1;
                int endPort = 65535;

                for (int port = startPort; port <= endPort; port++) {
                    try {
                        Socket socket = new Socket();
                        socket.connect(new InetSocketAddress(host, port), 1000);
                        socket.close();
                        resultList.handleEvent("端口 " + port + " 是开放的");
                    } catch (IOException ex) {
                        // 端口可能是关闭的
                    }
                }
            }
        });
    }

    public static void main(String[] args) {
        PortScannerGUI frame = new PortScannerGUI();
        frame.setVisible(true);
    }
}
