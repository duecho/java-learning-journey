package book;

/**
 * @Author: zzj
 * @Description:
 */
public class BookList {
    private final Book[] books;
    private  int usedSize; //记录书架放书数量
    public  BookList() {
        this.books = new  Book[10];
        this.books[0] = new Book("西游记","吴承恩",10,"小说");
        this.books[1] = new Book("三国演义","罗贯中",9,"小说");
        this.books[2] = new Book("水浒传","施耐庵",11,"小说");
        this.books[3] = new Book("红楼梦","曹雪芹",12,"小说");
        this.usedSize = 4;
    }

    public int getUsedSize() {
        return usedSize;
    }

    public void setUsedSize(int usedSize) {
        this.usedSize = usedSize;
    }

    public Book getBook(int pos) {
        return books[pos];
    }
    public void setBook(Book book,int pos) {
        books[pos] = book;
    }
}
